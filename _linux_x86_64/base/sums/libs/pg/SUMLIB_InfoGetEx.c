/* Processed by ecpg (4.10.0) */
/* These include files are added by the preprocessor */
#include <ecpglib.h>
#include <ecpgerrno.h>
#include <sqlca.h>
/* End of automatic include section */

#line 1 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGetEx.pgc"
/* SUMLIB_InfoGetEx.pgc
 * Returns in the results keylist info on the storage units for the 
 * given ds_index values. 
 * The keywords in the input KEY * are like so:
 *   uid    = unique id from a SUM_open() call for this session
 *   reqcnt = number of ds_index values being requested
 *   dsix_0 = first ds_index value 
 *   dsix_1 = second ds_index value 
 *   [etc.]
 * Returns 0 on success, else error code.
 * Does the query of all the ds_index values in a single SQL call.
 * This is faster than doing a call for each ds_index.
 */
#include <SUM.h>
#include <sum_rpc.h>
#include <soi_error.h>
#include <keyU.h>
#include <printk.h>


static int n_off, n_total;	/* counts since dsds_svc started */
static int c_off, c_total;	/* counts for last 100 ds */
static char dsix_name[128];
static char testname[256];

extern char jsoc_machine[];

int SUMLIB_InfoGetEx(KEY *params, KEY **results);
//int SUMLIB_InfoGetEx_U(KEYU *params, KEYU **results);
int SUMLIB_InfoGetArray(Sunumarray *params, char *file, int *mode);
int compare_sta(const void *a, const void *b);

int SUMLIB_InfoGetEx(KEY *params, KEY **results)
{
/* exec sql begin declare section */
     
     
     
    
      

     
     
     

     
     
     
     
     
     
     
     
       
     
     
     
     
     
     
     
     

#line 36 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGetEx.pgc"
 long long uid ;
 
#line 37 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGetEx.pgc"
 long long ds_index ;
 
#line 38 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGetEx.pgc"
 long long ds_index_arr [ MAXSUMREQCNT ] ;
 
#line 39 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGetEx.pgc"
 int touch ;
 
#line 40 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGetEx.pgc"
 int daro = DARO ;
 
#line 42 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGetEx.pgc"
 int pa_status ;
 
#line 43 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGetEx.pgc"
 int pa_substatus ;
 
#line 44 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGetEx.pgc"
  struct varchar_1  { int len; char arr[ 20 ]; }  effective_date ;
 
#line 46 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGetEx.pgc"
  struct varchar_2  { int len; char arr[ 81 ]; }  online_loc ;
 
#line 47 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGetEx.pgc"
  struct varchar_3  { int len; char arr[ 5 ]; }  online_status ;
 
#line 48 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGetEx.pgc"
  struct varchar_4  { int len; char arr[ 5 ]; }  archive_status ;
 
#line 49 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGetEx.pgc"
  struct varchar_5  { int len; char arr[ 5 ]; }  offsite_ack ;
 
#line 50 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGetEx.pgc"
  struct varchar_6  { int len; char arr[ 81 ]; }  history_comment ;
 
#line 51 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGetEx.pgc"
  struct varchar_7  { int len; char arr[ 81 ]; }  owning_series ;
 
#line 52 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGetEx.pgc"
 int storage_group ;
 
#line 53 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGetEx.pgc"
 double bytes ;
 
#line 54 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGetEx.pgc"
 long long create_sumid ;
 
#line 55 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGetEx.pgc"
  struct varchar_8  { int len; char arr[ 32 ]; }  creat_date ;
 
#line 56 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGetEx.pgc"
  struct varchar_9  { int len; char arr[ 11 ]; }  username ;
 
#line 57 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGetEx.pgc"
  struct varchar_10  { int len; char arr[ 21 ]; }  arch_tape ;
 
#line 58 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGetEx.pgc"
 int arch_tape_fn ;
 
#line 59 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGetEx.pgc"
  struct varchar_11  { int len; char arr[ 32 ]; }  arch_tape_date ;
 
#line 60 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGetEx.pgc"
  struct varchar_12  { int len; char arr[ 21 ]; }  safe_tape ;
 
#line 61 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGetEx.pgc"
 int safe_tape_fn ;
 
#line 62 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGetEx.pgc"
  struct varchar_13  { int len; char arr[ 32 ]; }  safe_tape_date ;
/* exec sql end declare section */
#line 63 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGetEx.pgc"

   int i, reqcnt;

    uid = (long long)getkey_uint64(params, "uid");
    reqcnt = getkey_int(params, "reqcnt");
  	 
     /* exec sql whenever sqlerror  goto  sqlerror ; */
#line 69 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGetEx.pgc"

     /* exec sql whenever not found  goto  noerror ; */
#line 70 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGetEx.pgc"

    //Make the query statement.
    for(i=0; i < reqcnt ; i++) {
      sprintf(dsix_name, "dsix_%d", i);
      if(!findkey(params, dsix_name)) {
        printk("Bad keylist given to DS_DataRequest()\n");
        return(DS_DATA_REQ);
      }
      ds_index = (long long)getkey_uint64(params, dsix_name);


 { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "select online_loc , online_status , archive_status , offsite_ack , history_comment , owning_series , storage_group , bytes , create_sumid , creat_date , username , coalesce ( arch_tape , 'N/A' ) , coalesce ( arch_tape_fn , 0 ) , coalesce ( arch_tape_date , '1958-01-01 00:00:00' ) , coalesce ( safe_tape , 'N/A' ) , coalesce ( safe_tape_fn , 0 ) , coalesce ( safe_tape_date , '1958-01-01 00:00:00' ) from SUM_MAIN where DS_INDEX = $1 ", 
	ECPGt_long_long,&(ds_index),(long)1,(long)1,sizeof(long long), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EOIT, 
	ECPGt_varchar,&(online_loc),(long)81,(long)1,sizeof(struct varchar_2), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_varchar,&(online_status),(long)5,(long)1,sizeof(struct varchar_3), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_varchar,&(archive_status),(long)5,(long)1,sizeof(struct varchar_4), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_varchar,&(offsite_ack),(long)5,(long)1,sizeof(struct varchar_5), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_varchar,&(history_comment),(long)81,(long)1,sizeof(struct varchar_6), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_varchar,&(owning_series),(long)81,(long)1,sizeof(struct varchar_7), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_int,&(storage_group),(long)1,(long)1,sizeof(int), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_double,&(bytes),(long)1,(long)1,sizeof(double), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_long_long,&(create_sumid),(long)1,(long)1,sizeof(long long), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_varchar,&(creat_date),(long)32,(long)1,sizeof(struct varchar_8), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_varchar,&(username),(long)11,(long)1,sizeof(struct varchar_9), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_varchar,&(arch_tape),(long)21,(long)1,sizeof(struct varchar_10), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_int,&(arch_tape_fn),(long)1,(long)1,sizeof(int), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_varchar,&(arch_tape_date),(long)32,(long)1,sizeof(struct varchar_11), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_varchar,&(safe_tape),(long)21,(long)1,sizeof(struct varchar_12), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_int,&(safe_tape_fn),(long)1,(long)1,sizeof(int), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_varchar,&(safe_tape_date),(long)32,(long)1,sizeof(struct varchar_13), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EORT);
#line 91 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGetEx.pgc"

if (sqlca.sqlcode == ECPG_NOT_FOUND) goto noerror;
#line 91 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGetEx.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 91 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGetEx.pgc"


        sprintf(dsix_name, "online_loc_%d", i);
        sprintf(testname, "online_loc_%d", i);
    	setkey_str(results, dsix_name, online_loc.arr);
        sprintf(dsix_name, "online_status_%d", i);
        setkey_str(results, dsix_name, online_status.arr);
        sprintf(dsix_name, "archive_status_%d", i);
    	setkey_str(results, dsix_name, archive_status.arr); 
        sprintf(dsix_name, "offsite_ack_%d", i);
    	setkey_str(results, dsix_name, offsite_ack.arr); 
        sprintf(dsix_name, "history_comment_%d", i);
    	setkey_str(results, dsix_name, history_comment.arr); 
        sprintf(dsix_name, "owning_series_%d", i);
    	setkey_str(results, dsix_name, owning_series.arr); 
        sprintf(dsix_name, "storage_group_%d", i);
        setkey_int(results, dsix_name, storage_group);
        sprintf(dsix_name, "bytes_%d", i);
 	setkey_double(results, dsix_name, bytes);
        sprintf(dsix_name, "creat_date_%d", i);
    	setkey_str(results, dsix_name, creat_date.arr); 
        sprintf(dsix_name, "username_%d", i);
    	setkey_str(results, dsix_name, username.arr); 
        sprintf(dsix_name, "arch_tape_%d", i);
    	setkey_str(results, dsix_name, arch_tape.arr); 
        sprintf(dsix_name, "arch_tape_fn_%d", i);
        setkey_int(results, dsix_name, arch_tape_fn);
        sprintf(dsix_name, "arch_tape_date_%d", i);
    	setkey_str(results, dsix_name, arch_tape_date.arr); 
        sprintf(dsix_name, "safe_tape_%d", i);
    	setkey_str(results, dsix_name, safe_tape.arr); 
        sprintf(dsix_name, "safe_tape_fn_%d", i);
        setkey_int(results, dsix_name, safe_tape_fn);
        sprintf(dsix_name, "safe_tape_date_%d", i);
    	setkey_str(results, dsix_name, safe_tape_date.arr); 
        sprintf(dsix_name, "ds_index_%d", i);
        setkey_uint64(results, dsix_name, (unsigned long long)ds_index);

      //now get values in sum_partn_alloc
      //use DISTINCT in case dup entries
     { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "select distinct on ( effective_date ) effective_date , status , archive_substatus from SUM_PARTN_ALLOC where DS_INDEX = $1  and STATUS <> 8", 
	ECPGt_long_long,&(ds_index),(long)1,(long)1,sizeof(long long), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EOIT, 
	ECPGt_varchar,&(effective_date),(long)20,(long)1,sizeof(struct varchar_1), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_int,&(pa_status),(long)1,(long)1,sizeof(int), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_int,&(pa_substatus),(long)1,(long)1,sizeof(int), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EORT);
#line 133 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGetEx.pgc"

if (sqlca.sqlcode == ECPG_NOT_FOUND) goto noerror;
#line 133 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGetEx.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 133 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGetEx.pgc"

        sprintf(dsix_name, "pa_status_%d", i);
        setkey_int(results, dsix_name, pa_status);
        sprintf(dsix_name, "pa_substatus_%d", i);
        setkey_int(results, dsix_name, pa_substatus);
        sprintf(dsix_name, "effective_date_%d", i);
        setkey_str(results, dsix_name, effective_date.arr);
        continue;
    noerror:
        sprintf(dsix_name, "pa_status_%d", i);
        setkey_int(results, dsix_name, 0);
        sprintf(dsix_name, "pa_substatus_%d", i);
        setkey_int(results, dsix_name, 0);
        sprintf(dsix_name, "effective_date_%d", i);
        setkey_str(results, dsix_name, "N/A");
        continue;
sqlerror:
        printk("sqlerror for i=%d ds_index = %lld\n", i, ds_index);
    }
        setkey_int(results, "reqcnt", reqcnt);
    	{ ECPGtrans(__LINE__, NULL, "commit work");
#line 153 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGetEx.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 153 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGetEx.pgc"

	return NO_ERROR;
}

/********TEST STUFF ONLY*************************************************
int SUMLIB_InfoGetEx_U(KEYU *params, KEYU **results)
{
EXEC SQL BEGIN DECLARE SECTION;
  char stmt[8192];
  long long int uid;
  long long int ds_index;
  long long int ds_index_arr[MAXSUMREQCNT];
  int  touch;
  int  daro = DARO;

    int pa_status;
    int pa_substatus;
    VARCHAR effective_date[20];

    VARCHAR online_loc[81];
    VARCHAR online_status[5];
    VARCHAR archive_status[5];
    VARCHAR offsite_ack[5];
    VARCHAR history_comment[81];
    VARCHAR owning_series[81];
    int storage_group;
    double bytes;
    long long int create_sumid;
    VARCHAR creat_date[32];
    VARCHAR username[11];
    VARCHAR arch_tape[21];
    int arch_tape_fn;
    VARCHAR arch_tape_date[32];
    VARCHAR safe_tape[21];
    int safe_tape_fn;
    VARCHAR safe_tape_date[32];
EXEC SQL END DECLARE SECTION;
   int i, reqcnt;

    uid = (long long)getkey_uint64U(params, "uid");
    reqcnt = getkey_intU(params, "reqcnt");
  	 
     EXEC SQL WHENEVER SQLERROR GOTO sqlerror;
     EXEC SQL WHENEVER NOT FOUND GOTO noerror;
    //Make the query statement.
    for(i=0; i < reqcnt ; i++) {
      sprintf(dsix_name, "dsix_%d", i);
      if(!findkeyU(params, dsix_name)) {
        printk("Bad keylist given to SUMLIB_InfoGetEx_U()\n");
        return(DS_DATA_REQ);
      }
      ds_index = (long long)getkey_uint64U(params, dsix_name);


 EXEC SQL SELECT online_loc, online_status, archive_status, offsite_ack,
 history_comment, owning_series, storage_group, bytes, create_sumid, creat_date, username,
 coalesce(arch_tape,'N/A'), coalesce(arch_tape_fn,0),
 coalesce(arch_tape_date,'1958-01-01 00:00:00'),
 coalesce(safe_tape,'N/A'), coalesce(safe_tape_fn,0),
 coalesce(safe_tape_date,'1958-01-01 00:00:00')
 INTO :online_loc, :online_status, :archive_status, :offsite_ack,
 :history_comment, :owning_series, :storage_group, :bytes, :create_sumid, :creat_date,
 :username, :arch_tape, :arch_tape_fn, :arch_tape_date, :safe_tape,
 :safe_tape_fn, :safe_tape_date
 FROM SUM_MAIN WHERE DS_INDEX = :ds_index;

        sprintf(dsix_name, "online_loc_%d", i);
        sprintf(testname, "online_loc_%d", i);
    	setkey_strU(results, dsix_name, online_loc.arr);
        sprintf(dsix_name, "online_status_%d", i);
        setkey_strU(results, dsix_name, online_status.arr);
        sprintf(dsix_name, "archive_status_%d", i);
    	setkey_strU(results, dsix_name, archive_status.arr); 
        sprintf(dsix_name, "offsite_ack_%d", i);
    	setkey_strU(results, dsix_name, offsite_ack.arr); 
        sprintf(dsix_name, "history_comment_%d", i);
    	setkey_strU(results, dsix_name, history_comment.arr); 
        sprintf(dsix_name, "owning_series_%d", i);
    	setkey_strU(results, dsix_name, owning_series.arr); 
        sprintf(dsix_name, "storage_group_%d", i);
        setkey_intU(results, dsix_name, storage_group);
        sprintf(dsix_name, "bytes_%d", i);
 	setkey_doubleU(results, dsix_name, bytes);
        sprintf(dsix_name, "creat_date_%d", i);
    	setkey_strU(results, dsix_name, creat_date.arr); 
        sprintf(dsix_name, "username_%d", i);
    	setkey_strU(results, dsix_name, username.arr); 
        sprintf(dsix_name, "arch_tape_%d", i);
    	setkey_strU(results, dsix_name, arch_tape.arr); 
        sprintf(dsix_name, "arch_tape_fn_%d", i);
        setkey_intU(results, dsix_name, arch_tape_fn);
        sprintf(dsix_name, "arch_tape_date_%d", i);
    	setkey_strU(results, dsix_name, arch_tape_date.arr); 
        sprintf(dsix_name, "safe_tape_%d", i);
    	setkey_strU(results, dsix_name, safe_tape.arr); 
        sprintf(dsix_name, "safe_tape_fn_%d", i);
        setkey_intU(results, dsix_name, safe_tape_fn);
        sprintf(dsix_name, "safe_tape_date_%d", i);
    	setkey_strU(results, dsix_name, safe_tape_date.arr); 
        sprintf(dsix_name, "ds_index_%d", i);
        setkey_uint64U(results, dsix_name, (unsigned long long)ds_index);

      //now get values in sum_partn_alloc
      //use DISTINCT in case dup entries
     EXEC SQL SELECT DISTINCT ON (effective_date) effective_date,  status, archive_substatus
     INTO :effective_date, :pa_status, :pa_substatus
     FROM SUM_PARTN_ALLOC WHERE DS_INDEX = :ds_index and STATUS != 8;
        sprintf(dsix_name, "pa_status_%d", i);
        setkey_intU(results, dsix_name, pa_status);
        sprintf(dsix_name, "pa_substatus_%d", i);
        setkey_intU(results, dsix_name, pa_substatus);
        sprintf(dsix_name, "effective_date_%d", i);
        setkey_strU(results, dsix_name, effective_date.arr);
        continue;
    noerror:
        sprintf(dsix_name, "pa_status_%d", i);
        setkey_intU(results, dsix_name, 0);
        sprintf(dsix_name, "pa_substatus_%d", i);
        setkey_intU(results, dsix_name, 0);
        sprintf(dsix_name, "effective_date_%d", i);
        setkey_strU(results, dsix_name, "N/A");
        continue;
sqlerror:
        printk("sqlerror for i=%d ds_index = %lld\n", i, ds_index);
    }
        setkey_intU(results, "reqcnt", reqcnt);
    	EXEC SQL COMMIT WORK;
	return NO_ERROR;
}
********END TEST STUFF ONLY*************************************************/

/********MORE TEST STUFF*************************************************
// Called by infodoArray_1() in sum_svc_proc.c.
// Creates a file and puts the answers in it.
// Uses the single ds_index query at a time.
//
int SUMLIB_InfoGetArrayXX(Sunumarray *params, char *file)
{
EXEC SQL BEGIN DECLARE SECTION;
  long long int uid;
  long long int ds_index;
  int  touch;
  int  daro = DARO;

    int pa_status;
    int pa_substatus;
    VARCHAR effective_date[20];

    VARCHAR online_loc[81];
    VARCHAR online_status[5];
    VARCHAR archive_status[5];
    VARCHAR offsite_ack[5];
    VARCHAR history_comment[81];
    VARCHAR owning_series[81];
    int storage_group;
    double bytes;
    long long int create_sumid;
    VARCHAR creat_date[32];
    VARCHAR username[11];
    VARCHAR arch_tape[21];
    int arch_tape_fn;
    VARCHAR arch_tape_date[32];
    VARCHAR safe_tape[21];
    int safe_tape_fn;
    VARCHAR safe_tape_date[32];
EXEC SQL END DECLARE SECTION;
  int i, reqcnt;
  uint64_t *suptr;
  FILE *ifp;
  SUM_info_t *sit, *sitX, *sit0;

    uid = (long long)(params->uid);
    reqcnt = params->reqcnt;
    suptr = params->sunums;
    if((ifp=fopen(file, "w")) == NULL) {
      printk("Can't open %s in SUMLIB_InfoGetArray\n", file);
      return(1);
    }
    sit = (SUM_info_t *)calloc(reqcnt, sizeof(SUM_info_t));
    sit0 = sit;

     EXEC SQL WHENEVER SQLERROR GOTO sqlerror;

    //Make the query statement.
    for(i=0; i < reqcnt ; i++) {
      ds_index = (long long)(*suptr++);

 EXEC SQL WHENEVER NOT FOUND GOTO noerror;
 EXEC SQL SELECT online_loc, online_status, archive_status, offsite_ack,
 history_comment, owning_series, storage_group, bytes, create_sumid, creat_date, username,
 coalesce(arch_tape,'N/A'), coalesce(arch_tape_fn,0),
 coalesce(arch_tape_date,'1958-01-01 00:00:00'),
 coalesce(safe_tape,'N/A'), coalesce(safe_tape_fn,0),
 coalesce(safe_tape_date,'1958-01-01 00:00:00')
 INTO :online_loc, :online_status, :archive_status, :offsite_ack,
 :history_comment, :owning_series, :storage_group, :bytes, :create_sumid, :creat_date,
 :username, :arch_tape, :arch_tape_fn, :arch_tape_date, :safe_tape,
 :safe_tape_fn, :safe_tape_date
 FROM SUM_MAIN WHERE DS_INDEX = :ds_index;
	sit->sunum = (unsigned long long)ds_index;
	sprintf(sit->online_loc, "%s", online_loc.arr);
	sprintf(sit->online_status, "%s", online_status.arr);
	sprintf(sit->archive_status, "%s", archive_status.arr);
	sprintf(sit->offsite_ack, "%s", offsite_ack.arr);
	snprintf(sit->history_comment, 80, "%s", history_comment.arr);
	sprintf(sit->owning_series, "%s", owning_series.arr);
	sit->storage_group = storage_group;
	sit->bytes = bytes;
	sprintf(sit->creat_date, "%s", creat_date.arr);
	sprintf(sit->username, "%s", username.arr);
	sprintf(sit->arch_tape, "%s", arch_tape.arr);
        sit->arch_tape_fn = arch_tape_fn;
	sprintf(sit->arch_tape_date, "%s", arch_tape_date.arr);
	sprintf(sit->safe_tape, "%s", safe_tape.arr);
        sit->safe_tape_fn = safe_tape_fn;
	sprintf(sit->safe_tape_date, "%s", safe_tape_date.arr);

      //now get values in sum_partn_alloc
      //use DISTINCT in case dup entries
     EXEC SQL WHENEVER NOT FOUND GOTO noerror2;
     EXEC SQL SELECT DISTINCT ON (effective_date) effective_date,  status, archive_substatus
     INTO :effective_date, :pa_status, :pa_substatus
     FROM SUM_PARTN_ALLOC WHERE DS_INDEX = :ds_index and STATUS != 8;

	sit->pa_status = pa_status;
	sit->pa_substatus = pa_substatus;
	sprintf(sit->effective_date, "%s", effective_date.arr);
        sitX = sit;
        sitX++;
	sit->next = sitX;
        //fwrite(sit, sizeof(SUM_info_t), 1, ifp);
        sit = sitX;
        continue;
    noerror:
	sit->sunum = (unsigned long long)ds_index;
    noerror2:
	sit->pa_status = 0;
	sit->pa_substatus = 0;
	sprintf(sit->effective_date, "N/A");
        sitX = sit;
        sitX++;
	sit->next = sitX;
        //fwrite(sit, sizeof(SUM_info_t), 1, ifp);
        sit = sitX;
        continue;
sqlerror:
        printk("sqlerror for i=%d ds_index = %lld\n", i, ds_index);
    }
        fwrite(sit0, sizeof(SUM_info_t), reqcnt, ifp);
        fclose(ifp);
        free(sit0);
    	EXEC SQL COMMIT WORK;
	return NO_ERROR;
}
********END MORE TEST STUFF*************************************************/

/* Compare two uint64_t sunum. Returns an integer
 * less than, equal to, or greater than zero to indicate if the first arg
 * is to be considered less than, equal to, or greater than the second.
*/
int compare_sunum(const void *a, const void *b)
{
  uint64_t *x=(uint64_t *)a, *y=(uint64_t *)b;

  if(*x < *y) return(-1);
  if(*x > *y) return(1);
  return(0);
}

/* Called by infodoArray_1() in sum_svc_proc.c.
 * Creates a file and puts the answers in it.
*/
int SUMLIB_InfoGetArray(Sunumarray *params, char *file, int *mode)
{
/* exec sql begin declare section */
   
     
     
    
      

     
     
     

     
     
     
     
     
     
     
     
       
     
     
     
     
     
     
     
     

#line 428 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGetEx.pgc"
 char * stmt ;
 
#line 429 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGetEx.pgc"
 long long uid ;
 
#line 430 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGetEx.pgc"
 long long ds_index ;
 
#line 431 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGetEx.pgc"
 int touch ;
 
#line 432 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGetEx.pgc"
 int daro = DARO ;
 
#line 434 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGetEx.pgc"
 int pa_status ;
 
#line 435 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGetEx.pgc"
 int pa_substatus ;
 
#line 436 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGetEx.pgc"
  struct varchar_14  { int len; char arr[ 20 ]; }  effective_date ;
 
#line 438 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGetEx.pgc"
  struct varchar_15  { int len; char arr[ 81 ]; }  online_loc ;
 
#line 439 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGetEx.pgc"
  struct varchar_16  { int len; char arr[ 5 ]; }  online_status ;
 
#line 440 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGetEx.pgc"
  struct varchar_17  { int len; char arr[ 5 ]; }  archive_status ;
 
#line 441 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGetEx.pgc"
  struct varchar_18  { int len; char arr[ 5 ]; }  offsite_ack ;
 
#line 442 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGetEx.pgc"
  struct varchar_19  { int len; char arr[ 81 ]; }  history_comment ;
 
#line 443 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGetEx.pgc"
  struct varchar_20  { int len; char arr[ 81 ]; }  owning_series ;
 
#line 444 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGetEx.pgc"
 int storage_group ;
 
#line 445 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGetEx.pgc"
 double bytes ;
 
#line 446 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGetEx.pgc"
 long long create_sumid ;
 
#line 447 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGetEx.pgc"
  struct varchar_21  { int len; char arr[ 32 ]; }  creat_date ;
 
#line 448 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGetEx.pgc"
  struct varchar_22  { int len; char arr[ 11 ]; }  username ;
 
#line 449 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGetEx.pgc"
  struct varchar_23  { int len; char arr[ 21 ]; }  arch_tape ;
 
#line 450 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGetEx.pgc"
 int arch_tape_fn ;
 
#line 451 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGetEx.pgc"
  struct varchar_24  { int len; char arr[ 32 ]; }  arch_tape_date ;
 
#line 452 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGetEx.pgc"
  struct varchar_25  { int len; char arr[ 21 ]; }  safe_tape ;
 
#line 453 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGetEx.pgc"
 int safe_tape_fn ;
 
#line 454 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGetEx.pgc"
  struct varchar_26  { int len; char arr[ 32 ]; }  safe_tape_date ;
/* exec sql end declare section */
#line 455 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGetEx.pgc"

  char *cptr;
  int i, reqcnt;
  uint64_t *suptr;
  FILE *ifp;
  SUM_info_t *sit, *sitX, *sitsv;
  int filemode;
  KEYU *L_ds_index = newkeylistU();
  KEYU *L_online_loc = newkeylistU();
  KEYU *L_online_status = newkeylistU();
  KEYU *L_archive_status = newkeylistU();
  KEYU *L_offsite_ack = newkeylistU();
  KEYU *L_history_comment = newkeylistU();
  KEYU *L_owning_series = newkeylistU();
  KEYU *L_storage_group = newkeylistU();
  KEYU *L_bytes = newkeylistU();
  KEYU *L_create_sumid = newkeylistU();
  KEYU *L_creat_date = newkeylistU();
  KEYU *L_username = newkeylistU();
  KEYU *L_arch_tape = newkeylistU();
  KEYU *L_arch_tape_fn = newkeylistU();
  KEYU *L_arch_tape_date = newkeylistU();
  KEYU *L_safe_tape = newkeylistU();
  KEYU *L_safe_tape_fn = newkeylistU();
  KEYU *L_safe_tape_date = newkeylistU();
  KEYU *L_effective_date = newkeylistU();
  KEYU *L_pa_status = newkeylistU();
  KEYU *L_pa_substatus = newkeylistU();
  KEYU *L_key_index = newkeylistU();
  char key[16], keyindex[16];


//char STMT[] = "SELECT ds_index, online_loc, online_status, archive_status, offsite_ack, history_comment, owning_series, storage_group, bytes, create_sumid, creat_date, username, coalesce(arch_tape,'N/A'), coalesce(arch_tape_fn,0), coalesce(arch_tape_date,'1958-01-01 00:00:00'), coalesce(safe_tape,'N/A'), coalesce(safe_tape_fn,0), coalesce(safe_tape_date,'1958-01-01 00:00:00') from sum_main where ds_index in (";

char STMT[] = "SELECT T1.ds_index, T1.online_loc, T1.online_status, T1.archive_status, T1.offsite_ack, T1.history_comment, T1.owning_series, T1.storage_group, T1.bytes, T1.create_sumid, T1.creat_date, T1.username, coalesce(T1.arch_tape,'N/A'), coalesce(T1.arch_tape_fn,0), coalesce(T1.arch_tape_date,'1958-01-01 00:00:00'), coalesce(T1.safe_tape,'N/A'), coalesce(T1.safe_tape_fn,0), coalesce(T1.safe_tape_date,'1958-01-01 00:00:00'), coalesce(T2.effective_date,'195801010000'), coalesce(T2.status,0), coalesce(T2.archive_substatus,0) FROM sum_main AS T1 LEFT OUTER JOIN sum_partn_alloc AS T2 ON (T1.ds_index=T2.ds_index) WHERE T1.ds_index in(";  

    uid = (long long)(params->uid);
    reqcnt = params->reqcnt;
    suptr = params->sunums;
    if(strcmp(jsoc_machine, params->machinetype))
      filemode=1;
    else
      filemode=0;
    if((ifp=fopen(file, "w")) == NULL) {
      printk("Can't open %s in SUMLIB_InfoGetArray\n", file);
      return(1);
    }
    //must sort suptr array in ascending sunum (i.e. ds_index) order
    //Going to do the query w/order by ds_index so need to match this list.
    //qsort(suptr, reqcnt, sizeof(uint64_t), &compare_sunum);

    stmt = (char *)malloc((32*reqcnt)+2048); //will hold sql query
    if(filemode == 0) {
      sit = (SUM_info_t *)calloc(reqcnt, sizeof(SUM_info_t));
      sitsv = sit;
    }
    sprintf(stmt, "%s", STMT);
    for(i=0; i < reqcnt; i++) {
      sprintf(stmt, "%s%lu, ", stmt, *suptr++);
    }
    cptr = rindex(stmt, ',');
    if(cptr) *cptr = '\0';
    //sprintf(stmt, "%s) AND (T2.status is NULL OR T2.status != 8) order by T1.ds_index;", stmt);
    sprintf(stmt, "%s) AND (T2.status is NULL OR T2.status != 8);", stmt);

     /* exec sql whenever sqlerror  goto  sqlerror ; */
#line 520 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGetEx.pgc"

     { ECPGprepare(__LINE__, NULL, 0, "query", stmt);
#line 521 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGetEx.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 521 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGetEx.pgc"

     /* declare reqcursor cursor for $1 */
#line 522 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGetEx.pgc"

     { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "declare reqcursor cursor for $1", 
	ECPGt_char_variable,(ECPGprepared_statement(NULL, "query", __LINE__)),(long)1,(long)1,(1)*sizeof(char), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EOIT, ECPGt_EORT);
#line 523 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGetEx.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 523 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGetEx.pgc"

     /* exec sql whenever not found  goto  end_fetch ; */
#line 524 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGetEx.pgc"



  //Make the query statement.
  i=0;
  for(;;) {
    { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "fetch reqcursor", ECPGt_EOIT, 
	ECPGt_long_long,&(ds_index),(long)1,(long)1,sizeof(long long), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_varchar,&(online_loc),(long)81,(long)1,sizeof(struct varchar_15), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_varchar,&(online_status),(long)5,(long)1,sizeof(struct varchar_16), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_varchar,&(archive_status),(long)5,(long)1,sizeof(struct varchar_17), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_varchar,&(offsite_ack),(long)5,(long)1,sizeof(struct varchar_18), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_varchar,&(history_comment),(long)81,(long)1,sizeof(struct varchar_19), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_varchar,&(owning_series),(long)81,(long)1,sizeof(struct varchar_20), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_int,&(storage_group),(long)1,(long)1,sizeof(int), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_double,&(bytes),(long)1,(long)1,sizeof(double), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_long_long,&(create_sumid),(long)1,(long)1,sizeof(long long), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_varchar,&(creat_date),(long)32,(long)1,sizeof(struct varchar_21), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_varchar,&(username),(long)11,(long)1,sizeof(struct varchar_22), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_varchar,&(arch_tape),(long)21,(long)1,sizeof(struct varchar_23), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_int,&(arch_tape_fn),(long)1,(long)1,sizeof(int), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_varchar,&(arch_tape_date),(long)32,(long)1,sizeof(struct varchar_24), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_varchar,&(safe_tape),(long)21,(long)1,sizeof(struct varchar_25), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_int,&(safe_tape_fn),(long)1,(long)1,sizeof(int), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_varchar,&(safe_tape_date),(long)32,(long)1,sizeof(struct varchar_26), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_varchar,&(effective_date),(long)20,(long)1,sizeof(struct varchar_14), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_int,&(pa_status),(long)1,(long)1,sizeof(int), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_int,&(pa_substatus),(long)1,(long)1,sizeof(int), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EORT);
#line 530 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGetEx.pgc"

if (sqlca.sqlcode == ECPG_NOT_FOUND) goto end_fetch;
#line 530 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGetEx.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 530 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGetEx.pgc"


  //printk("i=%d ds_index=%lld\n", i, ds_index); //!!TEMP
  sprintf(key, "%d", i);
  sprintf(keyindex, "%lld", ds_index);
  setkey_strU(&L_key_index, keyindex, key); //the ds_index key gives u the order
  setkey_uint64U(&L_ds_index, key, (unsigned long long)ds_index);
  setkey_strU(&L_online_loc, key, online_loc.arr);
  setkey_strU(&L_online_status, key, online_status.arr);
  setkey_strU(&L_archive_status, key, archive_status.arr);
  setkey_strU(&L_offsite_ack, key, offsite_ack.arr);
  setkey_strU(&L_history_comment, key, history_comment.arr);
  setkey_strU(&L_owning_series, key, owning_series.arr);
  setkey_intU(&L_storage_group, key, storage_group);
  setkey_doubleU(&L_bytes, key, bytes);
  setkey_uint64U(&L_create_sumid, key, create_sumid);
  setkey_strU(&L_creat_date, key, creat_date.arr);
  setkey_strU(&L_username, key, username.arr);
  setkey_strU(&L_arch_tape, key, arch_tape.arr);
  setkey_intU(&L_arch_tape_fn, key, arch_tape_fn);
  setkey_strU(&L_arch_tape_date, key, arch_tape_date.arr);
  setkey_strU(&L_safe_tape, key, safe_tape.arr);
  setkey_intU(&L_safe_tape_fn, key, safe_tape_fn);
  setkey_strU(&L_safe_tape_date, key, safe_tape_date.arr);
  setkey_strU(&L_effective_date, key, effective_date.arr);
  setkey_intU(&L_pa_status, key, pa_status);
  setkey_intU(&L_pa_substatus, key, pa_substatus);
  
sqlerror:
//        printk("sqlerror in SUMLIB_InfoGetArray for i=%d ds_index = %lld\n",
//		 i, ds_index);
    i++;
  }
end_fetch:
        { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "close reqcursor", ECPGt_EOIT, ECPGt_EORT);
#line 564 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGetEx.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 564 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGetEx.pgc"


  char *thekey;

  reqcnt = params->reqcnt;
  suptr = params->sunums;
  //set up answers in original request order and see if any blank results
  for(i=0; i < reqcnt; i++) {
    ds_index = (long long)(suptr[i]);
    sprintf(keyindex, "%lld", ds_index);
    if(findkeyU(L_key_index, keyindex)) {
      thekey = getkey_strU(L_key_index, keyindex);
      if(filemode == 0) {	//binary mode
	sit->sunum = (unsigned long long)ds_index;
	sprintf(sit->online_loc, "%s", GETKEY_strU(L_online_loc, thekey));
        sprintf(sit->online_status, "%s", GETKEY_strU(L_online_status, thekey));
        sprintf(sit->archive_status, "%s", GETKEY_strU(L_archive_status, thekey));
        sprintf(sit->offsite_ack, "%s", GETKEY_strU(L_offsite_ack, thekey));
        snprintf(sit->history_comment, 80, "%s", GETKEY_strU(L_history_comment, thekey));
        sprintf(sit->owning_series, "%s", GETKEY_strU(L_owning_series, thekey));
        sit->storage_group = getkey_intU(L_storage_group, thekey);
        sit->bytes = getkey_doubleU(L_bytes, thekey);
        sprintf(sit->creat_date, "%s", GETKEY_strU(L_creat_date, thekey));
        sprintf(sit->username, "%s", GETKEY_strU(L_username, thekey));
        sprintf(sit->arch_tape, "%s", GETKEY_strU(L_arch_tape, thekey));
        sit->arch_tape_fn = getkey_intU(L_arch_tape_fn, thekey);
        sprintf(sit->arch_tape_date, "%s", GETKEY_strU(L_arch_tape_date, thekey));
        sprintf(sit->safe_tape, "%s", GETKEY_strU(L_safe_tape, thekey));
        sit->safe_tape_fn = getkey_intU(L_safe_tape_fn, thekey);
        sprintf(sit->safe_tape_date, "%s", GETKEY_strU(L_safe_tape_date, thekey));
        sit->pa_status = getkey_intU(L_pa_status, thekey);
        sit->pa_substatus = getkey_intU(L_pa_substatus, thekey);
        sprintf(sit->effective_date, "%s", GETKEY_strU(L_effective_date, thekey));
        sitX = sit;
        sitX++;
        sit->next = sitX;
        //fwrite(sit, sizeof(SUM_info_t), 1, ifp); //!!TEMP
        sit = sitX;
      }
      else {
        fprintf(ifp, "ds_index= %lld\n", ds_index);
        fprintf(ifp, "online_loc= %s\n", GETKEY_strU(L_online_loc, thekey));
        fprintf(ifp, "online_status= %s\n", GETKEY_strU(L_online_status, thekey));
        fprintf(ifp, "archive_status= %s\n", GETKEY_strU(L_archive_status, thekey));
        fprintf(ifp, "offsite_ack= %s\n", GETKEY_strU(L_offsite_ack, thekey));
        fprintf(ifp, "history_comment= %s\n", GETKEY_strU(L_history_comment, thekey));
        fprintf(ifp, "owning_series= %s\n", GETKEY_strU(L_owning_series, thekey));
        fprintf(ifp, "storage_group= %d\n", getkey_intU(L_storage_group, thekey));
        fprintf(ifp, "bytes= %g\n", getkey_doubleU(L_bytes, thekey));
        fprintf(ifp, "creat_date= %s\n", GETKEY_strU(L_creat_date, thekey));
        fprintf(ifp, "username= %s\n", GETKEY_strU(L_username, thekey));
        fprintf(ifp, "arch_tape= %s\n", GETKEY_strU(L_arch_tape, thekey));
        fprintf(ifp, "arch_tape_fn= %d\n", getkey_intU(L_arch_tape_fn, thekey));
        fprintf(ifp, "arch_tape_date= %s\n", GETKEY_strU(L_arch_tape_date, thekey));
        fprintf(ifp, "safe_tape= %s\n", GETKEY_strU(L_safe_tape, thekey));
        fprintf(ifp, "safe_tape_fn= %d\n", getkey_intU(L_safe_tape_fn, thekey));
        fprintf(ifp, "safe_tape_date= %s\n", GETKEY_strU(L_safe_tape_date, thekey));
        fprintf(ifp, "pa_status= %d\n", getkey_intU(L_pa_status, thekey));
        fprintf(ifp, "pa_substatus= %d\n", getkey_intU(L_pa_substatus, thekey));
        fprintf(ifp, "effective_date= %s\n", GETKEY_strU(L_effective_date, thekey));
      }
      free(thekey);
    }
    else {			//sunum not in query results
      if(filemode == 0) {	//binary mode
        sit->sunum = (unsigned long long)ds_index;
        sit->pa_status = 0;
        sit->pa_substatus = 0;
        sprintf(sit->effective_date, "N/A");
        sitX = sit;
        sitX++;
        sit->next = sitX;
        //fwrite(sit, sizeof(SUM_info_t), 1, ifp); //!!TEMP
        sit = sitX;
      }
      else {			//text mode
        fprintf(ifp, "ds_index= %lld\n", ds_index);
        fprintf(ifp, "pa_status= %d\n", 0);
        fprintf(ifp, "pa_substatus= %d\n", 0);
        fprintf(ifp, "effective_date= %s\n", "N/A");
      }
    }
  }
      if(filemode == 0) {
        fwrite(sitsv, sizeof(SUM_info_t), reqcnt, ifp);
        free(sitsv);
      }
        fclose(ifp);
        free(stmt);
        freekeylistU(&L_ds_index);
        freekeylistU(&L_online_loc);
        freekeylistU(&L_online_status);
        freekeylistU(&L_archive_status);
        freekeylistU(&L_offsite_ack);
        freekeylistU(&L_history_comment);
        freekeylistU(&L_owning_series);
        freekeylistU(&L_storage_group);
        freekeylistU(&L_bytes);
        freekeylistU(&L_create_sumid);
        freekeylistU(&L_creat_date);
        freekeylistU(&L_username);
        freekeylistU(&L_arch_tape);
        freekeylistU(&L_arch_tape_fn);
        freekeylistU(&L_arch_tape_date);
        freekeylistU(&L_safe_tape);
        freekeylistU(&L_safe_tape_fn);
        freekeylistU(&L_safe_tape_date);
        freekeylistU(&L_effective_date);
        freekeylistU(&L_pa_status);
        freekeylistU(&L_pa_substatus);
        freekeylistU(&L_key_index);
        *mode = filemode;
        //EXEC SQL CLOSE reqcursor2;
    	{ ECPGtrans(__LINE__, NULL, "commit work");
#line 677 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGetEx.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 677 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGetEx.pgc"

	return NO_ERROR;
}

