/* Processed by ecpg (4.10.0) */
/* These include files are added by the preprocessor */
#include <ecpglib.h>
#include <ecpgerrno.h>
#include <sqlca.h>
/* End of automatic include section */

#line 1 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDoX.pgc"
/* SUMLIB_RmDoX.pc
 * Called by sum_rm to find expired dirs and remove them.
 */
#include <SUM.h>
#include <sum_rpc.h>
#include <soi_error.h>
#include <printk.h>


int DS_Rm_Commit()
{
  { ECPGtrans(__LINE__, NULL, "commit");}
#line 12 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDoX.pgc"

  printk("Commit done at end of sum_rm cycle\n");
  return(0);
}

int DS_RmDoX(char *name, double bytesdel)
{
/* exec sql begin declare section */
   
   
   
     
     
   
   

#line 20 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDoX.pgc"
 char stmt [ 256 ] ;
 
#line 21 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDoX.pgc"
  struct varchar_1  { int len; char arr[ 80 ]; }  l_wd ;
 
#line 22 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDoX.pgc"
  struct varchar_2  { int len; char arr[ 20 ]; }  l_eff_date ;
 
#line 23 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDoX.pgc"
 long long l_sumid ;
 
#line 24 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDoX.pgc"
 long long ds_index ;
 
#line 25 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDoX.pgc"
 int l_archsub ;
 
#line 26 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDoX.pgc"
 double l_bytes ;
/* exec sql end declare section */
#line 27 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDoX.pgc"

  double bytesdeleted, rmbytes;
  int i, j;

  typedef struct palloc_cursor {
    char p_wd[80];
    uint64_t p_sumid;
    double p_bytes;
    char p_effdate[20];
    uint64_t p_ds_index;
    int p_archsub;
  } PALLOC_CURSOR;
  //PALLOC_CURSOR pcursor[1600];
  PALLOC_CURSOR pcursor[60000];

//char STMT[] = "SELECT WD, SUMID, BYTES, EFFECTIVE_DATE, DS_INDEX, ARCHIVE_SUBSTATUS FROM SUM_PARTN_ALLOC where STATUS=2 and WD like '%s/%s' order by effective_date limit 60000";
//char STMT[] = "SELECT WD, SUMID, BYTES, EFFECTIVE_DATE, DS_INDEX, ARCHIVE_SUBSTATUS FROM SUM_PARTN_ALLOC where STATUS=2 and WD like '%s/%s' order by effective_date limit 600";
//char STMT[] = "SELECT WD, SUMID, BYTES, EFFECTIVE_DATE, DS_INDEX, ARCHIVE_SUBSTATUS FROM SUM_PARTN_ALLOC where STATUS=2 and WD like '%s/%s' order by effective_date limit 1000";
char STMT[] = "SELECT WD, SUMID, BYTES, EFFECTIVE_DATE, DS_INDEX, ARCHIVE_SUBSTATUS FROM SUM_PARTN_ALLOC where STATUS=2 and WD like '%s/%s' and effective_date < '%s' order by effective_date limit 4000";


#line 1 "/usr/include/postgresql/sqlca.h"
#ifndef POSTGRES_SQLCA_H
#define POSTGRES_SQLCA_H

#ifndef PGDLLIMPORT
#if  defined(WIN32) || defined(__CYGWIN__)
#define PGDLLIMPORT __declspec (dllimport)
#else
#define PGDLLIMPORT
#endif   /* __CYGWIN__ */
#endif   /* PGDLLIMPORT */

#define SQLERRMC_LEN	150

#ifdef __cplusplus
extern		"C"
{
#endif

struct sqlca_t
{
	char		sqlcaid[8];
	long		sqlabc;
	long		sqlcode;
	struct
	{
		int			sqlerrml;
		char		sqlerrmc[SQLERRMC_LEN];
	}			sqlerrm;
	char		sqlerrp[8];
	long		sqlerrd[6];
	/* Element 0: empty						*/
	/* 1: OID of processed tuple if applicable			*/
	/* 2: number of rows processed				*/
	/* after an INSERT, UPDATE or				*/
	/* DELETE statement					*/
	/* 3: empty						*/
	/* 4: empty						*/
	/* 5: empty						*/
	char		sqlwarn[8];
	/* Element 0: set to 'W' if at least one other is 'W'	*/
	/* 1: if 'W' at least one character string		*/
	/* value was truncated when it was			*/
	/* stored into a host variable.             */

	/*
	 * 2: if 'W' a (hopefully) non-fatal notice occurred
	 */	/* 3: empty */
	/* 4: empty						*/
	/* 5: empty						*/
	/* 6: empty						*/
	/* 7: empty						*/

	char		sqlstate[5];
};

struct sqlca_t *ECPGget_sqlca(void);

#ifndef POSTGRES_ECPG_INTERNAL
#define sqlca (*ECPGget_sqlca())
#endif

#ifdef __cplusplus
}
#endif

#endif

#line 47 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDoX.pgc"


  printk("DS_RmDoX\n");  
  bytesdeleted = 0.0;
  sprintf(stmt, STMT, name, "%%", get_effdate(0));
  //printk("!!TEMP: stmt = %s\n", stmt);

  /* exec sql whenever sqlerror  goto  sqlerror ; */
#line 54 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDoX.pgc"

  { ECPGprepare(__LINE__, NULL, 0, "query", stmt);
#line 55 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDoX.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 55 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDoX.pgc"

  /* declare reqcursor cursor for $1 */
#line 56 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDoX.pgc"

  { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "declare reqcursor cursor for $1", 
	ECPGt_char_variable,(ECPGprepared_statement(NULL, "query", __LINE__)),(long)1,(long)1,(1)*sizeof(char), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EOIT, ECPGt_EORT);
#line 57 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDoX.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 57 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDoX.pgc"

  /* exec sql whenever not found  goto  end_fetch ; */
#line 58 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDoX.pgc"
 
	
/* Warn about SUMDC code commented out below */
//printk("WARN: tmp noop of offsite_ack check\n"); /* !!!TEMP */

  for(i=0; ; i++ ){
    { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "fetch reqcursor", ECPGt_EOIT, 
	ECPGt_varchar,&(l_wd),(long)80,(long)1,sizeof(struct varchar_1), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_long_long,&(l_sumid),(long)1,(long)1,sizeof(long long), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_double,&(l_bytes),(long)1,(long)1,sizeof(double), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_varchar,&(l_eff_date),(long)20,(long)1,sizeof(struct varchar_2), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_long_long,&(ds_index),(long)1,(long)1,sizeof(long long), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_int,&(l_archsub),(long)1,(long)1,sizeof(int), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EORT);
#line 64 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDoX.pgc"

if (sqlca.sqlcode == ECPG_NOT_FOUND) goto end_fetch;
#line 64 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDoX.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 64 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDoX.pgc"

    /* must save results as can't put other sql calls in this loop */
    strcpy(pcursor[i].p_wd, l_wd.arr);
    pcursor[i].p_sumid = (unsigned long long)l_sumid;
    pcursor[i].p_bytes = l_bytes;
    strcpy(pcursor[i].p_effdate, l_eff_date.arr);
    pcursor[i].p_ds_index = (unsigned long long)ds_index;
    pcursor[i].p_archsub = l_archsub;
  }

end_fetch:
    { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "close reqcursor", ECPGt_EOIT, ECPGt_EORT);
#line 75 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDoX.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 75 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDoX.pgc"

    /* exec sql whenever not found  goto  end_fetch2 ; */
#line 76 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDoX.pgc"


    for(j=0; j < i; j++) {
#ifdef SUMDC
      /* only rm those with Offsite_Ack, and Safe_Tape */

/* !!!TEMP noop out below for testing with DDS */
      ds_index = (long long)(pcursor[j].p_ds_index);
      { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "select DS_INDEX from sum_main where DS_INDEX = $1  and OFFSITE_ACK = 'Y' and SAFE_TAPE is not null", 
	ECPGt_long_long,&(ds_index),(long)1,(long)1,sizeof(long long), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EOIT, 
	ECPGt_long_long,&(ds_index),(long)1,(long)1,sizeof(long long), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EORT);
#line 86 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDoX.pgc"

if (sqlca.sqlcode == ECPG_NOT_FOUND) goto end_fetch2;
#line 86 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDoX.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 86 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDoX.pgc"

/* END of noop */

#endif

      if(DS_RmNowX(pcursor[j].p_wd, pcursor[j].p_sumid, pcursor[j].p_bytes, 
		pcursor[j].p_effdate, 
		pcursor[j].p_ds_index, pcursor[j].p_archsub, &rmbytes)) {
        //EXEC SQL ROLLBACK WORK;
        printk("Error in DS_RmDoX() call to DS_RmNowX() ds_index=%llu\n",
		pcursor[j].p_ds_index);
        //return DS_DATA_QRY;
        continue;
      }
      bytesdeleted += rmbytes;     /* add what we deleted so far */
      if(bytesdeleted >= bytesdel) break; //don't delete any more
end_fetch2:
      continue;
    }
    if(bytesdeleted != 0.0) {
      printk("bytes deleted=%e\n", bytesdeleted);
    }
    //EXEC SQL COMMIT;
    return NO_ERROR;
	
sqlerror:
    printk("Error in DS_RmDoX\n");
    printk("% .70s \n", sqlca.sqlerrm.sqlerrmc);
    //EXEC SQL ROLLBACK WORK;
    return DS_DATA_QRY;
}

int DS_RmNowX(char *wd, uint64_t sumid, double bytes, char *effdate,
		uint64_t ds_index, int archsub, double *rmbytes) {
		
/* exec sql begin declare section */
   
   
     
   

#line 122 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDoX.pgc"
  struct varchar_3  { int len; char arr[ 80 ]; }  target ;
 
#line 123 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDoX.pgc"
 int l_count ;
 
#line 124 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDoX.pgc"
 long long l_ds_index ;
 
#line 125 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDoX.pgc"
 char stmt [ 65536 ] ;
/* exec sql end declare section */
#line 126 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDoX.pgc"

  FILE *rxfp;
  uint64_t eff_date, today_date;
  char rwd[80], rectxt[128], line[128], seriesname[128];
  char *rootdir, *cptr, *cptr1, *token, *effd;

  /* exec sql whenever sqlerror  goto  sqlrmerror ; */
#line 132 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDoX.pgc"

  /* exec sql whenever not found  goto  sqlrmerror ; */
#line 133 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDoX.pgc"
 

    effd = get_effdate(0);
    today_date = (uint64_t)atol(effd);
    free(effd);
    l_ds_index = (long long)ds_index;
    { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "select count ( * ) from SUM_PARTN_ALLOC where DS_INDEX = $1  and status = 8", 
	ECPGt_long_long,&(l_ds_index),(long)1,(long)1,sizeof(long long), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EOIT, 
	ECPGt_int,&(l_count),(long)1,(long)1,sizeof(int), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EORT);
#line 140 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDoX.pgc"

if (sqlca.sqlcode == ECPG_NOT_FOUND) goto sqlrmerror;
#line 140 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDoX.pgc"

if (sqlca.sqlcode < 0) goto sqlrmerror;}
#line 140 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDoX.pgc"

    /* !!!TBD lock the table before determine if it's DARO */
    if(l_count) {
       printk("%s is open DARO\n", wd);
       *rmbytes = 0.0;
       //EXEC SQL COMMIT;
       return(0);		/* skip this dir opend for read */
    }
    eff_date = (uint64_t)atol(effdate);

    rootdir = strcpy(rwd, wd);
    //contin with del if garbage date > year 30000
    if(eff_date < 3000000000000) { //yyyymmddhhss
      if(eff_date > today_date) {
        *rmbytes = 0.0;
        //EXEC SQL COMMIT;
        return(0);
      }
    }
    /* remove del pend entry from sum_partn_alloc tbl */
    if(NC_PaUpdate
        (wd, sumid, bytes,DADP,0,0,0,0,0,0,1))
    {
      printk("Err: NC_PaUpdate(%s,%ld,%e,DADP,0,0,0, ...)to rm from dp list\n", 
		wd,sumid,bytes);
      printk("  ??This is how we got the info in the first place!\n");
    }
    if(!(cptr = strstr(rootdir+1, "/D"))) {
      printk("The wd=%s doesn't have a /.../Dxxx term!\n",rootdir);
      *rmbytes = 0.0;
      //EXEC SQL COMMIT;
      return(0);
    }
    if((cptr1 = strstr(cptr+1, "/"))) {
      *cptr1 = (char)NULL;		/* make like /SUM1/D1234 */
    }
    printk("Removing %s\n", wd);
    printk("eff_date=%lu today_date=%lu\n", eff_date, today_date);
    if(ds_index != 0) {         /* Don't take offline if 0*/
    if(SUM_StatOffline(ds_index)) {
      printk("Err: SUM_StatOffline(%llu, ...)\n", ds_index);
    }
    }
    if(archsub == DAAEDDP || archsub == DADPDELSU) { /* a temporary dataset */
      printk("Removing sum_main for ds_index = %llu\n", ds_index);
      if(DS_SumMainDelete(ds_index)) {
        printk("**Err: DS_SumMainDelete(%llu)\n", ds_index);
      }
    } 
#ifndef SUMDC
//goto BYPASS;		/* oct 30, 2012 bypass deleting DRMS records for JILA */
    //Note: Records.txt file is not on the datacapture nodes
    if(archsub != DADPDELSU) {
      sprintf(rectxt, "%s/Records.txt", wd);
      if(!(rxfp=fopen(rectxt, "r"))) {
        printk("**Err: DS_RmNowX() can't open %s\n", rectxt);
        //NOTE: get this error for DRMS log directories w/no Records.txt
      }
      else {
        while(fgets(line, 256, rxfp)) {   /* get Records.txt file lines */
          if(!strstr(line, "DELETE_SLOTS_RECORDS")) { //must be 1st line
            break;
          }
          else {		//delete all record #s
            while(fgets(line, 256, rxfp)) {
              if(strstr(line, "series=")) {
                token=(char *)strtok(line, "=\n");
                if(token=(char *)strtok(NULL, "\n")) {
                  strcpy(seriesname, token);
                  sprintf(stmt, "delete from %s where recnum in (", 
					seriesname);
                }
                else {
                  printk("DS_RmNowX() bad Records.txt file\n");
                  fclose(rxfp);
                  goto sqlrmerror;
                }
              }
              else if(strstr(line, "slot")) {
                continue;
              }
              else {		//this is line like: 0       425490
                token=(char *)strtok(line, "\t");
                if(token=(char *)strtok(NULL, "\n")) {
                  sprintf(stmt, "%s%s,", stmt, token);
                }
                else {
                  printk("DS_RmNowX() bad Records.txt wd=%s\n", wd);
                  fclose(rxfp);
                  goto sqlrmerror;
                }
              }
            }
            cptr = rindex(stmt, ',');
            if(cptr) *cptr = '\0';
            sprintf(stmt, "%s);", stmt);
            printk("Records.txt statement is:\n%s\n", stmt);
            //sprintf(target.arr,  "%s@%s:%s", DBNAME, SERVER, DRMSPGPORT);
            sprintf(target.arr,  "%s@%s:%s", DBNAME, SERVER, DRMSPGPORT);
            target.len = strlen(target.arr);

            /* exec sql whenever sqlerror  goto  sqljsocerror0 ; */
#line 241 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDoX.pgc"
 //catch connect err
            //Typically: EXEC SQL CONNECT TO jsoc@hmidb:5432;
            { ECPGconnect(__LINE__, 0, target.arr , NULL, NULL , "jsocdb", 0); 
#line 243 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDoX.pgc"

if (sqlca.sqlcode < 0) goto sqljsocerror0;}
#line 243 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDoX.pgc"

            /* exec sql whenever sqlerror  goto  sqljsocerror1 ; */
#line 244 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDoX.pgc"

            { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_exec_immediate, stmt, ECPGt_EOIT, ECPGt_EORT);
#line 245 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDoX.pgc"

if (sqlca.sqlcode < 0) goto sqljsocerror1;}
#line 245 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDoX.pgc"

            { ECPGtrans(__LINE__, NULL, "commit");
#line 246 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDoX.pgc"

if (sqlca.sqlcode < 0) goto sqljsocerror1;}
#line 246 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDoX.pgc"

            { ECPGdisconnect(__LINE__, "jsocdb");
#line 247 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDoX.pgc"

if (sqlca.sqlcode < 0) goto sqljsocerror1;}
#line 247 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDoX.pgc"

            /* exec sql whenever sqlerror  goto  sqlrmerror ; */
#line 248 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDoX.pgc"

          }
        }
        fclose(rxfp);
      }
    }
#endif
BYPASS:
    if(rmdirs(wd, rootdir)) {
      printk("Cannot rm %s\n", wd);
    }
    *rmbytes = bytes;
    //EXEC SQL COMMIT;
    return(0);

sqlrmerror:
    printk("Error in DS_RmNowX\n");
    printk("% .70s \n", sqlca.sqlerrm.sqlerrmc);
    //EXEC SQL ROLLBACK WORK;
    return DS_DATA_QRY;

sqljsocerror0:
    printk("Error in DS_RmNowX() for del from Records.txt\n");
    //printk("Can't connect to jsoc@hmidb:5432\n");
    //printk("Can't connect to %s@%s:%s\n", DBNAME, SERVER, DRMSPGPORT);
    printk("Can't connect to %s@%s:%s\n", DBNAME, SERVER, DRMSPGPORT);
    printk("% .70s \n", sqlca.sqlerrm.sqlerrmc);
    //Assume error was in the connect so don't disconnect
    //EXEC SQL DISCONNECT;
    //EXEC SQL ROLLBACK WORK;
    fclose(rxfp);
    return DS_DATA_QRY;

sqljsocerror1:
    printk("Error in DS_RmNowX() for del from Records.txt\n");
    //printk("Error was after the connect to jsoc@hmidb:5432\n");
    //printk("Error was after the connect to %s@%s:%s\n", DBNAME, SERVER, DRMSPGPORT);
    printk("Error was after the connect to %s@%s:%s\n", DBNAME, SERVER, DRMSPGPORT);
    printk("% .70s \n", sqlca.sqlerrm.sqlerrmc);
    { ECPGdisconnect(__LINE__, "CURRENT");
#line 287 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDoX.pgc"

if (sqlca.sqlcode < 0) goto sqlrmerror;}
#line 287 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDoX.pgc"

    //EXEC SQL ROLLBACK WORK;
    fclose(rxfp);
    //NEW Oct 30,2012 for ignore JILA err on delete record from DRMS
    //goto BYPASS;
    return DS_DATA_QRY;
}

//Test only function used by mainconn.c in base/sums/apps
int DS_Conn_test()
{
/* exec sql begin declare section */
   
   

#line 299 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDoX.pgc"
  struct varchar_4  { int len; char arr[ 80 ]; }  target ;
 
#line 300 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDoX.pgc"
 char stmt [ 256 ] ;
/* exec sql end declare section */
#line 301 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDoX.pgc"

  char conncmd[128];

  sprintf(stmt, "%s;", "delete from aia_test.lev1p5 where recnum in (68917629)");

//first connect to jsoc_sum before connect to jsoc:5432
            sprintf(target.arr,  "%s@%s:%s", "jsoc_sums", "hmidb", "5434");
            target.len = strlen(target.arr);
            sprintf(conncmd, "EXEC SQL CONNECT TO %s", target.arr);
            printf("connect command will be: %s\n", conncmd);
            /* exec sql whenever sqlerror  goto  sqljsocerror0 ; */
#line 311 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDoX.pgc"
 //catch connect err
            { ECPGconnect(__LINE__, 0, target.arr , NULL, NULL , NULL, 0); 
#line 312 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDoX.pgc"

if (sqlca.sqlcode < 0) goto sqljsocerror0;}
#line 312 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDoX.pgc"

            printf("Connected to jsoc_sums. Will now disconnect\n");
            { ECPGdisconnect(__LINE__, "CURRENT");
#line 314 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDoX.pgc"

if (sqlca.sqlcode < 0) goto sqljsocerror0;}
#line 314 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDoX.pgc"


            printf("statement is:\n%s\n", stmt);
            sprintf(target.arr,  "%s@%s:%s", "jsoc", "hmidb", "5432");
            target.len = strlen(target.arr);
            sprintf(conncmd, "EXEC SQL CONNECT TO %s@%s:%s", "jsoc", "hmidb", "5432");
            printf("connect command will be: %s\n", conncmd);
            /* exec sql whenever sqlerror  goto  sqljsocerror0 ; */
#line 321 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDoX.pgc"
 //catch connect err
            //Typically: EXEC SQL CONNECT TO jsoc@hmidb:5432;
            { ECPGconnect(__LINE__, 0, target.arr , NULL, NULL , NULL, 0); 
#line 323 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDoX.pgc"

if (sqlca.sqlcode < 0) goto sqljsocerror0;}
#line 323 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDoX.pgc"

            /* exec sql whenever sqlerror  goto  sqljsocerror1 ; */
#line 324 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDoX.pgc"

            { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_exec_immediate, stmt, ECPGt_EOIT, ECPGt_EORT);
#line 325 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDoX.pgc"

if (sqlca.sqlcode < 0) goto sqljsocerror1;}
#line 325 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDoX.pgc"

            { ECPGtrans(__LINE__, NULL, "commit");
#line 326 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDoX.pgc"

if (sqlca.sqlcode < 0) goto sqljsocerror1;}
#line 326 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDoX.pgc"

            { ECPGdisconnect(__LINE__, "CURRENT");
#line 327 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDoX.pgc"

if (sqlca.sqlcode < 0) goto sqljsocerror1;}
#line 327 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDoX.pgc"

            return(0);

sqljsocerror0:
    printf("Error in connect!\n");
    printf("% .70s \n", sqlca.sqlerrm.sqlerrmc);
    return(1);

sqljsocerror1:
    printf("Error delete recnum\n");
    printf("% .70s \n", sqlca.sqlerrm.sqlerrmc);
    { ECPGdisconnect(__LINE__, "CURRENT");
#line 338 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDoX.pgc"

if (sqlca.sqlcode < 0) goto sqljsocerror1;}
#line 338 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDoX.pgc"

    return(2);

}
