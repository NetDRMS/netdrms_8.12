/* Processed by ecpg (4.10.0) */
/* These include files are added by the preprocessor */
#include <ecpglib.h>
#include <ecpgerrno.h>
#include <sqlca.h>
/* End of automatic include section */

#line 1 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Open.pgc"
/* SUM_open.pc
 * Return a unique id (using Oracle's Sequence Generator) for a user 
 * opening with the SUMS. Return 0 on error.
 */
#include <SUM.h>
#include <sum_rpc.h>
#include <printk.h>


SUMID_t SUMLIB_Open()
{
  typedef  unsigned int SUMID_t ;
#line 12 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Open.pgc"


/* exec sql begin declare section */
   
   

#line 15 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Open.pgc"
 SUMID_t uid ;
 
#line 16 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Open.pgc"
  struct varchar_1  { int len; char arr[ 32 ]; }  l_date ;
/* exec sql end declare section */
#line 17 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Open.pgc"

 
    /* exec sql whenever sqlerror  goto  sqlerror ; */
#line 19 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Open.pgc"


    { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "select NEXTVAL ( 'SUM_SEQ' )", ECPGt_EOIT, 
	ECPGt_unsigned_int,&(uid),(long)1,(long)1,sizeof(unsigned int), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EORT);
#line 21 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Open.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 21 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Open.pgc"

    sprintf(l_date.arr, "%s", get_datetime());
    l_date.len = strlen(l_date.arr);
    { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "insert into SUM_OPEN ( SUMID , OPEN_DATE ) values ( $1  , $2  )", 
	ECPGt_unsigned_int,&(uid),(long)1,(long)1,sizeof(unsigned int), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_varchar,&(l_date),(long)32,(long)1,sizeof(struct varchar_1), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EOIT, ECPGt_EORT);
#line 25 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Open.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 25 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Open.pgc"

    { ECPGtrans(__LINE__, NULL, "commit");
#line 26 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Open.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 26 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Open.pgc"

    return(uid);

sqlerror:
    printk("Error in SUMLIB_Open() call \n");
    printk("% .70s \n", sqlca.sqlerrm.sqlerrmc);
    { ECPGtrans(__LINE__, NULL, "rollback work");
#line 32 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Open.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 32 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Open.pgc"

    return(0);
}
