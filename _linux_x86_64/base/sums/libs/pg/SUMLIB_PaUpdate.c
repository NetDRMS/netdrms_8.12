/* Processed by ecpg (4.10.0) */
/* These include files are added by the preprocessor */
#include <ecpglib.h>
#include <ecpgerrno.h>
#include <sqlca.h>
/* End of automatic include section */

#line 1 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PaUpdate.pgc"
/* NC_PaUpdate.pc 
*/
#include <SUM.h>
#include <sum_rpc.h>
#include <soi_error.h>
#include <printk.h>

/* Called to insert/delete an entry from the SUM_PARTN_ALLOC db table.
*/
int NC_PaUpdate
  (char *wd, uint64_t uid, double bytes, int status, int archsub, 
   char *eff_date, int gpid, int sid, uint64_t ds_index, int flg, int commit);

int NC_PaUpdate
  (char *wd, uint64_t uid, double bytes, int status, int archsub, 
   char *eff_date, int gpid, int sid, uint64_t ds_index, int flg, int commit)
{
/* exec sql begin declare section */
   
   
     
    
   	  
  	   
        
       
     

#line 19 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PaUpdate.pgc"
  struct varchar_1  { int len; char arr[ 80 ]; }  l_wd ;
 
#line 20 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PaUpdate.pgc"
  struct varchar_2  { int len; char arr[ 20 ]; }  l_eff_date ;
 
#line 21 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PaUpdate.pgc"
 long long l_uid ;
 
#line 22 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PaUpdate.pgc"
 double l_bytes ;
 
#line 23 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PaUpdate.pgc"
 int l_status ;
 
#line 24 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PaUpdate.pgc"
 int l_archsub ;
 
#line 25 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PaUpdate.pgc"
 int l_gpid ;
 
#line 26 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PaUpdate.pgc"
 int l_sid ;
 
#line 27 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PaUpdate.pgc"
 long long l_ds_index ;
/* exec sql end declare section */
#line 28 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PaUpdate.pgc"


   sprintf(l_wd.arr,"%s", wd);
   l_wd.len = strlen(l_wd.arr);
   if(eff_date) {    /* check if the eff_date is null */
	sprintf(l_eff_date.arr,"%s", eff_date); 
        l_eff_date.len = strlen(l_eff_date.arr);
   } else {		/* if eff_date is null then l_eff_date string 0 */
	sprintf(l_eff_date.arr,"%s", "0");  
        l_eff_date.len = strlen(l_eff_date.arr);
   }
   l_uid = (long long)uid;
   l_bytes = bytes;
   l_status = status;
   l_archsub = archsub;
   l_gpid = gpid;
   l_sid = sid;
   l_ds_index = (long long)ds_index;
   if(!strcmp(l_wd.arr, "")) {
           return DS_PALC_ERR;
   }

    /* exec sql whenever sqlerror  goto  sqlerror ; */
#line 50 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PaUpdate.pgc"

    /* exec sql whenever not found  goto  sqlerror ; */
#line 51 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PaUpdate.pgc"


    if(flg == 1){

      if(status == DADP) {	/* make sure don't have 2 del pend entries */
        /* exec sql whenever sqlerror  goto  sqlinsert ; */
#line 56 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PaUpdate.pgc"

        /* exec sql whenever not found  goto  sqlinsert ; */
#line 57 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PaUpdate.pgc"

        { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "delete from SUM_PARTN_ALLOC where DS_INDEX = $1  and STATUS = $2 ", 
	ECPGt_long_long,&(l_ds_index),(long)1,(long)1,sizeof(long long), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_int,&(l_status),(long)1,(long)1,sizeof(int), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EOIT, ECPGt_EORT);
#line 60 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PaUpdate.pgc"

if (sqlca.sqlcode == ECPG_NOT_FOUND) goto sqlinsert;
#line 60 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PaUpdate.pgc"

if (sqlca.sqlcode < 0) goto sqlinsert;}
#line 60 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PaUpdate.pgc"

        /* exec sql whenever sqlerror  goto  sqlerror ; */
#line 61 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PaUpdate.pgc"

        /* exec sql whenever not found  goto  sqlerror ; */
#line 62 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PaUpdate.pgc"

      }
sqlinsert:
	{ ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "insert into SUM_PARTN_ALLOC ( WD , SUMID , STATUS , BYTES , EFFECTIVE_DATE , ARCHIVE_SUBSTATUS , GROUP_ID , SAFE_ID , DS_INDEX ) values ( $1  , $2  , $3  , $4  , $5  , $6  , $7  , $8  , $9  )", 
	ECPGt_varchar,&(l_wd),(long)80,(long)1,sizeof(struct varchar_1), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_long_long,&(l_uid),(long)1,(long)1,sizeof(long long), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_int,&(l_status),(long)1,(long)1,sizeof(int), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_double,&(l_bytes),(long)1,(long)1,sizeof(double), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_varchar,&(l_eff_date),(long)20,(long)1,sizeof(struct varchar_2), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_int,&(l_archsub),(long)1,(long)1,sizeof(int), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_int,&(l_gpid),(long)1,(long)1,sizeof(int), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_int,&(l_sid),(long)1,(long)1,sizeof(int), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_long_long,&(l_ds_index),(long)1,(long)1,sizeof(long long), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EOIT, ECPGt_EORT);
#line 67 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PaUpdate.pgc"

if (sqlca.sqlcode == ECPG_NOT_FOUND) goto sqlerror;
#line 67 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PaUpdate.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 67 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PaUpdate.pgc"

        if(commit)
        { ECPGtrans(__LINE__, NULL, "commit work");
#line 69 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PaUpdate.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 69 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PaUpdate.pgc"

        return NO_ERROR;
    } else if(flg == 0) {
         { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "delete from SUM_PARTN_ALLOC where WD = $1  and SUMID = $2  and STATUS = $3 ", 
	ECPGt_varchar,&(l_wd),(long)80,(long)1,sizeof(struct varchar_1), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_long_long,&(l_uid),(long)1,(long)1,sizeof(long long), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_int,&(l_status),(long)1,(long)1,sizeof(int), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EOIT, ECPGt_EORT);
#line 75 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PaUpdate.pgc"

if (sqlca.sqlcode == ECPG_NOT_FOUND) goto sqlerror;
#line 75 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PaUpdate.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 75 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PaUpdate.pgc"

         /*if(commit)*/         /* no commit is obsolete. always commit */
         { ECPGtrans(__LINE__, NULL, "commit work");
#line 77 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PaUpdate.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 77 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PaUpdate.pgc"

		return NO_ERROR;
        } else {
		return DS_PALC_ERR;
    }
        
sqlerror:
	  printk("Error in NC_PaUpdate\n"); 
	  printk("% .70s \n", sqlca.sqlerrm.sqlerrmc); 
    /* exec sql whenever sqlerror  continue ; */
#line 86 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PaUpdate.pgc"

    { ECPGtrans(__LINE__, NULL, "rollback work");}
#line 87 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PaUpdate.pgc"

    return DS_PALC_ERR;
}

