/* Processed by ecpg (4.10.0) */
/* These include files are added by the preprocessor */
#include <ecpglib.h>
#include <ecpgerrno.h>
#include <sqlca.h>
/* End of automatic include section */

#line 1 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeFindGroup.pgc"
/* SUMLIB_TapeFindGroup.pgc
 */
#include <SUM.h>
#include <tape.h>
#include <sum_rpc.h>
#include <printk.h>

int SUMLIB_TapeFindGroup(int group, double bytes, TAPE *tape);

/* Called by writedo_1() in tape_svc to determine a tape with enough storage
 * to write the given group on.  If none found, will assign a new tape to
 * this group. Returns 1 if no tape can be found.
*/

int SUMLIB_TapeFindGroup(int group, double bytes, TAPE *tape)
{
/* exec sql begin declare section */
   
   
    
    
    
    
      

#line 18 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeFindGroup.pgc"
  struct varchar_1  { int len; char arr[ 20 ]; }  l_tapeid ;
 
#line 19 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeFindGroup.pgc"
  struct varchar_2  { int len; char arr[ 20 ]; }  l_tapeid2 ;
 
#line 20 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeFindGroup.pgc"
 int l_nxtwrtfn ;
 
#line 21 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeFindGroup.pgc"
 int l_spare ;
 
#line 22 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeFindGroup.pgc"
 int l_group_id ;
 
#line 23 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeFindGroup.pgc"
 int l_closed ;
 
#line 24 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeFindGroup.pgc"
 long long l_availblocks ;
/* exec sql end declare section */
#line 25 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeFindGroup.pgc"

  int found = 0;
  int closeit = 0;
  double size;
  char cltape[20];

  if(group < 0) {   //no negative group#
    printk("Error in SUMLIB_TapeFindGroup. Found neg group %d\n", group); 
    return(1);
  }
    /* exec sql whenever sqlerror  goto  sqlerror ; */
#line 35 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeFindGroup.pgc"

    l_group_id = group;

        ECPGset_var( 0, &( l_group_id ), __LINE__);\
 /* declare pfind cursor for select tapeid , nxtwrtfn , spare , avail_blocks , closed from SUM_TAPE where GROUP_ID = $1  and CLOSED <> 2 and CLOSED <> - 2 order by tapeid */
#line 42 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeFindGroup.pgc"
	//-2 may be found for a cleaning tape

        { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "declare pfind cursor for select tapeid , nxtwrtfn , spare , avail_blocks , closed from SUM_TAPE where GROUP_ID = $1  and CLOSED <> 2 and CLOSED <> - 2 order by tapeid", 
	ECPGt_int,&(l_group_id),(long)1,(long)1,sizeof(int), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EOIT, ECPGt_EORT);
#line 44 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeFindGroup.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 44 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeFindGroup.pgc"

    /* exec sql whenever not found  goto  end_fetch ; */
#line 45 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeFindGroup.pgc"


    for( ; ; ){
      { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "fetch pfind", ECPGt_EOIT, 
	ECPGt_varchar,&(l_tapeid),(long)20,(long)1,sizeof(struct varchar_1), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_int,&(l_nxtwrtfn),(long)1,(long)1,sizeof(int), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_int,&(l_spare),(long)1,(long)1,sizeof(int), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_long_long,&(l_availblocks),(long)1,(long)1,sizeof(long long), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_int,&(l_closed),(long)1,(long)1,sizeof(int), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EORT);
#line 48 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeFindGroup.pgc"

if (sqlca.sqlcode == ECPG_NOT_FOUND) goto end_fetch;
#line 48 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeFindGroup.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 48 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeFindGroup.pgc"

      size = (double)l_availblocks;
      size = size * 512.0;
      if(size >= (bytes + 100000000.0)) {   /* give 100MB leeway */
        found = 1;
        /* fill in the tape structure */
        tape->nxtwrtfn = l_nxtwrtfn;
        tape->spare = l_spare;
        tape->group_id = l_group_id;
        tape->availblocks = (unsigned long long)l_availblocks;
        tape->closed = l_closed;
        tape->tapeid = strdup(l_tapeid.arr);
        { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "close pfind", ECPGt_EOIT, ECPGt_EORT);
#line 60 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeFindGroup.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 60 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeFindGroup.pgc"

        { ECPGtrans(__LINE__, NULL, "commit");
#line 61 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeFindGroup.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 61 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeFindGroup.pgc"

        if(closeit) {
          printk("SUMLIB_TapeFindGroup: TMP MSG: Need to close %s\n", cltape);
          //just give above msg for now. Later after this test enable the close.
          //SUMLIB_TapeClose(cltape);
        }
	printk("SUMLIB_TapeFindGroup found tape %s for group %d\n", 
			l_tapeid.arr, group); 
        return(0);
      }
      else {			//the open tape doesn't have enough space
        closeit = 1;		//can't call sql to close at this point
        strcpy(cltape, l_tapeid.arr);
      }
    }

end_fetch:
    { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "close pfind", ECPGt_EOIT, ECPGt_EORT);
#line 78 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeFindGroup.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 78 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeFindGroup.pgc"

    { ECPGtrans(__LINE__, NULL, "commit");
#line 79 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeFindGroup.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 79 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeFindGroup.pgc"

    /* exec sql whenever sqlerror  goto  sqlerror ; */
#line 80 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeFindGroup.pgc"

    /* now get an unused tape and assign it this group id */
        /* declare tfind cursor for select tapeid , nxtwrtfn , spare , avail_blocks , closed from SUM_TAPE where GROUP_ID = - 1 and CLOSED <> 2 and CLOSED <> - 2 order by tapeid */
#line 86 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeFindGroup.pgc"


        { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "declare tfind cursor for select tapeid , nxtwrtfn , spare , avail_blocks , closed from SUM_TAPE where GROUP_ID = - 1 and CLOSED <> 2 and CLOSED <> - 2 order by tapeid", ECPGt_EOIT, ECPGt_EORT);
#line 88 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeFindGroup.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 88 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeFindGroup.pgc"

    /* exec sql whenever not found  goto  end_tfetch ; */
#line 89 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeFindGroup.pgc"


    found = 0;
    for( ; ; ){
      { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "fetch tfind", ECPGt_EOIT, 
	ECPGt_varchar,&(l_tapeid2),(long)20,(long)1,sizeof(struct varchar_2), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_int,&(l_nxtwrtfn),(long)1,(long)1,sizeof(int), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_int,&(l_spare),(long)1,(long)1,sizeof(int), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_long_long,&(l_availblocks),(long)1,(long)1,sizeof(long long), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_int,&(l_closed),(long)1,(long)1,sizeof(int), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EORT);
#line 93 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeFindGroup.pgc"

if (sqlca.sqlcode == ECPG_NOT_FOUND) goto end_tfetch;
#line 93 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeFindGroup.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 93 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeFindGroup.pgc"

      found = 1;
      /* fill in the tape structure */
      tape->nxtwrtfn = l_nxtwrtfn;
      tape->spare = l_spare;
      tape->group_id = l_group_id;
      tape->availblocks = (unsigned long long)l_availblocks;
      tape->closed = l_closed;
      tape->tapeid = strdup(l_tapeid2.arr);
      { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "close tfind", ECPGt_EOIT, ECPGt_EORT);
#line 102 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeFindGroup.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 102 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeFindGroup.pgc"

      /* now assign the group to this tapeid */
      { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "update SUM_TAPE set GROUP_ID = $1  where TAPEID = $2 ", 
	ECPGt_int,&(l_group_id),(long)1,(long)1,sizeof(int), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_varchar,&(l_tapeid2),(long)20,(long)1,sizeof(struct varchar_2), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EOIT, ECPGt_EORT);
#line 106 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeFindGroup.pgc"

if (sqlca.sqlcode == ECPG_NOT_FOUND) goto end_tfetch;
#line 106 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeFindGroup.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 106 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeFindGroup.pgc"

      { ECPGtrans(__LINE__, NULL, "commit work");
#line 107 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeFindGroup.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 107 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeFindGroup.pgc"

      printk("SUMLIB_TapeFindGroup found tape %s for group %d\n", 
			l_tapeid2.arr, group); 
      if(closeit) {
        printk("SUMLIB_TapeFindGroup: TMP MSG: Need to close %s\n", cltape);
        //just give the above msg for now. Later after this test enable the close.
        //SUMLIB_TapeClose(cltape);
      }
      return(0);	/* just need 1st entry */
    }

end_tfetch:
    { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "close tfind", ECPGt_EOIT, ECPGt_EORT);
#line 119 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeFindGroup.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 119 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeFindGroup.pgc"

    { ECPGtrans(__LINE__, NULL, "commit");
#line 120 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeFindGroup.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 120 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeFindGroup.pgc"

    if(closeit) {
      printk("SUMLIB_TapeFindGroup: TMP: Need to close %s\n", cltape);
      //SUMLIB_TapeClose(cltape);
    }
    if(!found) {
      printk("Error in SUMLIB_TapeFindGroup.  Can't find tape for group %d\n", group); 
      return(1);
    }

sqlerror:
    { ECPGtrans(__LINE__, NULL, "rollback work");
#line 131 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeFindGroup.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 131 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeFindGroup.pgc"

    printk("Error in SUMLIB_TapeFindGroup.  Can't get tape for group %d\n", group); 
    printk("% .70s \n", sqlca.sqlerrm.sqlerrmc); 
    if(closeit) {
      printk("TMP msg: Need to close %s\n", cltape);
      //SUMLIB_TapeClose(cltape);
    }
    return(1);
}
