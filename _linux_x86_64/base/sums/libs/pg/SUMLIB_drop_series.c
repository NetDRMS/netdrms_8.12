/* Processed by ecpg (4.10.0) */
/* These include files are added by the preprocessor */
#include <ecpglib.h>
#include <ecpgerrno.h>
#include <sqlca.h>
/* End of automatic include section */

#line 1 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_drop_series.pgc"
/* SUMLIB_drop_series.pgc
 *
 * WRONG!! This file is not used at all!
 *   -Art
 *
 * This is a function used by the delete_series program.
 * It is passed a DB table that has the list of sunum (aka ds_index)
 * values to be removed from the SUMS.
 * For each sunum will update any sum_partn_alloc del pending or archive
 * pending entry to be del pend with substatus DAAEDDP with effective
 * date of 0. A DAAEDDP substatus means a temp ds and that after the wd 
 * is removed the sum_main entry is also removed (done in SUMLIB_RmDo.pgc).
 * Returns 0 on success. 
 */
#include <SUM.h>
#include <sum_rpc.h>
#include <soi_error.h>
#include <printk.h>


int SUM_drop_series(char *tablename);

int SUM_drop_series(char *tablename) 
{
/* exec sql begin declare section */
   
     

#line 26 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_drop_series.pgc"
 char stmt [ 128 ] ;
 
#line 27 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_drop_series.pgc"
 long long ds_index ;
/* exec sql end declare section */
#line 28 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_drop_series.pgc"


int i, j;

  sprintf(stmt, "SELECT sunum from %s", tablename);
  /* exec sql whenever sqlerror  goto  sqlerror ; */
#line 33 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_drop_series.pgc"

  { ECPGprepare(__LINE__, NULL, 0, "query", stmt);
#line 34 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_drop_series.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 34 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_drop_series.pgc"

  /* declare reqcursor cursor for $1 */
#line 35 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_drop_series.pgc"

  { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "declare reqcursor cursor for $1", 
	ECPGt_char_variable,(ECPGprepared_statement(NULL, "query", __LINE__)),(long)1,(long)1,(1)*sizeof(char), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EOIT, ECPGt_EORT);
#line 36 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_drop_series.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 36 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_drop_series.pgc"


  /* exec sql whenever not found  goto  end_fetch ; */
#line 38 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_drop_series.pgc"


  for(i=0; ; i++) {
    { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "fetch reqcursor", ECPGt_EOIT, 
	ECPGt_long_long,&(ds_index),(long)1,(long)1,sizeof(long long), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EORT);
#line 41 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_drop_series.pgc"

if (sqlca.sqlcode == ECPG_NOT_FOUND) goto end_fetch;
#line 41 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_drop_series.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 41 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_drop_series.pgc"

    printk("in SUM_drop_series() ds_index = %lld\n", ds_index);
    /* exec sql whenever not found  goto  sqllp ; */
#line 43 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_drop_series.pgc"

    { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "update SUM_PARTN_ALLOC set STATUS = 2 , ARCHIVE_SUBSTATUS = 32 , EFFECTIVE_DATE = '0' where DS_INDEX = $1 ", 
	ECPGt_long_long,&(ds_index),(long)1,(long)1,sizeof(long long), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EOIT, ECPGt_EORT);
#line 45 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_drop_series.pgc"

if (sqlca.sqlcode == ECPG_NOT_FOUND) goto sqllp;
#line 45 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_drop_series.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 45 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_drop_series.pgc"

    /* exec sql whenever not found  goto  end_fetch ; */
#line 46 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_drop_series.pgc"

    continue;
sqllp:
    //no partn_alloc entry so just delete the sum_main entry
    /* exec sql whenever not found  goto  end_loop ; */
#line 50 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_drop_series.pgc"

    { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "delete from SUM_MAIN where DS_INDEX = $1 ", 
	ECPGt_long_long,&(ds_index),(long)1,(long)1,sizeof(long long), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EOIT, ECPGt_EORT);
#line 51 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_drop_series.pgc"

if (sqlca.sqlcode == ECPG_NOT_FOUND) goto end_loop;
#line 51 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_drop_series.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 51 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_drop_series.pgc"

    printk("del SUM_MAIN for ds_index=%lld\n", ds_index);
end_loop:
    /* exec sql whenever not found  goto  end_fetch ; */
#line 54 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_drop_series.pgc"

    continue;
  }

end_fetch:
        printk("end_fetch in SUM_drop_series()\n");
	{ ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "close reqcursor", ECPGt_EOIT, ECPGt_EORT);
#line 60 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_drop_series.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 60 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_drop_series.pgc"

        { ECPGtrans(__LINE__, NULL, "commit");
#line 61 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_drop_series.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 61 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_drop_series.pgc"

	return NO_ERROR;
    
sqlerror:
	{ ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "close reqcursor", ECPGt_EOIT, ECPGt_EORT);
#line 65 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_drop_series.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 65 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_drop_series.pgc"

        { ECPGtrans(__LINE__, NULL, "commit");
#line 66 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_drop_series.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 66 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_drop_series.pgc"

	printk("Error in SUM_drop_series()\n");  
	printk("% .70s \n", sqlca.sqlerrm.sqlerrmc);  
    return 1;
}
