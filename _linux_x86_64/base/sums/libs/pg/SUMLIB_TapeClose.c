/* Processed by ecpg (4.10.0) */
/* These include files are added by the preprocessor */
#include <ecpglib.h>
#include <ecpgerrno.h>
#include <sqlca.h>
/* End of automatic include section */

#line 1 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeClose.pgc"
/* SUMLIB_TapeClose.pgc
 */
#include <SUM.h>
#include <tape.h>
#include <sum_rpc.h>
#include <printk.h>

int SUMLIB_TapeClose(char *tapeid);

int SUMLIB_TapeActive(char *tapeid);

int SUMLIB_TapeState(char *tapeid);

int SUMLIB_TapeCatalog(char *tapeid);

/* Return the tape state in the sum_tape table.
 * Return 0 on error.
*/
int SUMLIB_TapeState(char *tapeid)
{
/* exec sql begin declare section */
   
    

#line 22 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeClose.pgc"
  struct varchar_1  { int len; char arr[ 20 ]; }  l_tapeid ;
 
#line 23 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeClose.pgc"
 int l_closed ;
/* exec sql end declare section */
#line 24 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeClose.pgc"


    /* exec sql whenever not found  goto  sqlerror ; */
#line 26 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeClose.pgc"
 
    /* exec sql whenever sqlerror  goto  sqlerror ; */
#line 27 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeClose.pgc"

    sprintf(l_tapeid.arr, "%s", tapeid);
    l_tapeid.len = strlen(l_tapeid.arr);
    { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "select CLOSED from SUM_TAPE where TAPEID = $1 ", 
	ECPGt_varchar,&(l_tapeid),(long)20,(long)1,sizeof(struct varchar_1), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EOIT, 
	ECPGt_int,&(l_closed),(long)1,(long)1,sizeof(int), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EORT);
#line 31 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeClose.pgc"

if (sqlca.sqlcode == ECPG_NOT_FOUND) goto sqlerror;
#line 31 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeClose.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 31 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeClose.pgc"


    { ECPGtrans(__LINE__, NULL, "commit work");
#line 33 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeClose.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 33 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeClose.pgc"

    /*printk("Closed status for tapeid %s is %d\n", tapeid, l_closed);*/
    return(l_closed);

sqlerror:
	printk("Error in SUMLIB_TapeState.\n"); 
	printk("% .70s \n", sqlca.sqlerrm.sqlerrmc); 
    { ECPGtrans(__LINE__, NULL, "rollback work");
#line 40 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeClose.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 40 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeClose.pgc"

    return(0);
}

/* Called by taperespwritedo_1() in tape_svc  when an error occured on 
 * a write tape operation. The tape is closed and no longer available for
 * writting and the attempted write is a noop. Return 0 on success.
*/
int SUMLIB_TapeClose(char *tapeid)
{
/* exec sql begin declare section */
   
    

#line 51 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeClose.pgc"
  struct varchar_2  { int len; char arr[ 20 ]; }  l_tapeid ;
 
#line 52 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeClose.pgc"
 int l_closed ;
/* exec sql end declare section */
#line 53 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeClose.pgc"



    /* exec sql whenever not found  goto  sqlerror ; */
#line 56 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeClose.pgc"
 
    /* exec sql whenever sqlerror  goto  sqlerror ; */
#line 57 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeClose.pgc"

    sprintf(l_tapeid.arr, "%s", tapeid);
    l_tapeid.len = strlen(l_tapeid.arr);
    l_closed = TAPECLOSED;
    { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "update SUM_TAPE set CLOSED = $1  where TAPEID = $2 ", 
	ECPGt_int,&(l_closed),(long)1,(long)1,sizeof(int), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_varchar,&(l_tapeid),(long)20,(long)1,sizeof(struct varchar_2), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EOIT, ECPGt_EORT);
#line 63 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeClose.pgc"

if (sqlca.sqlcode == ECPG_NOT_FOUND) goto sqlerror;
#line 63 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeClose.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 63 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeClose.pgc"


    { ECPGtrans(__LINE__, NULL, "commit work");
#line 65 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeClose.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 65 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeClose.pgc"

    printk("*Tp:Close: tapeid=%s\n", tapeid);
    return(0);

sqlerror:
	printk("Error in SUMLIB_TapeClose.\n"); 
	printk("% .70s \n", sqlca.sqlerrm.sqlerrmc); 
    { ECPGtrans(__LINE__, NULL, "rollback work");
#line 72 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeClose.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 72 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeClose.pgc"

    return(1);
}


/* Called by taperespwritedo_1() in tape_svc  when an uninitialized tape
 * has been made active by writting its label and 1st data file.
 * Return 0 on success.
*/
int SUMLIB_TapeActive(char *tapeid)
{
/* exec sql begin declare section */
   
    

#line 84 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeClose.pgc"
  struct varchar_3  { int len; char arr[ 20 ]; }  l_tapeid ;
 
#line 85 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeClose.pgc"
 int l_closed ;
/* exec sql end declare section */
#line 86 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeClose.pgc"


    /* exec sql whenever not found  goto  sqlerror ; */
#line 88 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeClose.pgc"
 
    /* exec sql whenever sqlerror  goto  sqlerror ; */
#line 89 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeClose.pgc"

    sprintf(l_tapeid.arr, "%s", tapeid);
    l_tapeid.len = strlen(l_tapeid.arr);
    l_closed = TAPEACTIVE;
    { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "update SUM_TAPE set CLOSED = $1  where TAPEID = $2 ", 
	ECPGt_int,&(l_closed),(long)1,(long)1,sizeof(int), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_varchar,&(l_tapeid),(long)20,(long)1,sizeof(struct varchar_3), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EOIT, ECPGt_EORT);
#line 95 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeClose.pgc"

if (sqlca.sqlcode == ECPG_NOT_FOUND) goto sqlerror;
#line 95 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeClose.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 95 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeClose.pgc"


    { ECPGtrans(__LINE__, NULL, "commit work");
#line 97 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeClose.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 97 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeClose.pgc"

    printk("Closed status for tapeid %s set to %d\n", tapeid, l_closed);
    return(0);

sqlerror:
	printk("Error in SUMLIB_TapeActive.\n"); 
	printk("% .70s \n", sqlca.sqlerrm.sqlerrmc); 
    { ECPGtrans(__LINE__, NULL, "rollback work");
#line 104 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeClose.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 104 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeClose.pgc"

    return(1);
}

/* See if the tapeid is already in the sum_tape table. If it is and it
 * is closed, the caller should output the "*Tp:ReClose:" msg for tui.
 * If it is not closed, then do nothing.
 * If it is not in sum_tape then make a new entry for it.
 * Returns the tape state, or 0 on error (there is no state 0).
*/
int SUMLIB_TapeCatalog(char *tapeid)
{
/* exec sql begin declare section */
   
    
   

#line 117 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeClose.pgc"
  struct varchar_4  { int len; char arr[ 20 ]; }  l_tapeid ;
 
#line 118 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeClose.pgc"
 int l_closed ;
 
#line 119 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeClose.pgc"
 long ablocks ;
/* exec sql end declare section */
#line 120 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeClose.pgc"


    /* exec sql whenever not found  goto  tapenotfound ; */
#line 122 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeClose.pgc"
 
    /* exec sql whenever sqlerror  goto  sqlerror ; */
#line 123 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeClose.pgc"

    sprintf(l_tapeid.arr, "%s", tapeid);
    l_tapeid.len = strlen(l_tapeid.arr);
    { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "select CLOSED from SUM_TAPE where TAPEID = $1 ", 
	ECPGt_varchar,&(l_tapeid),(long)20,(long)1,sizeof(struct varchar_4), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EOIT, 
	ECPGt_int,&(l_closed),(long)1,(long)1,sizeof(int), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EORT);
#line 127 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeClose.pgc"

if (sqlca.sqlcode == ECPG_NOT_FOUND) goto tapenotfound;
#line 127 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeClose.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 127 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeClose.pgc"


    { ECPGtrans(__LINE__, NULL, "commit work");
#line 129 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeClose.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 129 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeClose.pgc"

    return(l_closed);

tapenotfound:
    ablocks = MAX_AVAIL_BLOCKS;
    if(strstr(tapeid , "CLN")) {	//cleaning tape. make it closed
      { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "insert into SUM_TAPE ( tapeid , nxtwrtfn , spare , group_id , avail_blocks , closed , last_write ) values ( $1  , 1 , - 1 , - 1 , $2  , - 2 , null )", 
	ECPGt_varchar,&(l_tapeid),(long)20,(long)1,sizeof(struct varchar_4), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_long,&(ablocks),(long)1,(long)1,sizeof(long), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EOIT, ECPGt_EORT);
#line 135 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeClose.pgc"

if (sqlca.sqlcode == ECPG_NOT_FOUND) goto tapenotfound;
#line 135 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeClose.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 135 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeClose.pgc"

    }
    else {
      { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "insert into SUM_TAPE ( tapeid , nxtwrtfn , spare , group_id , avail_blocks , closed , last_write ) values ( $1  , 1 , - 1 , - 1 , $2  , - 1 , null )", 
	ECPGt_varchar,&(l_tapeid),(long)20,(long)1,sizeof(struct varchar_4), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_long,&(ablocks),(long)1,(long)1,sizeof(long), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EOIT, ECPGt_EORT);
#line 138 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeClose.pgc"

if (sqlca.sqlcode == ECPG_NOT_FOUND) goto tapenotfound;
#line 138 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeClose.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 138 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeClose.pgc"

      printk("*Cataloged new tape %s\n", tapeid);
    }
    { ECPGtrans(__LINE__, NULL, "commit work");
#line 141 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeClose.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 141 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeClose.pgc"

    return(-1);

sqlerror:
     { ECPGtrans(__LINE__, NULL, "rollback work");
#line 145 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeClose.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 145 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeClose.pgc"

     printk("Error in SUMLIB_TapeCatalog.\n"); 
     printk("% .70s \n", sqlca.sqlerrm.sqlerrmc); 
    return(0);
}
