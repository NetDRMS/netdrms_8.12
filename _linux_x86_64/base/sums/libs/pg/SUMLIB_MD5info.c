/* Processed by ecpg (4.10.0) */
/* These include files are added by the preprocessor */
#include <ecpglib.h>
#include <ecpgerrno.h>
#include <sqlca.h>
/* End of automatic include section */

#line 1 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_MD5info.pgc"
/* SUMLIB_MD5info.pgc
 */
#include <SUM.h>
#if defined(SUMS_TAPE_AVAILABLE) && SUMS_TAPE_AVAILABLE
  #include <tape.h>
#endif
#include <sum_rpc.h>
#include <printk.h>

int SUMLIB_MD5info(char *tapeid);
int SUMLIB_Get_MD5(char *tapeid, int filenum, char *md5ret);

/* Make a md5 info file to send to the offsite system so that it can
 * verify the tape before accepting it in the offsite system. 
 * The file will be: /x/x/x/tapeid.md5
 * Return non-0 on error.
*/
int SUMLIB_MD5info(char *tapeid)
{
/* exec sql begin declare section */
   
   
   

#line 21 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_MD5info.pgc"
  struct varchar_1  { int len; char arr[ 20 ]; }  l_tapeid ;
 
#line 22 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_MD5info.pgc"
  struct varchar_2  { int len; char arr[ 36 ]; }  l_md5cksum ;
 
#line 23 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_MD5info.pgc"
 int l_filenum ;
/* exec sql end declare section */
#line 24 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_MD5info.pgc"

FILE *md5fp;
char md5file[256], cmd[256];

    sprintf(l_tapeid.arr, "%s", tapeid);
    l_tapeid.len = strlen(l_tapeid.arr);

    /* exec sql whenever sqlerror  goto  sqlerror ; */
#line 31 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_MD5info.pgc"


        ECPGset_var( 0, &( l_tapeid ), __LINE__);\
 /* declare md5cursor cursor for select md5cksum , filenum from SUM_FILE where TAPEID = $1  */
#line 36 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_MD5info.pgc"


        { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "declare md5cursor cursor for select md5cksum , filenum from SUM_FILE where TAPEID = $1 ", 
	ECPGt_varchar,&(l_tapeid),(long)20,(long)1,sizeof(struct varchar_1), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EOIT, ECPGt_EORT);
#line 38 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_MD5info.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 38 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_MD5info.pgc"

    /* exec sql whenever not found  goto  end_fetch ; */
#line 39 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_MD5info.pgc"


    sprintf(md5file, "%s/%s.md5", OFFSITEDIR, tapeid);
    if((md5fp=fopen(md5file, "w")) == NULL) {
      printk("Can't open the md5 file %s\n", md5file);
      { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "close md5cursor", ECPGt_EOIT, ECPGt_EORT);
#line 44 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_MD5info.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 44 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_MD5info.pgc"

      { ECPGtrans(__LINE__, NULL, "rollback work");
#line 45 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_MD5info.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 45 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_MD5info.pgc"

      return(1);
    }
    fprintf(md5fp, "# %s from datacapture\n", md5file);

    for( ; ; ){
      { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "fetch md5cursor", ECPGt_EOIT, 
	ECPGt_varchar,&(l_md5cksum),(long)36,(long)1,sizeof(struct varchar_2), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_int,&(l_filenum),(long)1,(long)1,sizeof(int), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EORT);
#line 51 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_MD5info.pgc"

if (sqlca.sqlcode == ECPG_NOT_FOUND) goto end_fetch;
#line 51 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_MD5info.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 51 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_MD5info.pgc"

      fprintf(md5fp, "%u %s\n", l_filenum, l_md5cksum.arr);
    }

end_fetch:
    fclose(md5fp);
    { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "close md5cursor", ECPGt_EOIT, ECPGt_EORT);
#line 57 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_MD5info.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 57 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_MD5info.pgc"

    { ECPGtrans(__LINE__, NULL, "commit");
#line 58 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_MD5info.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 58 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_MD5info.pgc"

    //NO don't scp it. leave it in the local dcs machine in /dds/socdc
    //sprintf(cmd, "scp -p %s %s:%s", md5file, OFFSITEHOST, OFFSITEDIR);
    //printk("%s\n", cmd);
    //if(system(cmd)) { 
    //  printk("Error on: %s\n", cmd);
    //  return(1);
    //}
    return(0);

sqlerror:
    fclose(md5fp);
    { ECPGtrans(__LINE__, NULL, "rollback work");
#line 70 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_MD5info.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 70 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_MD5info.pgc"

    printk("Error in SUMLIB_MD5info for tape %s\n", tapeid);
    printk("% .70s \n", sqlca.sqlerrm.sqlerrmc);
    return(1);
}

int SUMLIB_Get_MD5(char *tapeid, int filenum, char *md5ret)
{
/* exec sql begin declare section */
   
   
   

#line 79 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_MD5info.pgc"
  struct varchar_3  { int len; char arr[ 20 ]; }  l_tapeid ;
 
#line 80 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_MD5info.pgc"
  struct varchar_4  { int len; char arr[ 36 ]; }  l_md5cksum ;
 
#line 81 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_MD5info.pgc"
 int l_filenum ;
/* exec sql end declare section */
#line 82 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_MD5info.pgc"


    /* exec sql whenever sqlerror  goto  sqlerror ; */
#line 84 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_MD5info.pgc"

    sprintf(l_tapeid.arr, "%s", tapeid);
    l_tapeid.len = strlen(l_tapeid.arr);
    l_filenum = filenum;
    { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "select MD5CKSUM from SUM_FILE where TAPEID = $1  and FILENUM = $2 ", 
	ECPGt_varchar,&(l_tapeid),(long)20,(long)1,sizeof(struct varchar_3), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_int,&(l_filenum),(long)1,(long)1,sizeof(int), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EOIT, 
	ECPGt_varchar,&(l_md5cksum),(long)36,(long)1,sizeof(struct varchar_4), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EORT);
#line 89 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_MD5info.pgc"

if (sqlca.sqlcode == ECPG_NOT_FOUND) goto end_fetch;
#line 89 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_MD5info.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 89 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_MD5info.pgc"

    strcpy(md5ret, l_md5cksum.arr);
    { ECPGtrans(__LINE__, NULL, "commit");
#line 91 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_MD5info.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 91 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_MD5info.pgc"

    return(0);
sqlerror:
end_fetch:
    { ECPGtrans(__LINE__, NULL, "rollback work");
#line 95 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_MD5info.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 95 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_MD5info.pgc"

    printk("Error in SUMLIB_Get_MD5 for tape %s file %d\n", tapeid, filenum);
    printk("% .70s \n", sqlca.sqlerrm.sqlerrmc);
    return(1);
}

