/* Processed by ecpg (4.10.0) */
/* These include files are added by the preprocessor */
#include <ecpglib.h>
#include <ecpgerrno.h>
#include <sqlca.h>
/* End of automatic include section */

#line 1 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGet.pgc"
/* SUMLIB_InfoGet.pgc
*/
/* This returns info from the sum_main table for the given sunum 
 * (i.e. ds_index). Returns non-0 on error.
 */
#include <SUM.h>
#include <sum_rpc.h>
#include <printk.h>

int SUMLIB_InfoGet(uint64_t sunum , KEY **results);

int SUMLIB_InfoGet(uint64_t sunum , KEY **result)
{
/* exec sql begin declare section */
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
       
      
     

#line 15 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGet.pgc"
  struct varchar_1  { int len; char arr[ 80 ]; }  online_loc ;
 
#line 16 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGet.pgc"
  struct varchar_2  { int len; char arr[ 5 ]; }  online_status ;
 
#line 17 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGet.pgc"
  struct varchar_3  { int len; char arr[ 5 ]; }  archive_status ;
 
#line 18 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGet.pgc"
  struct varchar_4  { int len; char arr[ 5 ]; }  offsite_ack ;
 
#line 19 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGet.pgc"
  struct varchar_5  { int len; char arr[ 80 ]; }  history_comment ;
 
#line 20 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGet.pgc"
  struct varchar_6  { int len; char arr[ 80 ]; }  owning_series ;
 
#line 21 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGet.pgc"
 int storage_group ;
 
#line 22 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGet.pgc"
 double bytes ;
 
#line 23 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGet.pgc"
  struct varchar_7  { int len; char arr[ 32 ]; }  creat_date ;
 
#line 24 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGet.pgc"
  struct varchar_8  { int len; char arr[ 10 ]; }  username ;
 
#line 25 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGet.pgc"
  struct varchar_9  { int len; char arr[ 20 ]; }  arch_tape ;
 
#line 26 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGet.pgc"
 int arch_tape_fn ;
 
#line 27 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGet.pgc"
  struct varchar_10  { int len; char arr[ 32 ]; }  arch_tape_date ;
 
#line 28 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGet.pgc"
  struct varchar_11  { int len; char arr[ 20 ]; }  safe_tape ;
 
#line 29 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGet.pgc"
 int safe_tape_fn ;
 
#line 30 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGet.pgc"
  struct varchar_12  { int len; char arr[ 32 ]; }  safe_tape_date ;
 
#line 31 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGet.pgc"
 long long ds_index ;
 
#line 32 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGet.pgc"
 int pa_status , pa_substatus ;
 
#line 33 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGet.pgc"
  struct varchar_13  { int len; char arr[ 20 ]; }  effective_date ;
/* exec sql end declare section */
#line 34 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGet.pgc"


    /* exec sql whenever sqlerror  goto  sqlerror ; */
#line 36 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGet.pgc"

    /* exec sql whenever not found  goto  sqlerror ; */
#line 37 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGet.pgc"
 

   ds_index = (long long)sunum;
 
 { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "select online_loc , online_status , archive_status , offsite_ack , history_comment , owning_series , storage_group , bytes , creat_date , username , coalesce ( arch_tape , 'N/A' ) , coalesce ( arch_tape_fn , 0 ) , coalesce ( arch_tape_date , '1958-01-01 00:00:00' ) , coalesce ( safe_tape , 'N/A' ) , coalesce ( safe_tape_fn , 0 ) , coalesce ( safe_tape_date , '1958-01-01 00:00:00' ) from SUM_MAIN where DS_INDEX = $1 ", 
	ECPGt_long_long,&(ds_index),(long)1,(long)1,sizeof(long long), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EOIT, 
	ECPGt_varchar,&(online_loc),(long)80,(long)1,sizeof(struct varchar_1), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_varchar,&(online_status),(long)5,(long)1,sizeof(struct varchar_2), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_varchar,&(archive_status),(long)5,(long)1,sizeof(struct varchar_3), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_varchar,&(offsite_ack),(long)5,(long)1,sizeof(struct varchar_4), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_varchar,&(history_comment),(long)80,(long)1,sizeof(struct varchar_5), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_varchar,&(owning_series),(long)80,(long)1,sizeof(struct varchar_6), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_int,&(storage_group),(long)1,(long)1,sizeof(int), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_double,&(bytes),(long)1,(long)1,sizeof(double), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_varchar,&(creat_date),(long)32,(long)1,sizeof(struct varchar_7), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_varchar,&(username),(long)10,(long)1,sizeof(struct varchar_8), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_varchar,&(arch_tape),(long)20,(long)1,sizeof(struct varchar_9), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_int,&(arch_tape_fn),(long)1,(long)1,sizeof(int), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_varchar,&(arch_tape_date),(long)32,(long)1,sizeof(struct varchar_10), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_varchar,&(safe_tape),(long)20,(long)1,sizeof(struct varchar_11), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_int,&(safe_tape_fn),(long)1,(long)1,sizeof(int), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_varchar,&(safe_tape_date),(long)32,(long)1,sizeof(struct varchar_12), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EORT);
#line 51 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGet.pgc"

if (sqlca.sqlcode == ECPG_NOT_FOUND) goto sqlerror;
#line 51 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGet.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 51 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGet.pgc"


    setkey_str(result, "online_loc", online_loc.arr);
    setkey_str(result, "online_status", online_status.arr);
    setkey_str(result, "archive_status", archive_status.arr);
    setkey_str(result, "offsite_ack", offsite_ack.arr);
    setkey_str(result, "history_comment", history_comment.arr);
    setkey_str(result, "owning_series", owning_series.arr);
    setkey_int(result, "storage_group", storage_group);
    setkey_double(result, "bytes", bytes);
    setkey_str(result, "creat_date", creat_date.arr);
    setkey_str(result, "username", username.arr);
    setkey_str(result, "arch_tape", arch_tape.arr);
    setkey_int(result, "arch_tape_fn", arch_tape_fn);
    setkey_str(result, "arch_tape_date", arch_tape_date.arr);
    setkey_str(result, "safe_tape", safe_tape.arr);
    setkey_int(result, "safe_tape_fn", safe_tape_fn);
    setkey_str(result, "safe_tape_date", safe_tape_date.arr);
    setkey_uint64(result, "SUNUM", (unsigned long long)ds_index);

  //now get values in sum_partn_alloc
    /* exec sql whenever not found  goto  noerror ; */
#line 72 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGet.pgc"
 
    //use DISTINCT in case dup entries
 { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "select distinct on ( effective_date ) effective_date , status , archive_substatus from SUM_PARTN_ALLOC where DS_INDEX = $1  and STATUS <> 8", 
	ECPGt_long_long,&(ds_index),(long)1,(long)1,sizeof(long long), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EOIT, 
	ECPGt_varchar,&(effective_date),(long)20,(long)1,sizeof(struct varchar_13), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_int,&(pa_status),(long)1,(long)1,sizeof(int), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_int,&(pa_substatus),(long)1,(long)1,sizeof(int), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EORT);
#line 76 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGet.pgc"

if (sqlca.sqlcode == ECPG_NOT_FOUND) goto noerror;
#line 76 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGet.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 76 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGet.pgc"

    setkey_int(result, "pa_status", pa_status);
    setkey_int(result, "pa_substatus", pa_substatus);
    setkey_str(result, "effective_date", effective_date.arr);
    return(0);

noerror:
    setkey_int(result, "pa_status", -1);
    setkey_int(result, "pa_substatus", -1);
    setkey_str(result, "effective_date", "N/A");
    return(0);

sqlerror:
	printk("Error in SUMLIB_InfoGet sunum=%lld\n", ds_index); 
	//printk("% .70s \n", sqlca.sqlerrm.sqlerrmc); 
    /* exec sql whenever sqlerror  continue ; */
#line 91 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGet.pgc"

    { ECPGtrans(__LINE__, NULL, "rollback work");}
#line 92 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_InfoGet.pgc"

    return(1);
}
