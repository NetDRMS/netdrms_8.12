/* Processed by ecpg (4.10.0) */
/* These include files are added by the preprocessor */
#include <ecpglib.h>
#include <ecpgerrno.h>
#include <sqlca.h>
/* End of automatic include section */

#line 1 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_SafeTapeUpdate.pgc"
/* SUMLIB_SafeTapeUpdate.pgc
 *
 * Called by ingest_tlm when it gets a .parc file from the pipeline
 * backend and needs to update sum_main with info on where the tlm
 * file was archived on the pipeline tape.
*/
#include <SUM.h>
#include <sum_rpc.h>
#include <soi_error.h>
#include <printk.h>

int SUMLIB_SafeTapeUpdate(char *suname, char *tapeid, int tapefn, char *tapedate);

int SUMLIB_SafeTapeUpdate(char *suname, char *tapeid, int tapefn, char *tapedate)
{

/* exec sql begin declare section */
   
   
   
   

#line 18 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_SafeTapeUpdate.pgc"
  struct varchar_1  { int len; char arr[ 20 ]; }  safe_tape ;
 
#line 19 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_SafeTapeUpdate.pgc"
 int safe_tape_fn ;
 
#line 20 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_SafeTapeUpdate.pgc"
  struct varchar_2  { int len; char arr[ 32 ]; }  safe_tape_date ;
 
#line 21 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_SafeTapeUpdate.pgc"
  struct varchar_3  { int len; char arr[ 80 ]; }  owning_series ;
/* exec sql end declare section */
#line 22 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_SafeTapeUpdate.pgc"


    sprintf(owning_series.arr, "%s", suname);
    owning_series.len = strlen(owning_series.arr);
    sprintf(safe_tape.arr, "%s", tapeid);
    safe_tape.len = strlen(safe_tape.arr);
    sprintf(safe_tape_date.arr, "%s", tapedate);
    safe_tape_date.len = strlen(safe_tape_date.arr);
    safe_tape_fn = tapefn;

     /* exec sql whenever sqlerror  goto  sqlerror ; */
#line 32 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_SafeTapeUpdate.pgc"

     /* exec sql whenever not found  goto  sqlerror ; */
#line 33 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_SafeTapeUpdate.pgc"


     { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "update SUM_MAIN set SAFE_TAPE = $1  , SAFE_TAPE_FN = $2  , SAFE_TAPE_DATE = $3  where OWNING_SERIES = $4 ", 
	ECPGt_varchar,&(safe_tape),(long)20,(long)1,sizeof(struct varchar_1), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_int,&(safe_tape_fn),(long)1,(long)1,sizeof(int), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_varchar,&(safe_tape_date),(long)32,(long)1,sizeof(struct varchar_2), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_varchar,&(owning_series),(long)80,(long)1,sizeof(struct varchar_3), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EOIT, ECPGt_EORT);
#line 39 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_SafeTapeUpdate.pgc"

if (sqlca.sqlcode == ECPG_NOT_FOUND) goto sqlerror;
#line 39 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_SafeTapeUpdate.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 39 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_SafeTapeUpdate.pgc"


  { ECPGtrans(__LINE__, NULL, "commit work");
#line 41 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_SafeTapeUpdate.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 41 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_SafeTapeUpdate.pgc"

  printk("UPDATE SUM_MAIN SET SAFE_TAPE=%s SAFE_TAPE_FN=%d\n", 
		safe_tape.arr, safe_tape_fn);
  printk("SAFE_TAPE_DATE=%s WHERE OWNING_SERIES=%s\n", 
		safe_tape_date.arr, owning_series.arr);
  return(0);

sqlerror:
     printk("Error in SUMLIB_SafeTapeUpdate() \n");
     printk("% .70s \n", sqlca.sqlerrm.sqlerrmc);
     /* exec sql whenever sqlerror  continue ; */
#line 51 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_SafeTapeUpdate.pgc"

     { ECPGtrans(__LINE__, NULL, "rollback work");}
#line 52 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_SafeTapeUpdate.pgc"

     return(1);
}
