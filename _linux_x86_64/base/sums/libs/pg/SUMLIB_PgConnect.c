/* Processed by ecpg (4.10.0) */
/* These include files are added by the preprocessor */
#include <ecpglib.h>
#include <ecpgerrno.h>
#include <sqlca.h>
/* End of automatic include section */

#line 1 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PgConnect.pgc"
#include <SUM.h>
#include <stdlib.h>
#include <soi_error.h>
#include <printk.h>
#include <serverdefs.h>

int DS_ConnectDB(char *dbname)
{
/* exec sql begin declare section */
   
   

#line 10 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PgConnect.pgc"
  struct varchar_1  { int len; char arr[ 80 ]; }  target ;
 
#line 11 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PgConnect.pgc"
  struct varchar_2  { int len; char arr[ 80 ]; }  user ;
/* exec sql end declare section */
#line 12 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PgConnect.pgc"

  char *dbuser, *dbhost, *pgport;
  //char pgport[32];


    /* exec sql whenever sqlerror  goto  sqlerror1 ; */
#line 17 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PgConnect.pgc"


   
    //sprintf(pgport, SUMPGPORT); 
    //setenv("PGPORT", pgport, 1); //need to connect to new jsoc_sums db
    if(!(dbuser = (char *)getenv("USER"))) dbuser = SUMS_MANAGER;
#ifdef SUMDC
    //if(!(dbhost = (char *)getenv("DBHOSTNAME"))) dbhost = "dcs0";
    //new 31Oct2008 always use localhost for dcs
    dbhost = "localhost";
#else
    //if(!(dbhost = (char *)getenv("DBHOSTNAME"))) dbhost = SERVER;
    if(!(dbhost = (char *)getenv("DBHOSTNAME"))) dbhost = SUMS_DB_HOST;
#endif

  if (!(pgport = getenv("SUMPGPORT"))) {
    sprintf(target.arr, "%s@%s:%s", dbname, dbhost, SUMPGPORT); //use compiled
  }
  else {
    sprintf(target.arr, "%s@%s:%s", dbname, dbhost, pgport); //use env
  }
    target.len = strlen(target.arr);
    sprintf(user.arr, "%s", dbuser);
    user.len = strlen(user.arr);

    	{ ECPGconnect(__LINE__, 0, target.arr , user.arr , NULL , NULL, 0); 
#line 42 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PgConnect.pgc"

if (sqlca.sqlcode < 0) goto sqlerror1;}
#line 42 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PgConnect.pgc"

    printk("Connect to Postgres host=%s db=%s user=%s Ok\n", 
		dbhost, dbname, dbuser);
    { ECPGtrans(__LINE__, NULL, "commit work");
#line 45 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PgConnect.pgc"

if (sqlca.sqlcode < 0) goto sqlerror1;}
#line 45 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PgConnect.pgc"

    return NO_ERROR;
	
sqlerror1:
	printk("Error in DS_ConnectDB target=%s user=%s\n", 
		target.arr, dbuser); 
	printk("% .80s \n", sqlca.sqlerrm.sqlerrmc); 
    /* exec sql whenever sqlerror  continue ; */
#line 52 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PgConnect.pgc"

    { ECPGtrans(__LINE__, NULL, "rollback work");}
#line 53 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PgConnect.pgc"

    return DS_CONNECT_DB;
}


int DS_DisConnectDB()
{

    /* exec sql whenever sqlerror  goto  sqlerror2 ; */
#line 61 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PgConnect.pgc"

    { ECPGdisconnect(__LINE__, "CURRENT");
#line 62 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PgConnect.pgc"

if (sqlca.sqlcode < 0) goto sqlerror2;}
#line 62 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PgConnect.pgc"

    printk("Disconnect from Postgres OK\n");
    return NO_ERROR;
	
sqlerror2:
	printk("Error in DS_DisConnectDB\n"); 
	printk("% .70s \n", sqlca.sqlerrm.sqlerrmc); 
    return DS_DISCONNECT_DB;
}

/* Quiet mode with no opening msg */
int DS_ConnectDB_Q(char *dbname)
{
/* exec sql begin declare section */
   
   

#line 76 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PgConnect.pgc"
  struct varchar_3  { int len; char arr[ 80 ]; }  target ;
 
#line 77 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PgConnect.pgc"
  struct varchar_4  { int len; char arr[ 80 ]; }  user ;
/* exec sql end declare section */
#line 78 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PgConnect.pgc"

  char *dbuser, *dbhost, *pgport;
  //char pgport[32];

    /* exec sql whenever sqlerror  goto  sqlerror3 ; */
#line 82 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PgConnect.pgc"


    //sprintf(pgport, SUMPGPORT); 
    //setenv("PGPORT", pgport, 1); //need to connect to new jsoc_sums db
    if(!(dbuser = (char *)getenv("USER"))) dbuser = SUMS_MANAGER;
#ifdef SUMDC
    if(!(dbhost = (char *)getenv("DBHOSTNAME"))) dbhost = "dcs0";
#else
    //if(!(dbhost = (char *)getenv("DBHOSTNAME"))) dbhost = SERVER;
    if(!(dbhost = (char *)getenv("DBHOSTNAME"))) dbhost = SUMS_DB_HOST;
#endif

  if (!(pgport = getenv("SUMPGPORT"))) {
    sprintf(target.arr, "%s@%s:%s", dbname, dbhost, SUMPGPORT); //use compiled
  }
  else {
    sprintf(target.arr, "%s@%s:%s", dbname, dbhost, pgport); //use env
  }
    target.len = strlen(target.arr);
    sprintf(user.arr, "%s", dbuser);
    user.len = strlen(user.arr);

        //printk("Target = %s\n", target.arr); //!!!TEMP
    	{ ECPGconnect(__LINE__, 0, target.arr , user.arr , NULL , NULL, 0); 
#line 105 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PgConnect.pgc"

if (sqlca.sqlcode < 0) goto sqlerror3;}
#line 105 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PgConnect.pgc"

    { ECPGtrans(__LINE__, NULL, "commit work");
#line 106 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PgConnect.pgc"

if (sqlca.sqlcode < 0) goto sqlerror3;}
#line 106 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PgConnect.pgc"

    return NO_ERROR;
	
sqlerror3:
	printk("Error in DS_ConnectDB_Q target=%s user=%s\n", 
		target.arr, dbuser); 
	printk("% .80s \n", sqlca.sqlerrm.sqlerrmc); 
    /* exec sql whenever sqlerror  continue ; */
#line 113 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PgConnect.pgc"

    { ECPGtrans(__LINE__, NULL, "rollback work");}
#line 114 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PgConnect.pgc"

    return DS_CONNECT_DB;
}


int DS_DisConnectDB_Q()
{

    /* exec sql whenever sqlerror  goto  sqlerror4 ; */
#line 122 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PgConnect.pgc"

    { ECPGdisconnect(__LINE__, "CURRENT");
#line 123 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PgConnect.pgc"

if (sqlca.sqlcode < 0) goto sqlerror4;}
#line 123 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PgConnect.pgc"

    return NO_ERROR;
	
sqlerror4:
	printk("Error in DS_DisConnectDB\n"); 
	printk("% .70s \n", sqlca.sqlerrm.sqlerrmc); 
    return DS_DISCONNECT_DB;
}

