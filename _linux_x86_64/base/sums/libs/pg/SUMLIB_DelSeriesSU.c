/* Processed by ecpg (4.10.0) */
/* These include files are added by the preprocessor */
#include <ecpglib.h>
#include <ecpgerrno.h>
#include <sqlca.h>
/* End of automatic include section */

#line 1 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DelSeriesSU.pgc"
/* SUMLIB_DelSeriesSU.pgc
*/
/* This is to update the sum_partn_alloc to status/substatus of
 * DADP/DADPDELSU. This is call from delseriesdo_1() in sum_svc_proc.c.
 * The delete_series program will make this call so that the sum_rm
 * program will NOT try to rm records in the Records.txt file in a SU dir
 * if the series has been deleted and the record # could potentially be
 * reused.
 * Called with the filename that has the list of sunum (i.e. ds_index) 
 * associated with the deleted series.
 */
#include <SUM.h>
#include <sum_rpc.h>
#include <unistd.h>
#include <printk.h>

int SUMLIB_DelSeriesSU(char *file, char *series);

int SUMLIB_DelSeriesSU(char *file, char *series)
{
/* exec sql begin declare section */
            

#line 22 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DelSeriesSU.pgc"
 long long l_ds_index ;
/* exec sql end declare section */
#line 23 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DelSeriesSU.pgc"

  FILE *fp;
  uint64_t index;
  char line[256];
  int i;
  int status = 0;
  int efound = 0;

    /* exec sql whenever sqlerror  goto  sqlerror ; */
#line 31 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DelSeriesSU.pgc"

    /* exec sql whenever not found  goto  sqlerror ; */
#line 32 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DelSeriesSU.pgc"

  printk("In SUMLIB_DelSeriesSU(%s):\n", series);

  if((fp=fopen(file, "r")) == NULL) {
    printk("Error in SUMLIB_DelSeriesSU\n"); 
    printk("Can't open the file %s\n", file);
    return(1);
  }

  while(fgets(line, 256, fp)) {
    if(line[0] == '#' || line[0] == '\n') continue;
    sscanf(line, "%ld", &index);
    printk("%ld\n", index); /* !!!TEMP */
    l_ds_index = (long long)index;

	{ ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "update SUM_PARTN_ALLOC set STATUS = 2 , EFFECTIVE_DATE = '0' , ARCHIVE_SUBSTATUS = 1024 where ds_index = $1 ", 
	ECPGt_long_long,&(l_ds_index),(long)1,(long)1,sizeof(long long), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EOIT, ECPGt_EORT);
#line 50 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DelSeriesSU.pgc"

if (sqlca.sqlcode == ECPG_NOT_FOUND) goto sqlerror;
#line 50 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DelSeriesSU.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 50 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DelSeriesSU.pgc"

    continue;
sqlerror:
	status = 1;
        if(!efound) {
          efound = 1;
	  printk("Error in SUMLIB_DelSeriesSU\n"); 
	  printk("% .70s \n", sqlca.sqlerrm.sqlerrmc); 
          printk("The sunum is probably no longer in SUMS.\n");
          printk("No further missing sunum will be reported.\n");
        }
    continue;
  }
  fclose(fp);

        { ECPGtrans(__LINE__, NULL, "commit work");
#line 65 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DelSeriesSU.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 65 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DelSeriesSU.pgc"

  return(status);
}


