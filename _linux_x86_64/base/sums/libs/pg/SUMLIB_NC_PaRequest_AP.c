/* Processed by ecpg (4.10.0) */
/* These include files are added by the preprocessor */
#include <ecpglib.h>
#include <ecpgerrno.h>
#include <sqlca.h>
/* End of automatic include section */

#line 1 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_NC_PaRequest_AP.pgc"
/* SUMLIB_NC_PaRequest_AP.pgc
 returns the archive pending list for the given groupset
*/
#include <SUM.h>
#include <sum_rpc.h>
#include <soi_error.h>
#include <printk.h>

/* !!Make sure no group# in more than one groupset unless group
shifting is done (as for group 6, see tapearc5.c) */
#define GS0 "2"
#define GS1 "3"
#define GS2 "4,5"
#define GS3 "0,1,8,9,11"
#define GS4 "6,7"
#define GS5 "6,7"  /* use 2nd tape drive for write */
#define GS6 "6,7"  /* use 3rd tape drive for write */
#define GS7 "310,311"
#define GS8 "10"

PADATA *NC_PaRequest_AP(int groupset);

PADATA *NC_PaRequest_AP(int groupset)
{
/* exec sql begin declare section */
   
   
   
     
   
   
   
     
   
   
   
   

#line 26 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_NC_PaRequest_AP.pgc"
 char stmt [ 512 ] ;
 
#line 27 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_NC_PaRequest_AP.pgc"
  struct varchar_1  { int len; char arr[ 80 ]; }  l_wd ;
 
#line 28 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_NC_PaRequest_AP.pgc"
  struct varchar_2  { int len; char arr[ 20 ]; }  l_eff_date ;
 
#line 29 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_NC_PaRequest_AP.pgc"
 long long l_sumid ;
 
#line 30 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_NC_PaRequest_AP.pgc"
 int l_status ;
 
#line 31 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_NC_PaRequest_AP.pgc"
 int group_id ;
 
#line 32 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_NC_PaRequest_AP.pgc"
 int safe_id ;
 
#line 33 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_NC_PaRequest_AP.pgc"
 long long ds_index ;
 
#line 34 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_NC_PaRequest_AP.pgc"
 int l_archsub ;
 
#line 35 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_NC_PaRequest_AP.pgc"
 double l_bytes ;
 
#line 36 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_NC_PaRequest_AP.pgc"
 char wd [ 80 ] ;
 
#line 37 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_NC_PaRequest_AP.pgc"
 char eff_date [ 20 ] ;
/* exec sql end declare section */
#line 38 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_NC_PaRequest_AP.pgc"


PADATA *ap = NULL;

//!!NOTE: don't make limit over 500
//char STMT[] = "SELECT WD, SUMID, STATUS, BYTES, EFFECTIVE_DATE, ARCHIVE_SUBSTATUS, GROUP_ID, SAFE_ID, DS_INDEX FROM SUM_PARTN_ALLOC WHERE (STATUS = 4 OR STATUS = 32) and GROUP_ID IN (%s) order by GROUP_ID limit 400";

char STMT[] = "SELECT WD, SUMID, STATUS, BYTES, EFFECTIVE_DATE, ARCHIVE_SUBSTATUS, GROUP_ID, SAFE_ID, DS_INDEX FROM SUM_PARTN_ALLOC WHERE (STATUS = 4 OR STATUS = 32) and GROUP_ID IN (%s) order by GROUP_ID,DS_INDEX limit 600";

//char STMT[] = "SELECT WD, SUMID, STATUS, BYTES, EFFECTIVE_DATE, ARCHIVE_SUBSTATUS, GROUP_ID, SAFE_ID, DS_INDEX FROM SUM_PARTN_ALLOC WHERE (STATUS = 4 OR STATUS = 32) and GROUP_ID IN (%s) order by GROUP_ID limit 3400";

//char STMT[] = "SELECT WD, SUMID, STATUS, BYTES, EFFECTIVE_DATE, ARCHIVE_SUBSTATUS, GROUP_ID, SAFE_ID, DS_INDEX FROM SUM_PARTN_ALLOC WHERE (STATUS = 4 OR STATUS = 32) and GROUP_ID IN (0,1,2,3,4,5,6,7,8,9,11,102,103,104,105) order by GROUP_ID limit 3400";

//char STMT[] = "SELECT WD, SUMID, STATUS, BYTES, EFFECTIVE_DATE, ARCHIVE_SUBSTATUS, GROUP_ID, SAFE_ID, DS_INDEX FROM SUM_PARTN_ALLOC WHERE (STATUS = 4 OR STATUS = 32) and GROUP_ID IN (4,5) limit 3400";

//char STMT[] = "SELECT WD, SUMID, STATUS, BYTES, EFFECTIVE_DATE, ARCHIVE_SUBSTATUS, GROUP_ID, SAFE_ID, DS_INDEX FROM SUM_PARTN_ALLOC WHERE (STATUS = 4 OR STATUS = 32) and GROUP_ID = 1 limit 10000";


#line 1 "/usr/include/postgresql/sqlca.h"
#ifndef POSTGRES_SQLCA_H
#define POSTGRES_SQLCA_H

#ifndef PGDLLIMPORT
#if  defined(WIN32) || defined(__CYGWIN__)
#define PGDLLIMPORT __declspec (dllimport)
#else
#define PGDLLIMPORT
#endif   /* __CYGWIN__ */
#endif   /* PGDLLIMPORT */

#define SQLERRMC_LEN	150

#ifdef __cplusplus
extern		"C"
{
#endif

struct sqlca_t
{
	char		sqlcaid[8];
	long		sqlabc;
	long		sqlcode;
	struct
	{
		int			sqlerrml;
		char		sqlerrmc[SQLERRMC_LEN];
	}			sqlerrm;
	char		sqlerrp[8];
	long		sqlerrd[6];
	/* Element 0: empty						*/
	/* 1: OID of processed tuple if applicable			*/
	/* 2: number of rows processed				*/
	/* after an INSERT, UPDATE or				*/
	/* DELETE statement					*/
	/* 3: empty						*/
	/* 4: empty						*/
	/* 5: empty						*/
	char		sqlwarn[8];
	/* Element 0: set to 'W' if at least one other is 'W'	*/
	/* 1: if 'W' at least one character string		*/
	/* value was truncated when it was			*/
	/* stored into a host variable.             */

	/*
	 * 2: if 'W' a (hopefully) non-fatal notice occurred
	 */	/* 3: empty */
	/* 4: empty						*/
	/* 5: empty						*/
	/* 6: empty						*/
	/* 7: empty						*/

	char		sqlstate[5];
};

struct sqlca_t *ECPGget_sqlca(void);

#ifndef POSTGRES_ECPG_INTERNAL
#define sqlca (*ECPGget_sqlca())
#endif

#ifdef __cplusplus
}
#endif

#endif

#line 55 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_NC_PaRequest_AP.pgc"


    printk("NC_PaRequest_AP \n");  
    printk("Special Version for select group_ids\n");

    /* exec sql whenever sqlerror  goto  sqlerror ; */
#line 60 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_NC_PaRequest_AP.pgc"

  switch(groupset) {
  case 0:
    sprintf(stmt, STMT, GS0);
    break;
  case 1:
    sprintf(stmt, STMT, GS1);
    break;
  case 2:
    sprintf(stmt, STMT, GS2);
    break;
  case 3:
    sprintf(stmt, STMT, GS3);
    break;
  case 4:
    sprintf(stmt, STMT, GS4);
    break;
  case 5:
    sprintf(stmt, STMT, GS5);
    break;
  case 6:
    sprintf(stmt, STMT, GS6);
    break;
  case 7:
    sprintf(stmt, STMT, GS7);
    break;
  case 8:
    sprintf(stmt, STMT, GS8);
    break;
  default:
    printk("Error in NC_PaRequest_AP Illegal groupset arg\n");
    return NULL;
    break;
  }

     /* exec sql whenever sqlerror  goto  sqlerror ; */
#line 95 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_NC_PaRequest_AP.pgc"

     { ECPGprepare(__LINE__, NULL, 0, "query", stmt);
#line 96 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_NC_PaRequest_AP.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 96 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_NC_PaRequest_AP.pgc"

     /* declare reqcursor cursor for $1 */
#line 97 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_NC_PaRequest_AP.pgc"

     { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "declare reqcursor cursor for $1", 
	ECPGt_char_variable,(ECPGprepared_statement(NULL, "query", __LINE__)),(long)1,(long)1,(1)*sizeof(char), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EOIT, ECPGt_EORT);
#line 98 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_NC_PaRequest_AP.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 98 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_NC_PaRequest_AP.pgc"

     /* exec sql whenever not found  goto  end_fetch ; */
#line 99 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_NC_PaRequest_AP.pgc"


  for(; ; ){
		{ ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "fetch reqcursor", ECPGt_EOIT, 
	ECPGt_varchar,&(l_wd),(long)80,(long)1,sizeof(struct varchar_1), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_long_long,&(l_sumid),(long)1,(long)1,sizeof(long long), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_int,&(l_status),(long)1,(long)1,sizeof(int), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_double,&(l_bytes),(long)1,(long)1,sizeof(double), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_varchar,&(l_eff_date),(long)20,(long)1,sizeof(struct varchar_2), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_int,&(l_archsub),(long)1,(long)1,sizeof(int), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_int,&(group_id),(long)1,(long)1,sizeof(int), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_int,&(safe_id),(long)1,(long)1,sizeof(int), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_long_long,&(ds_index),(long)1,(long)1,sizeof(long long), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EORT);
#line 102 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_NC_PaRequest_AP.pgc"

if (sqlca.sqlcode == ECPG_NOT_FOUND) goto end_fetch;
#line 102 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_NC_PaRequest_AP.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 102 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_NC_PaRequest_AP.pgc"


		strcpy(wd, l_wd.arr);
		strcpy(eff_date, l_eff_date.arr);

		switch(l_status) {    /* add the entry to the linked list */
		case DAAP:
		case DASAP:
		  setpadata(&ap, wd, (unsigned long long)l_sumid, l_bytes, l_status, l_archsub, eff_date, group_id, safe_id, (unsigned long long)ds_index);
		  break;
		default:
                  printk("\n this shouldn't happen \n");
		  break;
		 }
    }

end_fetch:
	{ ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "close reqcursor", ECPGt_EOIT, ECPGt_EORT);
#line 119 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_NC_PaRequest_AP.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 119 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_NC_PaRequest_AP.pgc"

        { ECPGtrans(__LINE__, NULL, "commit");
#line 120 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_NC_PaRequest_AP.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 120 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_NC_PaRequest_AP.pgc"

    return ap;
	
sqlerror:
    printk("Error in NC_PaRequest_AP\n");
    printk("% .70s \n", sqlca.sqlerrm.sqlerrmc);
    /* exec sql whenever sqlerror  continue ; */
#line 126 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_NC_PaRequest_AP.pgc"

    { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "close reqcursor", ECPGt_EOIT, ECPGt_EORT);}
#line 127 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_NC_PaRequest_AP.pgc"

    { ECPGtrans(__LINE__, NULL, "rollback work");}
#line 128 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_NC_PaRequest_AP.pgc"

    //continue;		//get next groupid
    return NULL;
return ap;
}

