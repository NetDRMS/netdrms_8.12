/* Processed by ecpg (4.10.0) */
/* These include files are added by the preprocessor */
#include <ecpglib.h>
#include <ecpgerrno.h>
#include <sqlca.h>
/* End of automatic include section */

#line 1 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeUpdate.pgc"
/* SUMLIB_TapeUpdate.pgc
*/
/* This is to update the sum_tape table with avail_blocks, 
 * next_write_file_number and last_write for the given tape barcode id. 
 * Updated later to pass in the next file# on the tape.
 * Returns 0 on failure, else the current file number (that was just written).
 * NOTE: file numbers are initialized to 1 in the sum_tape db table.
 * Also function to get the next file number to be written to the given tape.
 * NOTE: for the datacapture system the tellblock is always 0 and the 
 * avail_blocks is update from the totalbytes just written.
 */
#include <SUM.h>
#include <tape.h>
#include <sum_rpc.h>
#include <printk.h>

int SUMLIB_TapeUpdate(char *tapeid, int tapenxtfn,  uint64_t tellblock, double totalbytes);
int SUMLIB_TapeFilenumber(char *tapeid);

int SUMLIB_TapeUpdate(char *tapeid, int tapenxtfn, uint64_t tellblock, double totalbytes)
{
/* exec sql begin declare section */
   
   
      
    
    

#line 23 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeUpdate.pgc"
  struct varchar_1  { int len; char arr[ 32 ]; }  l_date ;
 
#line 24 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeUpdate.pgc"
  struct varchar_2  { int len; char arr[ 20 ]; }  l_tapeid ;
 
#line 25 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeUpdate.pgc"
 long long l_availblocks ;
 
#line 26 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeUpdate.pgc"
 int l_nxtwrtfn ;
 
#line 27 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeUpdate.pgc"
 int l_closed ;
/* exec sql end declare section */
#line 28 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeUpdate.pgc"

  int filen, numblocks;

    sprintf(l_tapeid.arr, "%s", tapeid);
    l_tapeid.len = strlen(l_tapeid.arr);

    /* exec sql whenever sqlerror  goto  sqlerror ; */
#line 34 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeUpdate.pgc"


    /* exec sql whenever not found  goto  sqlerror ; */
#line 36 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeUpdate.pgc"
 

   sprintf(l_date.arr, "%s", get_datetime());
   l_date.len = strlen(l_date.arr);

   { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "select nxtwrtfn , avail_blocks from SUM_TAPE where TAPEID = $1 ", 
	ECPGt_varchar,&(l_tapeid),(long)20,(long)1,sizeof(struct varchar_2), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EOIT, 
	ECPGt_int,&(l_nxtwrtfn),(long)1,(long)1,sizeof(int), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_long_long,&(l_availblocks),(long)1,(long)1,sizeof(long long), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EORT);
#line 42 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeUpdate.pgc"

if (sqlca.sqlcode == ECPG_NOT_FOUND) goto sqlerror;
#line 42 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeUpdate.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 42 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeUpdate.pgc"


   filen = tapenxtfn - 1;
   //l_nxtwrtfn++;	/* inc to next file # */
   l_nxtwrtfn = tapenxtfn;    //next file# on tape

    if(tellblock == 0) {	/* get availblocks from totalbytes */
      numblocks = (int)(totalbytes/512.0);
      if(numblocks >= l_availblocks) l_availblocks = 0;
      else l_availblocks = l_availblocks - numblocks;
    }
    else {
      if(tellblock >= MAX_AVAIL_BLOCKS) {
        l_availblocks = 0;
      }
      else {
        l_availblocks = MAX_AVAIL_BLOCKS - tellblock; /* blocks left on tape */
      }
    }

	{ ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "update SUM_TAPE set AVAIL_BLOCKS = $1  , NXTWRTFN = $2  , LAST_WRITE = $3  where TAPEID = $4 ", 
	ECPGt_long_long,&(l_availblocks),(long)1,(long)1,sizeof(long long), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_int,&(l_nxtwrtfn),(long)1,(long)1,sizeof(int), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_varchar,&(l_date),(long)32,(long)1,sizeof(struct varchar_1), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_varchar,&(l_tapeid),(long)20,(long)1,sizeof(struct varchar_2), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EOIT, ECPGt_EORT);
#line 66 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeUpdate.pgc"

if (sqlca.sqlcode == ECPG_NOT_FOUND) goto sqlerror;
#line 66 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeUpdate.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 66 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeUpdate.pgc"

        { ECPGtrans(__LINE__, NULL, "commit work");
#line 67 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeUpdate.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 67 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeUpdate.pgc"


    //close tape if no more space or file number reached 1200 (this is a 
    //temporary fix to the problem of a tape not unlocking 12/17/2010
    //if((l_availblocks < TAR_FILE_SZ_BLKS) || (filen >= 1200)) {  
    if((l_availblocks < TAR_FILE_SZ_BLKS)) {  
      //if(filen >= 1200) printk("File number %d >= 1200\n", filen);
      if(SUMLIB_TapeClose(tapeid)) {
        printk("Error in SUMLIB_TapeUpdate\n");
        return(0);
      }
//Now ret a neg filen for both systems 12/15/2010
//#ifdef SUMDC
      /* return neg filen to indicate that tape was closed */
      filen = -filen;
//#endif
    }
    return(filen);

sqlerror:
	printk("Error in SUMLIB_TapeUpdate\n"); 
	printk("% .70s \n", sqlca.sqlerrm.sqlerrmc); 
    /* exec sql whenever sqlerror  continue ; */
#line 89 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeUpdate.pgc"

    { ECPGtrans(__LINE__, NULL, "rollback work");}
#line 90 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeUpdate.pgc"

    return 0;
}


/* Get the next file number to write to the given tapeid from the sum_tape
 * table. Return 0 on error.
*/
int SUMLIB_TapeFilenumber(char *tapeid)
{

/* exec sql begin declare section */
   
    

#line 102 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeUpdate.pgc"
  struct varchar_3  { int len; char arr[ 20 ]; }  l_tapeid ;
 
#line 103 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeUpdate.pgc"
 int l_nxtwrtfn ;
/* exec sql end declare section */
#line 104 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeUpdate.pgc"

  int filen;

    sprintf(l_tapeid.arr, "%s", tapeid);
    l_tapeid.len = strlen(l_tapeid.arr);

    /* exec sql whenever sqlerror  goto  sqlerror ; */
#line 110 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeUpdate.pgc"

    /* exec sql whenever not found  goto  sqlerror ; */
#line 111 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeUpdate.pgc"
 

   { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "select nxtwrtfn from SUM_TAPE where TAPEID = $1 ", 
	ECPGt_varchar,&(l_tapeid),(long)20,(long)1,sizeof(struct varchar_3), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EOIT, 
	ECPGt_int,&(l_nxtwrtfn),(long)1,(long)1,sizeof(int), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EORT);
#line 114 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeUpdate.pgc"

if (sqlca.sqlcode == ECPG_NOT_FOUND) goto sqlerror;
#line 114 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeUpdate.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 114 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeUpdate.pgc"


   filen = l_nxtwrtfn;	/* this is the next file# to write to this tape */
   { ECPGtrans(__LINE__, NULL, "commit work");
#line 117 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeUpdate.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 117 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeUpdate.pgc"

   return(filen);

sqlerror:
	printk("Error in SUMLIB_TapeFilenumber(%s)\n", tapeid); 
	printk("% .70s \n", sqlca.sqlerrm.sqlerrmc); 
    { ECPGtrans(__LINE__, NULL, "rollback work");
#line 123 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeUpdate.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 123 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_TapeUpdate.pgc"

    return 0;
}
