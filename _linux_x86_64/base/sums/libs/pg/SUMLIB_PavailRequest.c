/* Processed by ecpg (4.10.0) */
/* These include files are added by the preprocessor */
#include <ecpglib.h>
#include <ecpgerrno.h>
#include <sqlca.h>
/* End of automatic include section */

#line 1 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailRequest.pgc"
/* DS_PavailRequest.pc
 * Called to update memory tables with partn available info. 
 * Basically downloading information from the partn_avail table to memory 
 * structures. 
 */
#include <SUM.h>
#include <sum_rpc.h>
#include <soi_error.h>
#include <printk.h>

int cmp_pds_num(const void *a, const void *b);
int DS_PavailRequest();
int DS_PavailRequestEx();  /* extended w/new column pds_set_prime 7/23/2012 */
int DS_PavailRequest2();

PART ptab[MAX_PART]; 		/* used in sum_rm.c */
PART ptabx[MAX_PART]; 		/* used in sum_svc.c */

int DS_PavailRequest()
{
/* exec sql begin declare section */
   
   
   
   
   
   

#line 22 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailRequest.pgc"
  struct varchar_1  { int len; char arr[ 80 ]; }  l_name ;
 
#line 23 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailRequest.pgc"
 double l_total ;
 
#line 24 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailRequest.pgc"
 double l_avail ;
 
#line 25 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailRequest.pgc"
 int l_pnum ;
 
#line 26 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailRequest.pgc"
 char name [ 80 ] ;
 
#line 27 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailRequest.pgc"
 char * namestr ;
/* exec sql end declare section */
#line 28 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailRequest.pgc"

int i;

    printk("DS_PavailRequest \n");  
    /* exec sql whenever sqlerror  goto  sqlerror ; */
#line 32 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailRequest.pgc"


	/* declare pavail cursor for select PARTN_NAME , TOTAL_BYTES , AVAIL_BYTES , PDS_SET_NUM from SUM_PARTN_AVAIL */
#line 36 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailRequest.pgc"


	{ ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "declare pavail cursor for select PARTN_NAME , TOTAL_BYTES , AVAIL_BYTES , PDS_SET_NUM from SUM_PARTN_AVAIL", ECPGt_EOIT, ECPGt_EORT);
#line 38 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailRequest.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 38 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailRequest.pgc"


    /* exec sql whenever not found  goto  end_fetch ; */
#line 40 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailRequest.pgc"
 
	
	for(i=0; i<MAX_PART-1; i++){
		{ ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "fetch pavail", ECPGt_EOIT, 
	ECPGt_varchar,&(l_name),(long)80,(long)1,sizeof(struct varchar_1), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_double,&(l_total),(long)1,(long)1,sizeof(double), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_double,&(l_avail),(long)1,(long)1,sizeof(double), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_int,&(l_pnum),(long)1,(long)1,sizeof(int), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EORT);
#line 43 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailRequest.pgc"

if (sqlca.sqlcode == ECPG_NOT_FOUND) goto end_fetch;
#line 43 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailRequest.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 43 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailRequest.pgc"

		strcpy(name, l_name.arr);
		namestr = (char *) malloc (sizeof(name));
		strcpy(namestr, name);
 
    	/* setting up memory tables here */
		ptab[i].name = namestr;
		ptab[i].bytes_total = l_total;
		ptab[i].bytes_left = l_avail;
		ptab[i].bytes_alloc = 0.0;
		ptab[i].pds_set_num = l_pnum;
	}
	if (i==MAX_PART-1){
		/*printf("more partitions than reqd \n"); */
                { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "close pavail", ECPGt_EOIT, ECPGt_EORT);
#line 57 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailRequest.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 57 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailRequest.pgc"

                { ECPGtrans(__LINE__, NULL, "commit work");
#line 58 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailRequest.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 58 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailRequest.pgc"

		return DS_DATA_QRY;
	} 

end_fetch:
        /* sort ptab[] in ascending pds_set_num */
        qsort(ptab, i, sizeof(PART), &cmp_pds_num);
        /* put a terminating entry in the in-memory table */
	ptab[i].name = NULL;	
	ptab[i].pds_set_num = -1;	
	{ ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "close pavail", ECPGt_EOIT, ECPGt_EORT);
#line 68 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailRequest.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 68 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailRequest.pgc"

    { ECPGtrans(__LINE__, NULL, "commit work");
#line 69 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailRequest.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 69 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailRequest.pgc"

    return NO_ERROR;
	
sqlerror:
    printk("Error in DS_PavailRequest\n");
    printk("% .70s \n", sqlca.sqlerrm.sqlerrmc);
    /* exec sql whenever sqlerror  continue ; */
#line 75 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailRequest.pgc"

    { ECPGtrans(__LINE__, NULL, "rollback work");}
#line 76 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailRequest.pgc"

    return DS_DATA_QRY;
}

int DS_PavailRequestEx()
{
/* exec sql begin declare section */
   
   
   
   
   
   
   

#line 83 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailRequest.pgc"
  struct varchar_2  { int len; char arr[ 80 ]; }  l_name ;
 
#line 84 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailRequest.pgc"
 double l_total ;
 
#line 85 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailRequest.pgc"
 double l_avail ;
 
#line 86 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailRequest.pgc"
 int l_pnum ;
 
#line 87 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailRequest.pgc"
 int l_pnumprime ;
 
#line 88 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailRequest.pgc"
 char name [ 80 ] ;
 
#line 89 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailRequest.pgc"
 char * namestr ;
/* exec sql end declare section */
#line 90 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailRequest.pgc"

int i;

    printk("DS_PavailRequestEx \n");  
    /* exec sql whenever sqlerror  goto  sqlerror ; */
#line 94 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailRequest.pgc"


	/* declare pavailx cursor for select PARTN_NAME , TOTAL_BYTES , AVAIL_BYTES , PDS_SET_NUM , PDS_SET_PRIME from SUM_PARTN_AVAIL */
#line 98 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailRequest.pgc"


	{ ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "declare pavailx cursor for select PARTN_NAME , TOTAL_BYTES , AVAIL_BYTES , PDS_SET_NUM , PDS_SET_PRIME from SUM_PARTN_AVAIL", ECPGt_EOIT, ECPGt_EORT);
#line 100 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailRequest.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 100 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailRequest.pgc"


    /* exec sql whenever not found  goto  end_fetch ; */
#line 102 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailRequest.pgc"
 
	
	for(i=0; i<MAX_PART-1; i++){
		{ ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "fetch pavailx", ECPGt_EOIT, 
	ECPGt_varchar,&(l_name),(long)80,(long)1,sizeof(struct varchar_2), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_double,&(l_total),(long)1,(long)1,sizeof(double), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_double,&(l_avail),(long)1,(long)1,sizeof(double), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_int,&(l_pnum),(long)1,(long)1,sizeof(int), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_int,&(l_pnumprime),(long)1,(long)1,sizeof(int), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EORT);
#line 105 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailRequest.pgc"

if (sqlca.sqlcode == ECPG_NOT_FOUND) goto end_fetch;
#line 105 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailRequest.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 105 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailRequest.pgc"

		strcpy(name, l_name.arr);
		namestr = (char *) malloc (sizeof(name));
		strcpy(namestr, name);
 
    	/* setting up memory tables here */
		ptab[i].name = namestr;
		ptab[i].bytes_total = l_total;
		ptab[i].bytes_left = l_avail;
		ptab[i].bytes_alloc = 0.0;
		ptab[i].pds_set_num = l_pnum;
		ptab[i].pds_set_prime = l_pnumprime;
	}
	if (i==MAX_PART-1){
		/*printf("more partitions than reqd \n"); */
                { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "close pavailx", ECPGt_EOIT, ECPGt_EORT);
#line 120 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailRequest.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 120 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailRequest.pgc"

                { ECPGtrans(__LINE__, NULL, "commit work");
#line 121 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailRequest.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 121 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailRequest.pgc"

		return DS_DATA_QRY;
	} 

end_fetch:
        /* sort ptab[] in ascending pds_set_num */
        qsort(ptab, i, sizeof(PART), &cmp_pds_num);
        /* put a terminating entry in the in-memory table */
	ptab[i].name = NULL;	
	ptab[i].pds_set_num = -1;	
	{ ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "close pavailx", ECPGt_EOIT, ECPGt_EORT);
#line 131 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailRequest.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 131 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailRequest.pgc"

    { ECPGtrans(__LINE__, NULL, "commit work");
#line 132 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailRequest.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 132 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailRequest.pgc"

    return NO_ERROR;
	
sqlerror:
    printk("Error in DS_PavailRequestEx\n");
    printk("% .70s \n", sqlca.sqlerrm.sqlerrmc);
    /* exec sql whenever sqlerror  continue ; */
#line 138 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailRequest.pgc"

    { ECPGtrans(__LINE__, NULL, "rollback work");}
#line 139 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailRequest.pgc"

    return DS_DATA_QRY;
}

/* Like above, but sets up the sum_svc table instead. */
int DS_PavailRequest2()
{
/* exec sql begin declare section */
   
   
   
   
   
   

#line 147 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailRequest.pgc"
  struct varchar_3  { int len; char arr[ 80 ]; }  l_name ;
 
#line 148 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailRequest.pgc"
 double l_total ;
 
#line 149 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailRequest.pgc"
 double l_avail ;
 
#line 150 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailRequest.pgc"
 int l_pnum ;
 
#line 151 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailRequest.pgc"
 char name [ 80 ] ;
 
#line 152 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailRequest.pgc"
 char * namestr ;
/* exec sql end declare section */
#line 153 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailRequest.pgc"

int i, setnum, pnum;

extern int part_index[];	/* defined in SUMLIB_PavailGet.pgc */
extern int part_index_init[];	/* defined in SUMLIB_PavailGet.pgc */

    //printk("DS_PavailRequest2 \n");  

    /* exec sql whenever sqlerror  goto  sqlerror ; */
#line 161 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailRequest.pgc"


	/* declare pavail2 cursor for select PARTN_NAME , TOTAL_BYTES , AVAIL_BYTES , PDS_SET_NUM from SUM_PARTN_AVAIL */
#line 165 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailRequest.pgc"


	{ ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "declare pavail2 cursor for select PARTN_NAME , TOTAL_BYTES , AVAIL_BYTES , PDS_SET_NUM from SUM_PARTN_AVAIL", ECPGt_EOIT, ECPGt_EORT);
#line 167 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailRequest.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 167 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailRequest.pgc"


    /* exec sql whenever not found  goto  end_fetch ; */
#line 169 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailRequest.pgc"
 
	
	for(i=0; i<MAX_PART-1; i++){
		{ ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "fetch pavail2", ECPGt_EOIT, 
	ECPGt_varchar,&(l_name),(long)80,(long)1,sizeof(struct varchar_3), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_double,&(l_total),(long)1,(long)1,sizeof(double), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_double,&(l_avail),(long)1,(long)1,sizeof(double), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_int,&(l_pnum),(long)1,(long)1,sizeof(int), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EORT);
#line 172 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailRequest.pgc"

if (sqlca.sqlcode == ECPG_NOT_FOUND) goto end_fetch;
#line 172 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailRequest.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 172 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailRequest.pgc"

		strcpy(name, l_name.arr);
		namestr = (char *) malloc (sizeof(name));
		strcpy(namestr, name);
 
    	/* setting up memory tables here */
		ptabx[i].name = namestr;
		ptabx[i].bytes_total = l_total;
		ptabx[i].bytes_left = l_avail;
		ptabx[i].bytes_alloc = 0.0;
		ptabx[i].pds_set_num = l_pnum;
	}
	if (i==MAX_PART-1){
		/*printf("more partitions than reqd \n"); */
                { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "close pavail2", ECPGt_EOIT, ECPGt_EORT);
#line 186 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailRequest.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 186 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailRequest.pgc"

                { ECPGtrans(__LINE__, NULL, "commit work");
#line 187 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailRequest.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 187 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailRequest.pgc"

		return DS_DATA_QRY;
	} 

end_fetch:
        /* sort ptabx[] in ascending pds_set_num */
        qsort(ptabx, i, sizeof(PART), &cmp_pds_num);
        /* put a terminating entry in the in-memory table */
	ptabx[i].name = NULL;	
	ptabx[i].pds_set_num = -1;	
        pnum = i;
        /* set up part_index[] in NC_PavailGet for where each PDS set 
         * starts. Notice: The qsort above has made the PDS sets  
         * sequential in the ptabx table. */
        setnum = -1;
        for(i=0; i < pnum ; i++) {
          if(ptabx[i].pds_set_num != setnum) {
            //setnum++;         //N.G. assumes no missing pds_set_num
            setnum = ptabx[i].pds_set_num;
            part_index[setnum] = i;
            part_index_init[setnum] = i;
          }
        }
	{ ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "close pavail2", ECPGt_EOIT, ECPGt_EORT);
#line 210 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailRequest.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 210 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailRequest.pgc"

    { ECPGtrans(__LINE__, NULL, "commit work");
#line 211 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailRequest.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 211 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailRequest.pgc"

    return NO_ERROR;
	
sqlerror:
    printk("Error in DS_PavailRequest2\n");
    printk("% .70s \n", sqlca.sqlerrm.sqlerrmc);
    /* exec sql whenever sqlerror  continue ; */
#line 217 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailRequest.pgc"

    { ECPGtrans(__LINE__, NULL, "rollback work");}
#line 218 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailRequest.pgc"

    return DS_DATA_QRY;
}

int cmp_pds_num(const void *a, const void *b)
{
  PART *x=(PART *)a, *y=(PART *)b;
  char xname[1000], yname[1000];
  int n;

  if(x->pds_set_num < y->pds_set_num) return(-1);
  if(x->pds_set_num > y->pds_set_num) return(1);

// reverse partition names before sorting  04-27-2015 Keh-Cheng
  n = strlen(x->name); xname[n] = 0;
  for (int i=0; i<n; ++i) xname[i] = x->name[n-1-i]; 
  n = strlen(y->name); yname[n] = 0;
  for (int i=0; i<n; ++i) yname[i] = y->name[n-1-i]; 
    
  return strcmp(xname, yname);
}
