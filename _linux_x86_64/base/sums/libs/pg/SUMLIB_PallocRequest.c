/* Processed by ecpg (4.10.0) */
/* These include files are added by the preprocessor */
#include <ecpglib.h>
#include <ecpgerrno.h>
#include <sqlca.h>
/* End of automatic include section */

#line 1 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PallocRequest.pgc"
/* SUMLIB_PallocRequest.pc
 * Called by SUM_Init to update memory tables with partn alloc info. 
 */
/* !!!!!!!!!!!!!!!!!!NOTE: this is now vestigial in SUMS !!!!!!!!
*/
#include <SUM.h>
#include <sum_rpc.h>
#include <soi_error.h>
#include <printk.h>

int DS_PallocRequest();

int DS_PallocRequest()
{
/* exec sql begin declare section */
   
   
     
   
   
   
     
   
   

#line 16 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PallocRequest.pgc"
  struct varchar_1  { int len; char arr[ 80 ]; }  l_wd ;
 
#line 17 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PallocRequest.pgc"
  struct varchar_2  { int len; char arr[ 20 ]; }  l_eff_date ;
 
#line 18 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PallocRequest.pgc"
 long long l_uid ;
 
#line 19 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PallocRequest.pgc"
 int l_status ;
 
#line 20 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PallocRequest.pgc"
 int group_id ;
 
#line 21 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PallocRequest.pgc"
 int safe_id ;
 
#line 22 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PallocRequest.pgc"
 long long ds_index ;
 
#line 23 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PallocRequest.pgc"
 int l_archsub ;
 
#line 24 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PallocRequest.pgc"
 double l_bytes ;
/* exec sql end declare section */
#line 25 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PallocRequest.pgc"

  char wd[80];
  char eff_date[20];
  char *destptr;

extern PADATA *pahdr_rw;
extern PADATA *pahdr_ro;
extern PADATA *pahdr_dp;
extern PADATA *pahdr_ap;

    printk("DS_PallocRequest \n");  

    /* exec sql whenever sqlerror  goto  sqlerror ; */
#line 37 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PallocRequest.pgc"


    /* declare palloc cursor for select WD , SUMID , STATUS , BYTES , EFFECTIVE_DATE , ARCHIVE_SUBSTATUS , GROUP_ID , SAFE_ID , DS_INDEX from SUM_PARTN_ALLOC */
#line 41 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PallocRequest.pgc"


	{ ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "declare palloc cursor for select WD , SUMID , STATUS , BYTES , EFFECTIVE_DATE , ARCHIVE_SUBSTATUS , GROUP_ID , SAFE_ID , DS_INDEX from SUM_PARTN_ALLOC", ECPGt_EOIT, ECPGt_EORT);
#line 43 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PallocRequest.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 43 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PallocRequest.pgc"


    /* exec sql whenever not found  goto  end_fetch ; */
#line 45 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PallocRequest.pgc"
 
	
	for( ; ; ){
		{ ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "fetch palloc", ECPGt_EOIT, 
	ECPGt_varchar,&(l_wd),(long)80,(long)1,sizeof(struct varchar_1), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_long_long,&(l_uid),(long)1,(long)1,sizeof(long long), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_int,&(l_status),(long)1,(long)1,sizeof(int), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_double,&(l_bytes),(long)1,(long)1,sizeof(double), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_varchar,&(l_eff_date),(long)20,(long)1,sizeof(struct varchar_2), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_int,&(l_archsub),(long)1,(long)1,sizeof(int), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_int,&(group_id),(long)1,(long)1,sizeof(int), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_int,&(safe_id),(long)1,(long)1,sizeof(int), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_long_long,&(ds_index),(long)1,(long)1,sizeof(long long), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EORT);
#line 48 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PallocRequest.pgc"

if (sqlca.sqlcode == ECPG_NOT_FOUND) goto end_fetch;
#line 48 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PallocRequest.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 48 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PallocRequest.pgc"


		destptr = strcpy(wd, l_wd.arr);
		strcpy(eff_date, l_eff_date.arr);
		/* setting up memory tables here */
		switch(l_status) {    /* add the entry to the proper list */
	    case DARW:
		  setpadata(&pahdr_rw, wd, (unsigned long long)l_uid, l_bytes, l_status, l_archsub, eff_date, group_id, safe_id, (unsigned long long)ds_index);
		  break;
	    case DADP:
		  setpadata(&pahdr_dp, wd, (unsigned long long)l_uid, l_bytes, l_status, l_archsub, eff_date, group_id, safe_id, (unsigned long long)ds_index);
		  break;
	    case DAAP:
		  setpadata(&pahdr_ap, wd, (unsigned long long)l_uid, l_bytes, l_status, l_archsub, eff_date, group_id, safe_id, (unsigned long long)ds_index);
                  break;
	    case DASAP:
		  setpadata(&pahdr_ap, wd, (unsigned long long)l_uid, l_bytes, l_status, l_archsub, eff_date, group_id, safe_id, (unsigned long long)ds_index);
		  break;
	    case DARO:
		  setpadata(&pahdr_ro, wd, (unsigned long long)l_uid, l_bytes, l_status, l_archsub, eff_date, group_id, safe_id, (unsigned long long)ds_index);
		  break;
	    case DAPERM:
		  break;
	    default:
		 printk("Unknown status in sum_partn_alloc database\n");
                 { ECPGtrans(__LINE__, NULL, "rollback work");
#line 73 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PallocRequest.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 73 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PallocRequest.pgc"

		 return DS_DATA_QRY;
		 break;
		 }
    }

end_fetch:
	{ ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "close palloc", ECPGt_EOIT, ECPGt_EORT);
#line 80 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PallocRequest.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 80 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PallocRequest.pgc"

    { ECPGtrans(__LINE__, NULL, "commit work");
#line 81 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PallocRequest.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 81 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PallocRequest.pgc"

    return NO_ERROR;
	
sqlerror:
    printk("Error in DS_PallocRequest\n");
    printk("% .70s \n", sqlca.sqlerrm.sqlerrmc);
    /* exec sql whenever sqlerror  continue ; */
#line 87 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PallocRequest.pgc"

    { ECPGtrans(__LINE__, NULL, "rollback work");}
#line 88 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PallocRequest.pgc"

    return DS_DATA_QRY;
}
