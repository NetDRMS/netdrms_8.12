/* Processed by ecpg (4.10.0) */
/* These include files are added by the preprocessor */
#include <ecpglib.h>
#include <ecpgerrno.h>
#include <sqlca.h>
/* End of automatic include section */

#line 1 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_MainTapeUpdate.pgc"
/* SUMLIB_MainTapeUpdate.pgc 
 *
 * Updates the sum_main db table when a tape write completes.
 * The following fields are updated for each ds_index for reqcnt:
 * ARCHIVE_STATUS, ARCH_TAPE, ARCH_TAPE_FN, ARCH_TAPE_DATE.
 * Also delete AP/add DP sum_partn_alloc entries.
 * Also inserts the md5cksum into the sum_file table.
 * Return non-0 if any of the updates fail.
 * 
 * Called from taperespwritedo_1() in tape_svc. A call looks like:
 * tape_closed:    KEYTYP_INT      -1
 * tapeid:         KEYTYP_STRING   000014S
 * md5cksum:    KEYTYP_STRING   5ddf4230d9566ca38a7927512244c4ae
 * gtarblock:   KEYTYP_INT      256
 * availblocks: KEYTYP_UINT64   48828000
 * STATUS:      KEYTYP_INT      0
 * group_id:    KEYTYP_INT      99
 * nxtwrtfn:    KEYTYP_INT      2
 * procnum:     KEYTYP_ULONG    1
 * current_client:      KEYTYP_FILEP    6917529027641678096
 * OP:  KEYTYP_STRING   wt
 * reqcnt:      KEYTYP_INT      3
 * username_2:  KEYTYP_STRING   jim
 * ds_index_2:  KEYTYP_UINT64   460
 * safe_id_2:   KEYTYP_INT      0
 * group_id_2:  KEYTYP_INT      99
 * archsub_2:   KEYTYP_INT      128
 * status_2:    KEYTYP_INT      4
 * bytes_2:     KEYTYP_DOUBLE              1.200000e+08
 * sumid_2:     KEYTYP_UINT64   458
 * effective_date_2:    KEYTYP_STRING   2005212
 * wd_2:        KEYTYP_STRING   /SUM1/D460
 * username_1:  KEYTYP_STRING   jim
 * ds_index_1:  KEYTYP_UINT64    464
 * safe_id_1:   KEYTYP_INT      0
 * group_id_1:  KEYTYP_INT      99
 * archsub_1:   KEYTYP_INT      128
 * status_1:    KEYTYP_INT      4
 * bytes_1:     KEYTYP_DOUBLE              1.200000e+08
 * sumid_1:     KEYTYP_UINT64    460
 * effective_date_1:    KEYTYP_STRING   2005212
 * wd_1:        KEYTYP_STRING   /SUM1/D464
 * DEBUGFLG:    KEYTYP_INT      1
 * username_0:  KEYTYP_STRING   jim
 * ds_index_0:  KEYTYP_UINT64    1523
 * safe_id_0:   KEYTYP_INT      0
 * group_id_0:  KEYTYP_INT      99
 * archsub_0:   KEYTYP_INT      128
 * status_0:    KEYTYP_INT      4
 * bytes_0:     KEYTYP_DOUBLE              1.200000e+08
 * sumid_0:     KEYTYP_UINT64    840
 * effective_date_0:    KEYTYP_STRING   2005246
 * wd_0:        KEYTYP_STRING   /SUM5/D1523
 * dnum:        KEYTYP_INT      0
 * snum:        KEYTYP_INT      14
 * cmd1:  KEYTYP_STRING mtx -f /dev/sg7 load 15 0 1> /tmp/mtx_robot.log 2>&1
 */
#include <SUM.h>
#include <sum_rpc.h>
#include <soi_error.h>
#include <printk.h>

int SUMLIB_MainTapeUpdate(KEY *params); 

int SUMLIB_MainTapeUpdate(KEY *params) 
{
/* exec sql begin declare section */
   
   
   
   
   
   
     
     
   
   
   
   
   
   
   
   

#line 68 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_MainTapeUpdate.pgc"
  struct varchar_1  { int len; char arr[ 20 ]; }  l_eff_date ;
 
#line 69 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_MainTapeUpdate.pgc"
  struct varchar_2  { int len; char arr[ 80 ]; }  l_wd ;
 
#line 70 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_MainTapeUpdate.pgc"
  struct varchar_3  { int len; char arr[ 5 ]; }  arch_status ;
 
#line 71 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_MainTapeUpdate.pgc"
 int storage_group ;
 
#line 72 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_MainTapeUpdate.pgc"
 int safe_id ;
 
#line 73 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_MainTapeUpdate.pgc"
 double bytes ;
 
#line 74 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_MainTapeUpdate.pgc"
 long long ds_index ;
 
#line 75 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_MainTapeUpdate.pgc"
 long long create_sumid ;
 
#line 76 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_MainTapeUpdate.pgc"
  struct varchar_4  { int len; char arr[ 20 ]; }  arch_tape ;
 
#line 77 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_MainTapeUpdate.pgc"
  struct varchar_5  { int len; char arr[ 48 ]; }  l_md5cksum ;
 
#line 78 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_MainTapeUpdate.pgc"
 int gtarblock ;
 
#line 79 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_MainTapeUpdate.pgc"
 int arch_tape_fn ;
 
#line 80 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_MainTapeUpdate.pgc"
  struct varchar_6  { int len; char arr[ 32 ]; }  arch_tape_date ;
 
#line 81 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_MainTapeUpdate.pgc"
 int apstatus ;
 
#line 82 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_MainTapeUpdate.pgc"
 int archsub ;
 
#line 83 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_MainTapeUpdate.pgc"
 int groupid ;
/* exec sql end declare section */
#line 84 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_MainTapeUpdate.pgc"

  
int i, reqcnt, eflg, dosafe, permanent;
char *tapeid;
char nametmp[80];

  eflg = 0;
  reqcnt = getkey_int(params, "reqcnt");
  tapeid = GETKEY_str(params, "tapeid");
  arch_tape_fn = getkey_int(params, "nxtwrtfn");
  gtarblock = getkey_int(params, "gtarblock");
  sprintf(arch_status.arr, "%s", "Y");
  arch_status.len = strlen(arch_status.arr);
  sprintf(arch_tape.arr, "%s", tapeid);
  arch_tape.len = strlen(arch_tape.arr);
  sprintf(l_md5cksum.arr, "%s", GETKEY_str(params, "md5cksum"));
  l_md5cksum.len = strlen(l_md5cksum.arr);

  sprintf(arch_tape_date.arr, "%s", get_datetime());
  arch_tape_date.len = strlen(arch_tape_date.arr);

  for(i=0; i < reqcnt; i++) {
    sprintf(nametmp, "ds_index_%d", i);
    ds_index = (long long)getkey_uint64(params, nametmp);

     /* exec sql whenever sqlerror  goto  sqlerror ; */
#line 109 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_MainTapeUpdate.pgc"

     { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "update SUM_MAIN set ARCHIVE_STATUS = $1  , ARCH_TAPE = $2  , ARCH_TAPE_FN = $3  , ARCH_TAPE_DATE = $4  where DS_INDEX = $5 ", 
	ECPGt_varchar,&(arch_status),(long)5,(long)1,sizeof(struct varchar_3), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_varchar,&(arch_tape),(long)20,(long)1,sizeof(struct varchar_4), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_int,&(arch_tape_fn),(long)1,(long)1,sizeof(int), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_varchar,&(arch_tape_date),(long)32,(long)1,sizeof(struct varchar_6), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_long_long,&(ds_index),(long)1,(long)1,sizeof(long long), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EOIT, ECPGt_EORT);
#line 115 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_MainTapeUpdate.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 115 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_MainTapeUpdate.pgc"


    /* now delete AP/add DP sum_partn_alloc entries for reqcnt */
    sprintf(nametmp, "wd_%d", i);
    sprintf(l_wd.arr,"%s", GETKEY_str(params, nametmp));
    l_wd.len = strlen(l_wd.arr);
    sprintf(nametmp, "sumid_%d", i);
    create_sumid = (long long)getkey_uint64(params, nametmp);
    sprintf(nametmp, "status_%d", i);
    apstatus = getkey_int(params, nametmp);
    sprintf(nametmp, "archsub_%d", i);
    archsub = getkey_int(params, nametmp);
    sprintf(nametmp, "safe_id_%d", i);
    safe_id = getkey_int(params, nametmp);
    sprintf(nametmp, "group_id_%d", i);
    groupid = getkey_int(params, nametmp);
 //printk("In SUMLIB_MainTapeUpdate() delete from SUM_PARTN_ALLOC:\n");
 //printk("l_wd=%s, create_sumid=%lld, apstatus=%d\n", l_wd.arr,create_sumid,apstatus );
    { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "delete from SUM_PARTN_ALLOC where WD = $1  and SUMID = $2  and STATUS = $3 ", 
	ECPGt_varchar,&(l_wd),(long)80,(long)1,sizeof(struct varchar_2), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_long_long,&(create_sumid),(long)1,(long)1,sizeof(long long), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_int,&(apstatus),(long)1,(long)1,sizeof(int), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EOIT, ECPGt_EORT);
#line 136 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_MainTapeUpdate.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 136 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_MainTapeUpdate.pgc"


    sprintf(nametmp, "effective_date_%d", i);
    sprintf(l_eff_date.arr,"%s", GETKEY_str(params, nametmp));
    l_eff_date.len = strlen(l_eff_date.arr);

    dosafe = (apstatus==DASAP);
    permanent = (archsub==DAAPERM);
    apstatus = dosafe?DAAP:(permanent?DAPERM:DADP);
    storage_group = dosafe?safe_id:groupid;
    sprintf(nametmp, "bytes_%d", i);
    bytes = getkey_double(params, nametmp);

    { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "insert into SUM_PARTN_ALLOC ( WD , SUMID , STATUS , BYTES , EFFECTIVE_DATE , ARCHIVE_SUBSTATUS , GROUP_ID , SAFE_ID , DS_INDEX ) values ( $1  , $2  , $3  , $4  , $5  , $6  , $7  , $8  , $9  )", 
	ECPGt_varchar,&(l_wd),(long)80,(long)1,sizeof(struct varchar_2), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_long_long,&(create_sumid),(long)1,(long)1,sizeof(long long), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_int,&(apstatus),(long)1,(long)1,sizeof(int), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_double,&(bytes),(long)1,(long)1,sizeof(double), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_varchar,&(l_eff_date),(long)20,(long)1,sizeof(struct varchar_1), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_int,&(archsub),(long)1,(long)1,sizeof(int), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_int,&(storage_group),(long)1,(long)1,sizeof(int), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_int,&(safe_id),(long)1,(long)1,sizeof(int), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_long_long,&(ds_index),(long)1,(long)1,sizeof(long long), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EOIT, ECPGt_EORT);
#line 151 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_MainTapeUpdate.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 151 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_MainTapeUpdate.pgc"


     continue;

sqlerror:
     printk("Error in SUMLIB_MainTapeUpdate() \n");
     printk("% .70s \n", sqlca.sqlerrm.sqlerrmc);
     eflg = 1;
     /*EXEC SQL WHENEVER SQLERROR CONTINUE;*/
     /*EXEC SQL ROLLBACK WORK;*/
  }
  if(eflg) {
    { ECPGtrans(__LINE__, NULL, "commit work");
#line 163 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_MainTapeUpdate.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 163 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_MainTapeUpdate.pgc"

    return(1);
  }

  { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "insert into SUM_FILE ( TAPEID , FILENUM , GTARBLOCK , MD5CKSUM ) values ( $1  , $2  , $3  , $4  )", 
	ECPGt_varchar,&(arch_tape),(long)20,(long)1,sizeof(struct varchar_4), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_int,&(arch_tape_fn),(long)1,(long)1,sizeof(int), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_int,&(gtarblock),(long)1,(long)1,sizeof(int), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_varchar,&(l_md5cksum),(long)48,(long)1,sizeof(struct varchar_5), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EOIT, ECPGt_EORT);
#line 168 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_MainTapeUpdate.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 168 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_MainTapeUpdate.pgc"


  { ECPGtrans(__LINE__, NULL, "commit work");
#line 170 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_MainTapeUpdate.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 170 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_MainTapeUpdate.pgc"

  return(0);
}
