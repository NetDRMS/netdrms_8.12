/* Processed by ecpg (4.10.0) */
/* These include files are added by the preprocessor */
#include <ecpglib.h>
#include <ecpgerrno.h>
#include <sqlca.h>
/* End of automatic include section */

#line 1 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_StatOnline.pgc"
/* SUMLIB_StatOnline.pgc
 * This is a function to update the sum_main table
 * with the online status information as "Y" and new wd given the ds_index
 */
#include <SUM.h>
#include <sum_rpc.h>
#include <soi_error.h>
#include <printk.h>


#line 1 "/usr/include/postgresql/sqlca.h"
#ifndef POSTGRES_SQLCA_H
#define POSTGRES_SQLCA_H

#ifndef PGDLLIMPORT
#if  defined(WIN32) || defined(__CYGWIN__)
#define PGDLLIMPORT __declspec (dllimport)
#else
#define PGDLLIMPORT
#endif   /* __CYGWIN__ */
#endif   /* PGDLLIMPORT */

#define SQLERRMC_LEN	150

#ifdef __cplusplus
extern		"C"
{
#endif

struct sqlca_t
{
	char		sqlcaid[8];
	long		sqlabc;
	long		sqlcode;
	struct
	{
		int			sqlerrml;
		char		sqlerrmc[SQLERRMC_LEN];
	}			sqlerrm;
	char		sqlerrp[8];
	long		sqlerrd[6];
	/* Element 0: empty						*/
	/* 1: OID of processed tuple if applicable			*/
	/* 2: number of rows processed				*/
	/* after an INSERT, UPDATE or				*/
	/* DELETE statement					*/
	/* 3: empty						*/
	/* 4: empty						*/
	/* 5: empty						*/
	char		sqlwarn[8];
	/* Element 0: set to 'W' if at least one other is 'W'	*/
	/* 1: if 'W' at least one character string		*/
	/* value was truncated when it was			*/
	/* stored into a host variable.             */

	/*
	 * 2: if 'W' a (hopefully) non-fatal notice occurred
	 */	/* 3: empty */
	/* 4: empty						*/
	/* 5: empty						*/
	/* 6: empty						*/
	/* 7: empty						*/

	char		sqlstate[5];
};

struct sqlca_t *ECPGget_sqlca(void);

#ifndef POSTGRES_ECPG_INTERNAL
#define sqlca (*ECPGget_sqlca())
#endif

#ifdef __cplusplus
}
#endif

#endif

#line 10 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_StatOnline.pgc"


int SUM_StatOnline(uint64_t ds_index, char *newwd);

int SUM_StatOnline(uint64_t ds_index, char *newwd)
{
/* exec sql begin declare section */
   
   
   
     

#line 17 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_StatOnline.pgc"
  struct varchar_1  { int len; char arr[ 80 ]; }  l_newwd ;
 
#line 18 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_StatOnline.pgc"
  struct varchar_2  { int len; char arr[ 5 ]; }  l_status ;
 
#line 19 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_StatOnline.pgc"
  struct varchar_3  { int len; char arr[ 32 ]; }  l_date ;
 
#line 20 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_StatOnline.pgc"
 long long l_ds_index ;
/* exec sql end declare section */
#line 21 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_StatOnline.pgc"

  
    l_ds_index = (long long)ds_index;

    sprintf(l_newwd.arr,"%s", newwd);
    l_newwd.len = strlen(l_newwd.arr);
    sprintf(l_status.arr,"%s", "Y");
    l_status.len = strlen(l_status.arr);
    sprintf(l_date.arr, "%s", get_datetime());
    l_date.len = strlen(l_date.arr);

	/* exec sql whenever sqlerror  goto  sqlerror ; */
#line 32 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_StatOnline.pgc"


    /* exec sql whenever not found  goto  sqlerror ; */
#line 34 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_StatOnline.pgc"
 

	{ ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "update SUM_MAIN set ONLINE_STATUS = $1  , ONLINE_LOC = $2  , ACCESS_DATE = $3  where DS_INDEX = $4 ", 
	ECPGt_varchar,&(l_status),(long)5,(long)1,sizeof(struct varchar_2), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_varchar,&(l_newwd),(long)80,(long)1,sizeof(struct varchar_1), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_varchar,&(l_date),(long)32,(long)1,sizeof(struct varchar_3), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_long_long,&(l_ds_index),(long)1,(long)1,sizeof(long long), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EOIT, ECPGt_EORT);
#line 40 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_StatOnline.pgc"

if (sqlca.sqlcode == ECPG_NOT_FOUND) goto sqlerror;
#line 40 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_StatOnline.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 40 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_StatOnline.pgc"

	{ ECPGtrans(__LINE__, NULL, "commit work");
#line 41 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_StatOnline.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 41 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_StatOnline.pgc"

    return NO_ERROR;
	
sqlerror:
	printk("Error in SUM_StatOnline\n"); 
	printk("% .70s \n", sqlca.sqlerrm.sqlerrmc); 
    /* exec sql whenever sqlerror  continue ; */
#line 47 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_StatOnline.pgc"

    { ECPGtrans(__LINE__, NULL, "rollback work");}
#line 48 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_StatOnline.pgc"

    return DS_DATA_UPD;
}


/* This is a function to update the sum_main table
 * with the online status information as "N" given the ds_index
*/
int SUM_StatOffline(uint64_t ds_index);

int SUM_StatOffline(uint64_t ds_index)
{
/* exec sql begin declare section */
     
   

#line 61 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_StatOnline.pgc"
 long long l_ds_index ;
 
#line 62 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_StatOnline.pgc"
  struct varchar_4  { int len; char arr[ 5 ]; }  l_status ;
/* exec sql end declare section */
#line 63 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_StatOnline.pgc"

  
    l_ds_index = (long long)ds_index;
    sprintf(l_status.arr,"%s", "N");
    l_status.len = strlen(l_status.arr);

    /* exec sql whenever sqlerror  goto  sqlerror ; */
#line 69 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_StatOnline.pgc"

    /* exec sql whenever not found  goto  sqlerror ; */
#line 70 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_StatOnline.pgc"
 

	{ ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "update SUM_MAIN set ONLINE_STATUS = $1  where DS_INDEX = $2 ", 
	ECPGt_varchar,&(l_status),(long)5,(long)1,sizeof(struct varchar_4), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_long_long,&(l_ds_index),(long)1,(long)1,sizeof(long long), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EOIT, ECPGt_EORT);
#line 74 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_StatOnline.pgc"

if (sqlca.sqlcode == ECPG_NOT_FOUND) goto sqlerror;
#line 74 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_StatOnline.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 74 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_StatOnline.pgc"

	{ ECPGtrans(__LINE__, NULL, "commit work");
#line 75 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_StatOnline.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 75 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_StatOnline.pgc"

    return NO_ERROR;
	
sqlerror:
	printk("Error in SUM_StatOffline\n"); 
	printk("% .70s \n", sqlca.sqlerrm.sqlerrmc); 
    /* exec sql whenever sqlerror  continue ; */
#line 81 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_StatOnline.pgc"

    { ECPGtrans(__LINE__, NULL, "rollback work");}
#line 82 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_StatOnline.pgc"

    return DS_DATA_UPD;
}

/* Return the effective_date in the sum_partn_alloc table.
*/
int SUM_GetEffDate(uint64_t ds_index, char *effarray);

int SUM_GetEffDate(uint64_t ds_index, char *effarray)
{
/* exec sql begin declare section */
     
   
      

#line 93 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_StatOnline.pgc"
 long long l_ds_index ;
 
#line 94 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_StatOnline.pgc"
  struct varchar_5  { int len; char arr[ 20 ]; }  eff_date_str ;
 
#line 95 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_StatOnline.pgc"
 int daro = DARO ;
/* exec sql end declare section */
#line 96 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_StatOnline.pgc"


    l_ds_index = (long long)ds_index;
    /* exec sql whenever sqlerror  goto  sqlerror ; */
#line 99 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_StatOnline.pgc"

    /* exec sql whenever not found  goto  sqlerror ; */
#line 100 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_StatOnline.pgc"
 

    { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "select distinct EFFECTIVE_DATE from SUM_PARTN_ALLOC where DS_INDEX = $1  and STATUS <> $2 ", 
	ECPGt_long_long,&(l_ds_index),(long)1,(long)1,sizeof(long long), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_int,&(daro),(long)1,(long)1,sizeof(int), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EOIT, 
	ECPGt_varchar,&(eff_date_str),(long)20,(long)1,sizeof(struct varchar_5), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EORT);
#line 104 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_StatOnline.pgc"

if (sqlca.sqlcode == ECPG_NOT_FOUND) goto sqlerror;
#line 104 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_StatOnline.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 104 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_StatOnline.pgc"

    sprintf(effarray, eff_date_str.arr);
    return(0);

sqlerror:
	printk("Error in SUM_IncEffDate\n"); 
	printk("% .70s \n", sqlca.sqlerrm.sqlerrmc); 
    /* exec sql whenever sqlerror  continue ; */
#line 111 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_StatOnline.pgc"

    { ECPGtrans(__LINE__, NULL, "rollback work");}
#line 112 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_StatOnline.pgc"

    return(1);
}
