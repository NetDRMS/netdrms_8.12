/* Processed by ecpg (4.10.0) */
/* These include files are added by the preprocessor */
#include <ecpglib.h>
#include <ecpgerrno.h>
#include <sqlca.h>
/* End of automatic include section */

#line 1 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Ds_Ix_File.pgc"
/* SUMLIB_Ds_Ix_File.pgc
 * Takes a 0 terminated array of ds_index values (all the SU that are 
 * in the same file on the same tape) and determines which
 * ones are offline.
 * If none are offline, returns 0. Else returns 1 with the filedsixoff[]
 * array containing the offline ds_index values with 0 termination.
 * Return -1 on error.
 * NOTE: add 10Sep2008 Set dsmdiflg arg if Owning_Series is ds_mdi.*
 * NOTE: add 02Oct2008 Set dsmdiflg arg if Owning_Series is hmi_ground.lev0
 * NOTE: add 30Dec2008 Set dsmdiflg arg if Owning_Series is 
 *   hmi_ground.raw_egse_hmifsfm or aia_ground.raw_egse_aiafsfm
 * NOTE: add 12Jan2009 Set dsmdiflg arg if Owning_Series is 
 *   hmi_ground.test_config_files
 * NOTE: add 14Nov2012 Set dsmdiflg arg if dsds.mdi__lev1_5__fd_M_01h
 * NOTE: add 03Apr2015 Set dsmdiflg arg if sha.dsds
*/

#include <SUM.h>
#include <sum_rpc.h>
#include <soi_error.h>
#include <printk.h>
#include <string.h>

int SUMLIB_Ds_Ix_File(uint64_t filedsix[], uint64_t filedsixoff[], int *dsmdiflg);
int SUMLIB_Ds_Ix_FileX(uint64_t filedsix[], uint64_t filedsixoff[], int *dsmdiflg);
int SUMLIB_Ds_Ix_Find(char *tapeid, int filenum, uint64_t filedsix[],
			double filebytes[]);
int SUMLIB_Ds_Ix_FindX(char *tapeid, int filenum, uint64_t filedsix[],
			double filebytes[]);

int SUMLIB_Ds_Ix_File(uint64_t filedsix[],  uint64_t filedsixoff[], int *dsmdiflg)
{
/* exec sql begin declare section */
   
   
     

#line 34 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Ds_Ix_File.pgc"
  struct varchar_1  { int len; char arr[ 5 ]; }  online_status ;
 
#line 35 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Ds_Ix_File.pgc"
  struct varchar_2  { int len; char arr[ 80 ]; }  own_series ;
 
#line 36 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Ds_Ix_File.pgc"
 long long ds_index ;
/* exec sql end declare section */
#line 37 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Ds_Ix_File.pgc"

   int i, j, offline;

    /* exec sql whenever not found  goto  sqlerror ; */
#line 40 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Ds_Ix_File.pgc"
 
    /* exec sql whenever sqlerror  goto  sqlerror ; */
#line 41 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Ds_Ix_File.pgc"


    offline = 0;
    *dsmdiflg = 0;
    for(i=0, j=0; ; i++ ) {
      ds_index = (long long)(filedsix[i]);
      if(!ds_index) break;
      /*printk("SUMLIB_Ds_Ix_File ds_index = %lld\n", ds_index); */
      { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "select ONLINE_STATUS , OWNING_SERIES from SUM_MAIN where DS_INDEX = $1 ", 
	ECPGt_long_long,&(ds_index),(long)1,(long)1,sizeof(long long), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EOIT, 
	ECPGt_varchar,&(online_status),(long)5,(long)1,sizeof(struct varchar_1), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_varchar,&(own_series),(long)80,(long)1,sizeof(struct varchar_2), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EORT);
#line 52 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Ds_Ix_File.pgc"

if (sqlca.sqlcode == ECPG_NOT_FOUND) goto sqlerror;
#line 52 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Ds_Ix_File.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 52 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Ds_Ix_File.pgc"


      if(!strcmp(own_series.arr, "hmi_ground.lev0")) { 
        *dsmdiflg = 1;
      }
      else if(!strcmp(own_series.arr, "hmi_ground.raw_egse_hmifsfm")) { 
        *dsmdiflg = 1;
      }
      else if(!strcmp(own_series.arr, "aia_ground.raw_egse_aiafsfm")) { 
        *dsmdiflg = 1;
      }
      else if(!strcmp(own_series.arr, "hmi_ground.test_config_files")) { 
        *dsmdiflg = 1;
      }
      else if(!strcmp(own_series.arr, "hmi_ground.hk_dayfile")) { 
        *dsmdiflg = 1;
      }
      else if(!strcmp(own_series.arr, "sha.SNGB_norot_511mn")) { 
        *dsmdiflg = 1;
      }
      else if(!strcmp(own_series.arr, "sha.SNGB_rot30N")) { 
        *dsmdiflg = 1;
      }
      else if(!strcmp(own_series.arr, "dsds.mdi__lev1_5__fd_M_01h")) { 
        *dsmdiflg = 1;
      }
      else if(!strcmp(own_series.arr, "dsds.mdi__lev1_5__fd_M_96m_01d")) { 
        *dsmdiflg = 1;
      }
      else if(!strcmp(own_series.arr, "sha.dsds")) { 
        *dsmdiflg = 1;
      }
      else {
        if(strstr(own_series.arr, "ds_mdi.")) { *dsmdiflg = 1; }
      }
 
      if(strcmp(online_status.arr, "N")==0) {
        offline = 1;
	/*printk("SUMLIB_Ds_Ix_File offline ds_index = %lld\n", ds_index); */
        filedsixoff[j++] = (unsigned long long)ds_index;
      }
    }
    filedsixoff[j] = 0;
    	{ ECPGtrans(__LINE__, NULL, "commit work");
#line 95 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Ds_Ix_File.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 95 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Ds_Ix_File.pgc"

    if(!offline) return(0);
    return(1);
sqlerror:
         printk("Error in SUMLIB_Ds_Ix_File for ds_index=%lld\n", ds_index);
         printk("% .70s \n", sqlca.sqlerrm.sqlerrmc);
    	 { ECPGtrans(__LINE__, NULL, "commit work");
#line 101 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Ds_Ix_File.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 101 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Ds_Ix_File.pgc"

         return(-1);
}

/* Find all the ds_index values that are in sum_main for the given tapeid
 * and file number. Return the values in the given array. Also returns
 * a bytes array.
*/
int SUMLIB_Ds_Ix_Find(char *tapeid, int filenum, uint64_t filedsix[], 
			double filebytes[])
{
/* exec sql begin declare section */
   
     
   
   

#line 113 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Ds_Ix_File.pgc"
  struct varchar_3  { int len; char arr[ 20 ]; }  l_tapeid ;
 
#line 114 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Ds_Ix_File.pgc"
 long long ds_index ;
 
#line 115 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Ds_Ix_File.pgc"
 double l_bytes ;
 
#line 116 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Ds_Ix_File.pgc"
 int l_filenum ;
/* exec sql end declare section */
#line 117 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Ds_Ix_File.pgc"

   int i;

    /* exec sql whenever sqlerror  goto  sqlerror ; */
#line 120 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Ds_Ix_File.pgc"


    sprintf(l_tapeid.arr, "%s", tapeid);
    l_tapeid.len = strlen(l_tapeid.arr);
    l_filenum = filenum;

        ECPGset_var( 0, &( l_filenum ), __LINE__);\
 ECPGset_var( 1, &( l_tapeid ), __LINE__);\
 /* declare pfind cursor for select DS_INDEX , BYTES from SUM_MAIN where ARCH_TAPE = $1  and ARCH_TAPE_FN = $2  */
#line 129 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Ds_Ix_File.pgc"
 

        { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "declare pfind cursor for select DS_INDEX , BYTES from SUM_MAIN where ARCH_TAPE = $1  and ARCH_TAPE_FN = $2 ", 
	ECPGt_varchar,&(l_tapeid),(long)20,(long)1,sizeof(struct varchar_3), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_int,&(l_filenum),(long)1,(long)1,sizeof(int), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EOIT, ECPGt_EORT);
#line 131 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Ds_Ix_File.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 131 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Ds_Ix_File.pgc"

    /* exec sql whenever not found  goto  end_fetch ; */
#line 132 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Ds_Ix_File.pgc"


    for(i=0 ; ; i++) {
      { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "fetch pfind", ECPGt_EOIT, 
	ECPGt_long_long,&(ds_index),(long)1,(long)1,sizeof(long long), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_double,&(l_bytes),(long)1,(long)1,sizeof(double), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EORT);
#line 135 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Ds_Ix_File.pgc"

if (sqlca.sqlcode == ECPG_NOT_FOUND) goto end_fetch;
#line 135 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Ds_Ix_File.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 135 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Ds_Ix_File.pgc"

      filedsix[i] = (unsigned long long)ds_index;
      filebytes[i] = l_bytes;
    }
end_fetch:
    { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "close pfind", ECPGt_EOIT, ECPGt_EORT);
#line 140 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Ds_Ix_File.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 140 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Ds_Ix_File.pgc"

    filedsix[i] = 0;
    filebytes[i] = 0;
    { ECPGtrans(__LINE__, NULL, "commit");
#line 143 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Ds_Ix_File.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 143 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Ds_Ix_File.pgc"

    return(0);

sqlerror:
         printk("Error in SUMLIB_Ds_Ix_Find\n");
         printk("% .70s \n", sqlca.sqlerrm.sqlerrmc);
         { ECPGtrans(__LINE__, NULL, "rollback work");
#line 149 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Ds_Ix_File.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 149 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Ds_Ix_File.pgc"

         return(1);
}

int SUMLIB_Ds_Ix_FileX(uint64_t filedsix[],  uint64_t filedsixoff[], int *dsmdiflg)
{
/* exec sql begin declare section */
   
   
     

#line 156 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Ds_Ix_File.pgc"
  struct varchar_4  { int len; char arr[ 5 ]; }  online_status ;
 
#line 157 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Ds_Ix_File.pgc"
  struct varchar_5  { int len; char arr[ 80 ]; }  own_series ;
 
#line 158 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Ds_Ix_File.pgc"
 long long ds_index ;
/* exec sql end declare section */
#line 159 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Ds_Ix_File.pgc"

   int i, j, offline;

    /* exec sql whenever not found  goto  sqlerror ; */
#line 162 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Ds_Ix_File.pgc"
 
    /* exec sql whenever sqlerror  goto  sqlerror ; */
#line 163 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Ds_Ix_File.pgc"


    offline = 0;
    *dsmdiflg = 0;
    for(i=0, j=0; ; i++ ) {
      ds_index = (long long)(filedsix[i]);
      if(!ds_index) break;
      /*printk("SUMLIB_Ds_Ix_FileX ds_index = %lld\n", ds_index); */
      { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "select ONLINE_STATUS , OWNING_SERIES from SUM_MAIN where DS_INDEX = $1 ", 
	ECPGt_long_long,&(ds_index),(long)1,(long)1,sizeof(long long), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EOIT, 
	ECPGt_varchar,&(online_status),(long)5,(long)1,sizeof(struct varchar_4), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_varchar,&(own_series),(long)80,(long)1,sizeof(struct varchar_5), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EORT);
#line 174 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Ds_Ix_File.pgc"

if (sqlca.sqlcode == ECPG_NOT_FOUND) goto sqlerror;
#line 174 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Ds_Ix_File.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 174 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Ds_Ix_File.pgc"


      if(!strcmp(own_series.arr, "hmi_ground.lev0")) { 
        *dsmdiflg = 1;
      }
      else if(!strcmp(own_series.arr, "hmi_ground.raw_egse_hmifsfm")) { 
        *dsmdiflg = 1;
      }
      else if(!strcmp(own_series.arr, "aia_ground.raw_egse_aiafsfm")) { 
        *dsmdiflg = 1;
      }
      else if(!strcmp(own_series.arr, "hmi_ground.test_config_files")) { 
        *dsmdiflg = 1;
      }
      else if(!strcmp(own_series.arr, "hmi_ground.hk_dayfile")) { 
        *dsmdiflg = 1;
      }
      else if(!strcmp(own_series.arr, "sha.SNGB_norot_511mn")) { 
        *dsmdiflg = 1;
      }
      else if(!strcmp(own_series.arr, "sha.SNGB_rot30N")) { 
        *dsmdiflg = 1;
      }
      else if(!strcmp(own_series.arr, "dsds.mdi__lev1_5__fd_M_01h")) { 
        *dsmdiflg = 1;
      }
      else if(!strcmp(own_series.arr, "dsds.mdi__lev1_5__fd_M_96m_01d")) { 
        *dsmdiflg = 1;
      }
      else {
        if(strstr(own_series.arr, "ds_mdi.")) { *dsmdiflg = 1; }
      }
 
      if(strcmp(online_status.arr, "N")==0) {
        offline = 1;
	/*printk("SUMLIB_Ds_Ix_FileX offline ds_index = %lld\n", ds_index); */
        filedsixoff[j++] = (unsigned long long)ds_index;
      }
    }
    filedsixoff[j] = 0;
    	{ ECPGtrans(__LINE__, NULL, "commit work");
#line 214 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Ds_Ix_File.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 214 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Ds_Ix_File.pgc"

    if(!offline) return(0);
    return(1);
sqlerror:
         printk("Error in SUMLIB_Ds_Ix_FileX for ds_index=%lld\n", ds_index);
         printk("% .70s \n", sqlca.sqlerrm.sqlerrmc);
    	 { ECPGtrans(__LINE__, NULL, "commit work");
#line 220 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Ds_Ix_File.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 220 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Ds_Ix_File.pgc"

         return(-1);
}

/* Find all the ds_index values that are in sum_main for the given tapeid
 * and file number. Return the values in the given array. Also returns
 * a bytes array.
*/
/* Mod to not do the query. It is over 30 sec long (at 160M sum_main rows).
 * Try the tape reads w/o doing this query.
 * The filedsix[] has the one ds_index, filled in by the caller,
 * that kicked off this read.
*/
int SUMLIB_Ds_Ix_FindX(char *tapeid, int filenum, uint64_t filedsix[], 
			double filebytes[])
{
/* exec sql begin declare section */
   
     
   
   

#line 237 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Ds_Ix_File.pgc"
  struct varchar_6  { int len; char arr[ 20 ]; }  l_tapeid ;
 
#line 238 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Ds_Ix_File.pgc"
 long long ds_index ;
 
#line 239 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Ds_Ix_File.pgc"
 double l_bytes ;
 
#line 240 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Ds_Ix_File.pgc"
 int l_filenum ;
/* exec sql end declare section */
#line 241 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Ds_Ix_File.pgc"

   int i;

return(0);  //Don't do any long, egregious query

    /* exec sql whenever sqlerror  goto  sqlerror ; */
#line 246 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Ds_Ix_File.pgc"


    sprintf(l_tapeid.arr, "%s", tapeid);
    l_tapeid.len = strlen(l_tapeid.arr);
    l_filenum = filenum;

        ECPGset_var( 2, &( l_filenum ), __LINE__);\
 ECPGset_var( 3, &( l_tapeid ), __LINE__);\
 /* declare pfindx cursor for select DS_INDEX , BYTES from SUM_MAIN where ARCH_TAPE = $1  and ARCH_TAPE_FN = $2  */
#line 255 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Ds_Ix_File.pgc"
 

        { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "declare pfindx cursor for select DS_INDEX , BYTES from SUM_MAIN where ARCH_TAPE = $1  and ARCH_TAPE_FN = $2 ", 
	ECPGt_varchar,&(l_tapeid),(long)20,(long)1,sizeof(struct varchar_6), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_int,&(l_filenum),(long)1,(long)1,sizeof(int), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EOIT, ECPGt_EORT);
#line 257 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Ds_Ix_File.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 257 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Ds_Ix_File.pgc"

    /* exec sql whenever not found  goto  end_fetch ; */
#line 258 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Ds_Ix_File.pgc"


    for(i=0 ; ; i++) {
      { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "fetch pfindx", ECPGt_EOIT, 
	ECPGt_long_long,&(ds_index),(long)1,(long)1,sizeof(long long), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_double,&(l_bytes),(long)1,(long)1,sizeof(double), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EORT);
#line 261 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Ds_Ix_File.pgc"

if (sqlca.sqlcode == ECPG_NOT_FOUND) goto end_fetch;
#line 261 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Ds_Ix_File.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 261 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Ds_Ix_File.pgc"

      filedsix[i] = (unsigned long long)ds_index;
      filebytes[i] = l_bytes;
    }
end_fetch:
    { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "close pfindx", ECPGt_EOIT, ECPGt_EORT);
#line 266 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Ds_Ix_File.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 266 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Ds_Ix_File.pgc"

    filedsix[i] = 0;
    filebytes[i] = 0;
    { ECPGtrans(__LINE__, NULL, "commit");
#line 269 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Ds_Ix_File.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 269 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Ds_Ix_File.pgc"

    return(0);

sqlerror:
         printk("Error in SUMLIB_Ds_Ix_FindX\n");
         printk("% .70s \n", sqlca.sqlerrm.sqlerrmc);
         { ECPGtrans(__LINE__, NULL, "rollback work");
#line 275 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Ds_Ix_File.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 275 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Ds_Ix_File.pgc"

         return(1);
}
