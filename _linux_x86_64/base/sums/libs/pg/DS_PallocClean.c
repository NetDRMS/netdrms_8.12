/* Processed by ecpg (4.10.0) */
/* These include files are added by the preprocessor */
#include <ecpglib.h>
#include <ecpgerrno.h>
#include <sqlca.h>
/* End of automatic include section */

#line 1 "/usr/local/netdrms_8.12/base/sums/libs/pg/DS_PallocClean.pgc"
/* Called by SUM_Init() at sum_svc startup to clean up left over 
 * DARW and DARO entries in the sum_partn_alloc table.
 * Also clears out the sum_open table. (new 21Oct2009)
*/
#include <SUM.h>
#include <sum_rpc.h>
#include <soi_error.h>
#include <printk.h>

int DS_PallocClean();

int DS_PallocClean()
{

/* exec sql begin declare section */
   

#line 16 "/usr/local/netdrms_8.12/base/sums/libs/pg/DS_PallocClean.pgc"
 int l_status ;
/* exec sql end declare section */
#line 17 "/usr/local/netdrms_8.12/base/sums/libs/pg/DS_PallocClean.pgc"


    printk("DS_PallocClean\n");  
    /* exec sql whenever sqlerror  goto  sqlerror ; */
#line 20 "/usr/local/netdrms_8.12/base/sums/libs/pg/DS_PallocClean.pgc"

         l_status = DARW;
         { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "delete from SUM_PARTN_ALLOC where STATUS = $1 ", 
	ECPGt_int,&(l_status),(long)1,(long)1,sizeof(int), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EOIT, ECPGt_EORT);
#line 23 "/usr/local/netdrms_8.12/base/sums/libs/pg/DS_PallocClean.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 23 "/usr/local/netdrms_8.12/base/sums/libs/pg/DS_PallocClean.pgc"

         l_status = DARO;
         { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "delete from SUM_PARTN_ALLOC where STATUS = $1 ", 
	ECPGt_int,&(l_status),(long)1,(long)1,sizeof(int), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EOIT, ECPGt_EORT);
#line 26 "/usr/local/netdrms_8.12/base/sums/libs/pg/DS_PallocClean.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 26 "/usr/local/netdrms_8.12/base/sums/libs/pg/DS_PallocClean.pgc"

         { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "delete from SUM_OPEN", ECPGt_EOIT, ECPGt_EORT);
#line 27 "/usr/local/netdrms_8.12/base/sums/libs/pg/DS_PallocClean.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 27 "/usr/local/netdrms_8.12/base/sums/libs/pg/DS_PallocClean.pgc"

         { ECPGtrans(__LINE__, NULL, "commit work");
#line 28 "/usr/local/netdrms_8.12/base/sums/libs/pg/DS_PallocClean.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 28 "/usr/local/netdrms_8.12/base/sums/libs/pg/DS_PallocClean.pgc"

              return NO_ERROR;

sqlerror:
    printk("Error in DS_PallocClean\n");
    printk("% .70s \n", sqlca.sqlerrm.sqlerrmc);
    return DS_DATA_QRY;
}
