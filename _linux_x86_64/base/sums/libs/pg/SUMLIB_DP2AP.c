/* Processed by ecpg (4.10.0) */
/* These include files are added by the preprocessor */
#include <ecpglib.h>
#include <ecpgerrno.h>
#include <sqlca.h>
/* End of automatic include section */

#line 1 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DP2AP.pgc"
/* SUMLIB_DP2AP.pgc
*/
#include <SUM.h>
#include <sum_rpc.h>
#include <soi_error.h>
#include <printk.h>

/* Called to potientially change an entry from Del Pend to Archive Pend
 * in the SUM_PARTN_ALLOC db table.
 * This is used by ingest_lev0 when a retransmitted .tlm file needs to 
 * add data to a currently existing image. The storage unit (ds_index) 
 * may have already been archived and is now del pend, so it needs to be
 * marked archive pending again so that the new image will be saved.
*/
/* !!!NOTE: this was never used by ingest_lev0. It ended up cloning the
 * segment too.
*/

int SUMLIB_DP2AP(uint64_t ds_index);

int SUMLIB_DP2AP(uint64_t ds_index)
{
/* exec sql begin declare section */
   	  
     

#line 24 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DP2AP.pgc"
 int l_status ;
 
#line 25 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DP2AP.pgc"
 long long l_ds_index ;
/* exec sql end declare section */
#line 26 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DP2AP.pgc"


   l_ds_index = (long long)ds_index;

    /* exec sql whenever sqlerror  goto  sqlerror ; */
#line 30 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DP2AP.pgc"

    /* exec sql whenever not found  goto  notfound ; */
#line 31 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DP2AP.pgc"


    /* if find a del pend entry change it to arch pend */
    { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "select STATUS from SUM_PARTN_ALLOC where DS_INDEX = $1  and STATUS = 2", 
	ECPGt_long_long,&(l_ds_index),(long)1,(long)1,sizeof(long long), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EOIT, 
	ECPGt_int,&(l_status),(long)1,(long)1,sizeof(int), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EORT);
#line 35 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DP2AP.pgc"

if (sqlca.sqlcode == ECPG_NOT_FOUND) goto notfound;
#line 35 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DP2AP.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 35 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DP2AP.pgc"


    { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "update SUM_PARTN_ALLOC set STATUS = 4 where DS_INDEX = $1  and STATUS = 2", 
	ECPGt_long_long,&(l_ds_index),(long)1,(long)1,sizeof(long long), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EOIT, ECPGt_EORT);
#line 38 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DP2AP.pgc"

if (sqlca.sqlcode == ECPG_NOT_FOUND) goto notfound;
#line 38 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DP2AP.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 38 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DP2AP.pgc"


notfound:
    { ECPGtrans(__LINE__, NULL, "commit work");
#line 41 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DP2AP.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 41 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DP2AP.pgc"

    return NO_ERROR;
        
sqlerror:
    printk("Error in SUMLIB_DP2AP()\n"); 
    printk("% .70s \n", sqlca.sqlerrm.sqlerrmc); 
    /* exec sql whenever sqlerror  continue ; */
#line 47 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DP2AP.pgc"

    { ECPGtrans(__LINE__, NULL, "rollback work");}
#line 48 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DP2AP.pgc"

    return DS_PALC_ERR;
}

