/* Processed by ecpg (4.10.0) */
/* These include files are added by the preprocessor */
#include <ecpglib.h>
#include <ecpgerrno.h>
#include <sqlca.h>
/* End of automatic include section */

#line 1 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DS_DataRequest_WD.pgc"
/* SUMLIB_DS_DataRequest_WD.pc
*/
/* Returns in the results keylist the wd only of the storage units for the 
 * given ds_index values.
 * The keywords in the input KEY * are like so:
 *   uid    = unique id from a SUM_open() call for this session
 *   reqcnt = number of ds_index values being requested
 *   dsix_0 = first ds_index value 
 *   dsix_1 = second ds_index value 
 *   [etc.]
 * Returns 0 on success, else error code.
 * NOTE: this is usually called with the params and results keylist the same.
*/
#include <SUM.h>
#include <sum_rpc.h>
#include <soi_error.h>
#include <printk.h>

int DS_DataRequest_WD(KEY *params, KEY **results);

int DS_DataRequest_WD(KEY *params, KEY **results)
{
/* exec sql begin declare section */
   
     

#line 24 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DS_DataRequest_WD.pgc"
  struct varchar_1  { int len; char arr[ 80 ]; }  online_loc ;
 
#line 25 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DS_DataRequest_WD.pgc"
 long long ds_index ;
/* exec sql end declare section */
#line 26 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DS_DataRequest_WD.pgc"


   char dsix_name[128];
   char loc[80];
   int i, reqcnt;

   /* exec sql whenever not found  goto  sqlnotfound ; */
#line 32 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DS_DataRequest_WD.pgc"
 
   /* exec sql whenever sqlerror  goto  sqlerror ; */
#line 33 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DS_DataRequest_WD.pgc"


    reqcnt = getkey_int(params, "reqcnt");
    for(i=0; i < reqcnt ; i++) {
      sprintf(dsix_name, "dsix_%d", i);
      if(!findkey(params, dsix_name)) {
        printk("Bad keylist given to DS_DataRequest_WD()\n");
        return(DS_DATA_REQ);
        break;
      }
      ds_index = (long long)getkey_uint64(params, dsix_name);
      { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "select ONLINE_LOC from SUM_MAIN where DS_INDEX = $1 ", 
	ECPGt_long_long,&(ds_index),(long)1,(long)1,sizeof(long long), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EOIT, 
	ECPGt_varchar,&(online_loc),(long)80,(long)1,sizeof(struct varchar_1), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EORT);
#line 47 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DS_DataRequest_WD.pgc"

if (sqlca.sqlcode == ECPG_NOT_FOUND) goto sqlnotfound;
#line 47 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DS_DataRequest_WD.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 47 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DS_DataRequest_WD.pgc"


        strcpy(loc, online_loc.arr);
        sprintf(dsix_name, "wd_%d", i);
    	setkey_str(results, dsix_name, loc);
        continue;
sqlnotfound:
        printk("DS_DataRequest_WD() Data Not Found for ds_index=%lld\n",
			 ds_index);
        sprintf(dsix_name, "wd_%d", i);
    	setkey_str(results, dsix_name, "");
    }
        { ECPGtrans(__LINE__, NULL, "commit work");
#line 59 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DS_DataRequest_WD.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 59 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DS_DataRequest_WD.pgc"

	return NO_ERROR;
sqlerror:
         printk("Error in DS_DataRequest_WD\n");
         printk("% .70s \n", sqlca.sqlerrm.sqlerrmc);
         /* exec sql whenever sqlerror  continue ; */
#line 64 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DS_DataRequest_WD.pgc"

         { ECPGtrans(__LINE__, NULL, "rollback work");}
#line 65 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DS_DataRequest_WD.pgc"

         return DS_DATA_QRY;
}

