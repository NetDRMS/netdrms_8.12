/* Processed by ecpg (4.10.0) */
/* These include files are added by the preprocessor */
#include <ecpglib.h>
#include <ecpgerrno.h>
#include <sqlca.h>
/* End of automatic include section */

#line 1 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_EffDateUpdate.pgc"
/* SUMLIB_EffDateUpdate.pc
*/
#include <SUM.h>
#include <sum_rpc.h>
#include <soi_error.h>
#include <printk.h>

/* Called to update the effective_date in the sum_group table for the group_id
 * for the given tapeid.
 * If operation = 0 then a tape read has occured and the effective_date is set
 * to the later of today or what it is already set to.
 * If operation n.e. 0 then a tape write has occured and the effective_date is
 * set to today + retain_days from the sum_group table for this group.
 * Return 1 on error, else 0.
*/
int SUMLIB_EffDateUpdate
  (char *tapeid, int operation);

int SUMLIB_EffDateUpdate
  (char *tapeid, int operation)
{
/* exec sql begin declare section */
   
   
        
        

#line 23 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_EffDateUpdate.pgc"
  struct varchar_1  { int len; char arr[ 20 ]; }  l_tapeid ;
 
#line 24 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_EffDateUpdate.pgc"
  struct varchar_2  { int len; char arr[ 20 ]; }  l_eff_date ;
 
#line 25 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_EffDateUpdate.pgc"
 int l_gpid ;
 
#line 26 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_EffDateUpdate.pgc"
 int l_retaindays ;
/* exec sql end declare section */
#line 27 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_EffDateUpdate.pgc"

  int cmp, update;
  char *neweffdate;

    /* exec sql whenever sqlerror  goto  sqlerror ; */
#line 31 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_EffDateUpdate.pgc"

    /* exec sql whenever not found  goto  sqlerror ; */
#line 32 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_EffDateUpdate.pgc"


    sprintf(l_tapeid.arr,"%s", tapeid);
    l_tapeid.len = strlen(l_tapeid.arr);

  { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "select group_id from SUM_TAPE where tapeid = $1 ", 
	ECPGt_varchar,&(l_tapeid),(long)20,(long)1,sizeof(struct varchar_1), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EOIT, 
	ECPGt_int,&(l_gpid),(long)1,(long)1,sizeof(int), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EORT);
#line 37 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_EffDateUpdate.pgc"

if (sqlca.sqlcode == ECPG_NOT_FOUND) goto sqlerror;
#line 37 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_EffDateUpdate.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 37 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_EffDateUpdate.pgc"


  { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "select retain_days , effective_date from SUM_GROUP where group_id = $1 ", 
	ECPGt_int,&(l_gpid),(long)1,(long)1,sizeof(int), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EOIT, 
	ECPGt_int,&(l_retaindays),(long)1,(long)1,sizeof(int), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_varchar,&(l_eff_date),(long)20,(long)1,sizeof(struct varchar_2), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EORT);
#line 39 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_EffDateUpdate.pgc"

if (sqlca.sqlcode == ECPG_NOT_FOUND) goto sqlerror;
#line 39 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_EffDateUpdate.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 39 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_EffDateUpdate.pgc"

  /* printk("retain=%d eff_date=%s\n", l_retaindays, l_eff_date.arr); */
  update = 0;
  if(operation) {		/* this is for a write to tape */
    neweffdate = (char *)get_effdate(l_retaindays);
    update = 1;
  }
  else {
    neweffdate = (char *)get_effdate(0);
    cmp = strcmp(neweffdate, l_eff_date.arr);
    if(cmp > 0) {  /* neweffdate > current eff date so update table */
      update = 1;
    }
  }
  if(update) {
    sprintf(l_eff_date.arr, "%s", neweffdate);
    l_eff_date.len = strlen(l_eff_date.arr);
    { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "update SUM_GROUP set effective_date = $1  where group_id = $2 ", 
	ECPGt_varchar,&(l_eff_date),(long)20,(long)1,sizeof(struct varchar_2), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_int,&(l_gpid),(long)1,(long)1,sizeof(int), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EOIT, ECPGt_EORT);
#line 56 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_EffDateUpdate.pgc"

if (sqlca.sqlcode == ECPG_NOT_FOUND) goto sqlerror;
#line 56 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_EffDateUpdate.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 56 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_EffDateUpdate.pgc"

  }
  free(neweffdate);
  { ECPGtrans(__LINE__, NULL, "commit work");
#line 59 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_EffDateUpdate.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 59 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_EffDateUpdate.pgc"

  return(0);
        
sqlerror:
	  printk("Error in SUMLIB_EffDateUpdate()\n"); 
	  printk("% .70s \n", sqlca.sqlerrm.sqlerrmc); 
    /* exec sql whenever sqlerror  continue ; */
#line 65 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_EffDateUpdate.pgc"

    { ECPGtrans(__LINE__, NULL, "rollback work");}
#line 66 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_EffDateUpdate.pgc"

    return(1);
}

