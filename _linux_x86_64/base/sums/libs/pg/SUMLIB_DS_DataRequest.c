/* Processed by ecpg (4.10.0) */
/* These include files are added by the preprocessor */
#include <ecpglib.h>
#include <ecpgerrno.h>
#include <sqlca.h>
/* End of automatic include section */

#line 1 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DS_DataRequest.pgc"
/* SUMLIB_DS_DataRequest.pgc
 * Returns in the results keylist info on the storage units for the 
 * given ds_index values. Also does touch update and DARO status for
 * the given storage units.
 * The keywords in the input KEY * are like so:
 *   uid    = unique id from a SUM_open() call for this session
 *   reqcnt = number of ds_index values being requested
 *   dsix_0 = first ds_index value 
 *   dsix_1 = second ds_index value 
 *   [etc.]
 *   mode:   KEYTYP_INT      8
 *   tdays:  KEYTYP_INT      5
 * Returns 0 on success, else error code.
 * Does the query of all the ds_index values in a single SQL call.
 * This is faster than doing a call for each ds_index.
 */
#include <SUM.h>
#include <sum_rpc.h>
#include <soi_error.h>
#include <printk.h>


static int n_off, n_total;	/* counts since dsds_svc started */
static int c_off, c_total;	/* counts for last 100 ds */

int DS_DataRequest(KEY *params, KEY **results);

int DS_DataRequest(KEY *params, KEY **results)
{
/* exec sql begin declare section */
   
   
   
   
     
     
     
     
    
      
   
     
     
     
     
     
       
       
      
      
     
     
   

   
     
     
     
     
     
     
     
      
      
     
     
   


#line 31 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DS_DataRequest.pgc"
 char stmt [ 8192 ] ;
 
#line 32 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DS_DataRequest.pgc"
  struct varchar_1  { int len; char arr[ 20 ]; }  l_eff_date ;
 
#line 33 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DS_DataRequest.pgc"
  struct varchar_2  { int len; char arr[ 20 ]; }  eff_date_str ;
 
#line 34 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DS_DataRequest.pgc"
 char * eff_date ;
 
#line 35 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DS_DataRequest.pgc"
 long long uid ;
 
#line 36 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DS_DataRequest.pgc"
 long long ds_index ;
 
#line 37 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DS_DataRequest.pgc"
 long long ds_index_arr [ MAXSUMREQCNT ] ;
 
#line 38 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DS_DataRequest.pgc"
 long long ds_index_arr_pos [ MAXSUMREQCNT ] ;
 
#line 39 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DS_DataRequest.pgc"
 int touch ;
 
#line 40 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DS_DataRequest.pgc"
 int daro = DARO ;
 
#line 53 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DS_DataRequest.pgc"
 struct { 
#line 42 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DS_DataRequest.pgc"
  struct varchar_3  { int len; char arr[ 80 ]; }  online_loc ;
 
#line 43 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DS_DataRequest.pgc"
  struct varchar_4  { int len; char arr[ 5 ]; }  online_status ;
 
#line 44 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DS_DataRequest.pgc"
  struct varchar_5  { int len; char arr[ 5 ]; }  archive_status ;
 
#line 45 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DS_DataRequest.pgc"
  struct varchar_6  { int len; char arr[ 5 ]; }  offsite_ack ;
 
#line 46 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DS_DataRequest.pgc"
 double bytes ;
 
#line 47 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DS_DataRequest.pgc"
 long long ds_index ;
 
#line 48 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DS_DataRequest.pgc"
 long long create_sumid ;
 
#line 49 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DS_DataRequest.pgc"
  struct varchar_7  { int len; char arr[ 20 ]; }  tape_id ;
 
#line 50 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DS_DataRequest.pgc"
 int tape_file_num ;
 
#line 51 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DS_DataRequest.pgc"
  struct varchar_8  { int len; char arr[ 15 ]; }  tapepos ;
 
#line 52 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DS_DataRequest.pgc"
  struct varchar_9  { int len; char arr[ 32 ]; }  l_date ;
 } mainquery ;
 
#line 67 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DS_DataRequest.pgc"
 struct { 
#line 56 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DS_DataRequest.pgc"
 short online_loc_ind ;
 
#line 57 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DS_DataRequest.pgc"
 short online_status_ind ;
 
#line 58 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DS_DataRequest.pgc"
 short archive_status_ind ;
 
#line 59 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DS_DataRequest.pgc"
 short offsite_ack_ind ;
 
#line 60 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DS_DataRequest.pgc"
 short bytes_ind ;
 
#line 61 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DS_DataRequest.pgc"
 short ds_index_ind ;
 
#line 62 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DS_DataRequest.pgc"
 short create_sumid_ind ;
 
#line 63 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DS_DataRequest.pgc"
 short tape_id_ind ;
 
#line 64 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DS_DataRequest.pgc"
 short tape_file_num_ind ;
 
#line 65 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DS_DataRequest.pgc"
 short tapepos_ind ;
 
#line 66 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DS_DataRequest.pgc"
 short l_date_ind ;
 } mainind ;
/* exec sql end declare section */
#line 69 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DS_DataRequest.pgc"

char STMT[] = "SELECT ONLINE_LOC, ONLINE_STATUS, ARCHIVE_STATUS, OFFSITE_ACK, BYTES, DS_INDEX, CREATE_SUMID, ARCH_TAPE,ARCH_TAPE_FN,ARCH_TAPE_POS,CREAT_DATE FROM SUM_MAIN WHERE DS_INDEX in (";
   char loc[80];
   char l_tapepos[15];
   char l_tape_id[20];
   char dsix_name[128];
   int i, reqcnt, mode, tdays, posdays, offline, pos, change;

   uid = (long long)getkey_uint64(params, "uid");
   mode = getkey_int(params, "mode");
   if(findkey(params, "tdays")) 
     tdays = getkey_int(params, "tdays");
   else 
     tdays = 2;
  /* The mode bit "TOUCH" indicates that if this dataset is 
   * online, it is to have its effective_date for deletion set to today
   * plus "tdays" days. If it is not online, then "TOUCH" is handled 
   * in driven_svc.c if the ds is retrieved from tape.
   * NEW 15Sep2008: negative tdays means set effective_date to the greater
   * of the current effective_date or now + ABS(tdays).
   * Note, a tdays of 0 will set the effective_date to now.
  */
  if(mode & TOUCH) touch = 1;
  else touch = 0;

    reqcnt = getkey_int(params, "reqcnt");
    offline = 0;
    sprintf(stmt, "%s", STMT);
  	 
    /* Make the query statement. Remember that the wd returned may not be
     * in the same order as requested here.
     */
     
    /* sum_main query cursor. This cursor will loop over all SUs in the request.
     *
     */
    for(i=0; i < reqcnt ; i++) 
    {
      sprintf(dsix_name, "dsix_%d", i);
      if(!findkey(params, dsix_name)) 
      {
        printk("Bad keylist given to DS_DataRequest()\n");
        return(DS_DATA_REQ);
      }
      ds_index = (long long)getkey_uint64(params, dsix_name);
      ds_index_arr[i] = ds_index;         /* save in the proper position */
      ds_index_arr_pos[i] = 0;		  //keep track of any dups
      sprintf(dsix_name, "wd_%d", i);
      setkey_str(results, dsix_name, ""); /* preset blank in case doen't exist*/

      if (i == (reqcnt - 1))
      {
	    sprintf(stmt, "%s%lld);", stmt, ds_index);
	  }
      else
      {
	    sprintf(stmt, "%s%lld,", stmt, ds_index);
      }
    }

     /* exec sql whenever sqlerror  goto  sqlerror ; */
#line 129 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DS_DataRequest.pgc"

     { ECPGprepare(__LINE__, NULL, 0, "query", stmt);
#line 130 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DS_DataRequest.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 130 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DS_DataRequest.pgc"

     /* declare reqcursor cursor for $1 */
#line 131 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DS_DataRequest.pgc"

     { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "declare reqcursor cursor for $1", 
	ECPGt_char_variable,(ECPGprepared_statement(NULL, "query", __LINE__)),(long)1,(long)1,(1)*sizeof(char), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EOIT, ECPGt_EORT);
#line 132 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DS_DataRequest.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 132 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DS_DataRequest.pgc"

  	 
     /* exec sql whenever not found  goto  end_fetch ; */
#line 134 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DS_DataRequest.pgc"


     /* For each SU in the request... */
     for(i=0; ; i++) 
     {
       { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "fetch reqcursor", ECPGt_EOIT, 
	ECPGt_varchar,&(mainquery.online_loc),(long)80,(long)1,sizeof(struct varchar_3), 
	ECPGt_short,&(mainind.online_loc_ind),(long)1,(long)1,sizeof(short), 
	ECPGt_varchar,&(mainquery.online_status),(long)5,(long)1,sizeof(struct varchar_4), 
	ECPGt_short,&(mainind.online_status_ind),(long)1,(long)1,sizeof(short), 
	ECPGt_varchar,&(mainquery.archive_status),(long)5,(long)1,sizeof(struct varchar_5), 
	ECPGt_short,&(mainind.archive_status_ind),(long)1,(long)1,sizeof(short), 
	ECPGt_varchar,&(mainquery.offsite_ack),(long)5,(long)1,sizeof(struct varchar_6), 
	ECPGt_short,&(mainind.offsite_ack_ind),(long)1,(long)1,sizeof(short), 
	ECPGt_double,&(mainquery.bytes),(long)1,(long)1,sizeof(double), 
	ECPGt_short,&(mainind.bytes_ind),(long)1,(long)1,sizeof(short), 
	ECPGt_long_long,&(mainquery.ds_index),(long)1,(long)1,sizeof(long long), 
	ECPGt_short,&(mainind.ds_index_ind),(long)1,(long)1,sizeof(short), 
	ECPGt_long_long,&(mainquery.create_sumid),(long)1,(long)1,sizeof(long long), 
	ECPGt_short,&(mainind.create_sumid_ind),(long)1,(long)1,sizeof(short), 
	ECPGt_varchar,&(mainquery.tape_id),(long)20,(long)1,sizeof(struct varchar_7), 
	ECPGt_short,&(mainind.tape_id_ind),(long)1,(long)1,sizeof(short), 
	ECPGt_int,&(mainquery.tape_file_num),(long)1,(long)1,sizeof(int), 
	ECPGt_short,&(mainind.tape_file_num_ind),(long)1,(long)1,sizeof(short), 
	ECPGt_varchar,&(mainquery.tapepos),(long)15,(long)1,sizeof(struct varchar_8), 
	ECPGt_short,&(mainind.tapepos_ind),(long)1,(long)1,sizeof(short), 
	ECPGt_varchar,&(mainquery.l_date),(long)32,(long)1,sizeof(struct varchar_9), 
	ECPGt_short,&(mainind.l_date_ind),(long)1,(long)1,sizeof(short), ECPGt_EORT);
#line 139 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DS_DataRequest.pgc"

if (sqlca.sqlcode == ECPG_NOT_FOUND) goto end_fetch;
#line 139 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DS_DataRequest.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 139 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DS_DataRequest.pgc"


       strcpy(loc, mainquery.online_loc.arr);
       strcpy(l_tapepos, mainquery.tapepos.arr);
       strcpy(l_tape_id, mainquery.tape_id.arr);
       ds_index = mainquery.ds_index;
       
       /* find the position of the ds_index in the orig array */
       /* OK, this is a horrible N^2 algorithm, inside an N algorithm - so we have an O(N^3) algorithm! */
       for(pos=0; pos < reqcnt; pos++) 
       {
         if(ds_index == ds_index_arr[pos]) break;
       }
       if(ds_index_arr_pos[pos] == 0) 
       { 
         ds_index_arr_pos[pos] = 1;
       }
       else 
       {
         /* This means that duplicates have an empty string for their wd values. */
         continue; //skip a dup ds_index
       }	

       if (strcmp(mainquery.online_status.arr, "N")==0) 
       { /* not on-line */
         if(mode & NORETRIEVE) 
         {
           strcpy(loc, "");	/* give back empty wd */
         }
         else 
         {
           /* ck for not online & not archived! */
           if(strcmp(mainquery.archive_status.arr, "N")==0) 
           {
             strcpy(loc, "");			/* give back empty wd */
           }
           else 
           {
             offline = 1;
           }
         }
       }
       else 
       {                                      /* on-line */		

      /* If "touch" given then make deletable in tdays days. 
       * Update all entries
       * for the ds_index in the partn_alloc table.
      */
      if(touch) {
        /* get the original effective_date and see if ok to change */
        /* UGH, ds_index should have a primary key or unique key constraint, but it does not. */
        { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "select distinct EFFECTIVE_DATE from SUM_PARTN_ALLOC where DS_INDEX = $1  and STATUS <> $2 ", 
	ECPGt_long_long,&(ds_index),(long)1,(long)1,sizeof(long long), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_int,&(daro),(long)1,(long)1,sizeof(int), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EOIT, 
	ECPGt_varchar,&(eff_date_str),(long)20,(long)1,sizeof(struct varchar_2), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EORT);
#line 193 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DS_DataRequest.pgc"

if (sqlca.sqlcode == ECPG_NOT_FOUND) goto end_fetch;
#line 193 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DS_DataRequest.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 193 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DS_DataRequest.pgc"

        if(tdays < 0) {		//if tdays neg, only make eff_date bigger
          posdays = abs(tdays);
          eff_date = (char *)get_effdate(posdays);
          if(strcmp(eff_date, eff_date_str.arr) > 0) {
            change = 1; 
            //New 5/5/2014 Add 3 days grace, so likely subsequent calls
            //won't have to touch it again
            eff_date = (char *)get_effdate(posdays+3); //add 3 days grace (Did Jim really know about the rock band by this name - Art)
          }
          else change = 0;
        }
        else {
          eff_date = (char *)get_effdate(tdays);
          change = 1;
        }
        if(change) {
          printk("touch %lld %s %d days %s\n",
		ds_index, GETKEY_str(params, "username"), tdays, eff_date); 
          sprintf(l_eff_date.arr, "%s", eff_date);
          l_eff_date.len = strlen(l_eff_date.arr);
          /* exec sql whenever sqlerror  continue ; */
#line 214 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DS_DataRequest.pgc"

          /* exec sql whenever not found  continue ; */
#line 215 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DS_DataRequest.pgc"

          { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "update SUM_PARTN_ALLOC set EFFECTIVE_DATE = $1  where DS_INDEX = $2  and STATUS <> $3 ", 
	ECPGt_varchar,&(l_eff_date),(long)20,(long)1,sizeof(struct varchar_1), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_long_long,&(ds_index),(long)1,(long)1,sizeof(long long), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_int,&(daro),(long)1,(long)1,sizeof(int), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EOIT, ECPGt_EORT);}
#line 219 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DS_DataRequest.pgc"

          /* exec sql whenever sqlerror  goto  sqlerror ; */
#line 220 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DS_DataRequest.pgc"

          /* exec sql whenever not found  goto  end_fetch ; */
#line 221 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DS_DataRequest.pgc"

        }
        free(eff_date);
      } 
    }

      /* add an entry for read-only assignment to db partn_alloc tbl */
      // This was eliminated 22Oct2009. The DRMS has a default of touch 3
      // and so nothing should go offline while a module is running.
      //if(NC_PaUpdate(mainquery.online_loc.arr,uid,mainquery.bytes,DARO,0,0,0,0,ds_index,1,0))
      //{
      //  /* rollback already done */
      //  printk("Error on NC_PaUpdate to add to r/o list\n");
      //  return(DS_PALC_ERR);
      // }
        sprintf(dsix_name, "online_status_%d", pos);
        setkey_str(results, dsix_name, mainquery.online_status.arr);
        sprintf(dsix_name, "archive_status_%d", pos);
    	setkey_str(results, dsix_name, mainquery.archive_status.arr); 
        sprintf(dsix_name, "bytes_%d", pos);
 	setkey_double(results, dsix_name, mainquery.bytes);
        sprintf(dsix_name, "create_sumid_%d", pos);
        setkey_uint64(results, dsix_name, (unsigned long long)(mainquery.create_sumid));
        sprintf(dsix_name, "ds_index_%d", pos);
    	setkey_uint64(results, dsix_name, (unsigned long long)ds_index); 
        sprintf(dsix_name, "wd_%d", pos);
    	setkey_str(results, dsix_name, loc);
        sprintf(dsix_name, "tapeid_%d", pos);
        setkey_str(results, dsix_name, l_tape_id);
        sprintf(dsix_name, "tapefilenum_%d", pos);
        setkey_int(results, dsix_name, mainquery.tape_file_num);
    }
        end_fetch:
        setkey_int(results, "reqcnt", reqcnt);
        setkey_int(results, "mode", mode);
        setkey_int(results, "tdays", tdays);
        if((offline) && (mode & RETRIEVE)) {
          setkey_int(results, "offline", 1); 
        }
        else { setkey_int(results, "offline", 0); }
    	{ ECPGtrans(__LINE__, NULL, "commit work");
#line 261 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DS_DataRequest.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 261 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DS_DataRequest.pgc"

        /*reqreqstat(0, history);*/
	return NO_ERROR;
sqlerror:
         printk("Error in DS_DataRequest\n");
         printk("% .70s \n", sqlca.sqlerrm.sqlerrmc);
         /* exec sql whenever sqlerror  continue ; */
#line 267 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DS_DataRequest.pgc"

         { ECPGtrans(__LINE__, NULL, "rollback work");}
#line 268 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_DS_DataRequest.pgc"

         return DS_DATA_QRY;
}


/* This is an old legacy rte that is not used anymore. Keep for reference */
/* Called when a dataset has been found on or offline.
 * Keeps statistics on how many are on/offline. 
 * If offline, the DS_DataRequest() will be called again when the ds
 * is retrieved from tape, so dont inc the total counts if offline.
 *   offflg = the ds is offline if true
 *   log = log file for printf() type output msgs
*/
void reqreqstat(int offflg, int (*log)(char *fmt, ...))
{
  if(offflg) {
    n_off++; c_off++;
  }
  else {
    n_total++; c_total++;
  }
  /* output statistics for every 100 ds requested */
  if(c_total >= 100) {
    (*log)("\nIn total %d ds requested, %d were offline (%2.1fpercent)\n",
                n_total, n_off, ((float)n_off/(float)n_total)*100.0);
    (*log)("In last %d ds requested, %d were offline (%2.1fpercent)\n",
                c_total, c_off, ((float)c_off/(float)c_total)*100.0);
    c_total = c_off = 0;
  }
}
