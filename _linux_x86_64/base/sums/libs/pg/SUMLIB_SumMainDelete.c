/* Processed by ecpg (4.10.0) */
/* These include files are added by the preprocessor */
#include <ecpglib.h>
#include <ecpgerrno.h>
#include <sqlca.h>
/* End of automatic include section */

#line 1 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_SumMainDelete.pgc"
/* SUMLIB_SumMainDelete.pgc 
 * Special function to delete a row from the sum_main table given the ds_index 
 */
#include <SUM.h>
#include <sum_rpc.h>
#include <soi_error.h>
#include <printk.h>

int DS_SumMainDelete(uint64_t ds_index);

int DS_SumMainDelete(uint64_t ds_index)
{
/* exec sql begin declare section */
    	  

#line 14 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_SumMainDelete.pgc"
 long long l_ds_index ;
/* exec sql end declare section */
#line 15 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_SumMainDelete.pgc"


    l_ds_index = (long long)ds_index;

    /* exec sql whenever sqlerror  goto  sqlerror ; */
#line 19 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_SumMainDelete.pgc"

    /* exec sql whenever not found  goto  sqlerror ; */
#line 20 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_SumMainDelete.pgc"

		
    { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "delete from SUM_MAIN where DS_INDEX = $1 ", 
	ECPGt_long_long,&(l_ds_index),(long)1,(long)1,sizeof(long long), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EOIT, ECPGt_EORT);
#line 23 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_SumMainDelete.pgc"

if (sqlca.sqlcode == ECPG_NOT_FOUND) goto sqlerror;
#line 23 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_SumMainDelete.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 23 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_SumMainDelete.pgc"


    { ECPGtrans(__LINE__, NULL, "commit work");
#line 25 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_SumMainDelete.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 25 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_SumMainDelete.pgc"

	return NO_ERROR;

        
sqlerror:
	  printk("Error in DS_SumMainDelete\n"); 
	  printk("% .70s \n", sqlca.sqlerrm.sqlerrmc); 
    /* exec sql whenever sqlerror  continue ; */
#line 32 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_SumMainDelete.pgc"

    { ECPGtrans(__LINE__, NULL, "rollback work");}
#line 33 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_SumMainDelete.pgc"

    return DS_CATDEL_ERR;
}
