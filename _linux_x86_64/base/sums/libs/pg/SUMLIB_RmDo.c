/* Processed by ecpg (4.10.0) */
/* These include files are added by the preprocessor */
#include <ecpglib.h>
#include <ecpgerrno.h>
#include <sqlca.h>
/* End of automatic include section */

#line 1 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDo.pgc"
/* SUMLIB_RmDo.pc
 * Called by sum_rm to find expired dirs and remove them.
 */
#include <SUM.h>
#include <sum_rpc.h>
#include <soi_error.h>
#include <printk.h>

int DS_RmDo(double *bytesdel)
{
/* exec sql begin declare section */
   
   
     
     
   
   

#line 12 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDo.pgc"
  struct varchar_1  { int len; char arr[ 80 ]; }  l_wd ;
 
#line 13 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDo.pgc"
  struct varchar_2  { int len; char arr[ 20 ]; }  l_eff_date ;
 
#line 14 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDo.pgc"
 long long l_sumid ;
 
#line 15 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDo.pgc"
 long long ds_index ;
 
#line 16 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDo.pgc"
 int l_archsub ;
 
#line 17 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDo.pgc"
 double l_bytes ;
/* exec sql end declare section */
#line 18 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDo.pgc"

  double bytesdeleted, rmbytes;
  int i, j;

  typedef struct palloc_cursor {
    char p_wd[80];
    uint64_t p_sumid;
    double p_bytes;
    char p_effdate[20];
    uint64_t p_ds_index;
    int p_archsub;
  } PALLOC_CURSOR;
  //PALLOC_CURSOR pcursor[1600];
  PALLOC_CURSOR pcursor[60000];

  printk("DS_RmDo\n");  
  bytesdeleted = 0.0;

  /* exec sql whenever sqlerror  goto  sqlerror ; */
#line 36 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDo.pgc"


  /* declare palloc cursor for select WD , SUMID , BYTES , EFFECTIVE_DATE , DS_INDEX , ARCHIVE_SUBSTATUS from SUM_PARTN_ALLOC where STATUS = 2 order by effective_date limit 60000 */
#line 41 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDo.pgc"


  { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "declare palloc cursor for select WD , SUMID , BYTES , EFFECTIVE_DATE , DS_INDEX , ARCHIVE_SUBSTATUS from SUM_PARTN_ALLOC where STATUS = 2 order by effective_date limit 60000", ECPGt_EOIT, ECPGt_EORT);
#line 43 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDo.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 43 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDo.pgc"


  /* exec sql whenever not found  goto  end_fetch ; */
#line 45 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDo.pgc"
 
	
/* Warn about SUMDC code commented out below */
//printk("WARN: tmp noop of offsite_ack check\n"); /* !!!TEMP */

  for(i=0; ; i++ ){
    { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "fetch palloc", ECPGt_EOIT, 
	ECPGt_varchar,&(l_wd),(long)80,(long)1,sizeof(struct varchar_1), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_long_long,&(l_sumid),(long)1,(long)1,sizeof(long long), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_double,&(l_bytes),(long)1,(long)1,sizeof(double), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_varchar,&(l_eff_date),(long)20,(long)1,sizeof(struct varchar_2), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_long_long,&(ds_index),(long)1,(long)1,sizeof(long long), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_int,&(l_archsub),(long)1,(long)1,sizeof(int), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EORT);
#line 51 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDo.pgc"

if (sqlca.sqlcode == ECPG_NOT_FOUND) goto end_fetch;
#line 51 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDo.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 51 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDo.pgc"

    /* must save results as can't put other sql calls in this loop */
    strcpy(pcursor[i].p_wd, l_wd.arr);
    pcursor[i].p_sumid = (unsigned long long)l_sumid;
    pcursor[i].p_bytes = l_bytes;
    strcpy(pcursor[i].p_effdate, l_eff_date.arr);
    pcursor[i].p_ds_index = (unsigned long long)ds_index;
    pcursor[i].p_archsub = l_archsub;
  }

end_fetch:
    { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "close palloc", ECPGt_EOIT, ECPGt_EORT);
#line 62 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDo.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 62 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDo.pgc"

    /* exec sql whenever not found  goto  end_fetch2 ; */
#line 63 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDo.pgc"

    for(j=0; j < i; j++) {
#ifdef SUMDC
      /* only rm those with Offsite_Ack, and Safe_Tape */

/* !!!TEMP noop out below for testing with DDS */
      ds_index = (long long)(pcursor[j].p_ds_index);
      { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "select DS_INDEX from sum_main where DS_INDEX = $1  and OFFSITE_ACK = 'Y' and SAFE_TAPE is not null", 
	ECPGt_long_long,&(ds_index),(long)1,(long)1,sizeof(long long), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EOIT, 
	ECPGt_long_long,&(ds_index),(long)1,(long)1,sizeof(long long), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EORT);
#line 72 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDo.pgc"

if (sqlca.sqlcode == ECPG_NOT_FOUND) goto end_fetch2;
#line 72 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDo.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 72 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDo.pgc"

/* END of noop */

#endif
      if(DS_RmNow(pcursor[j].p_wd, pcursor[j].p_sumid, pcursor[j].p_bytes, 
		pcursor[j].p_effdate, 
		pcursor[j].p_ds_index, pcursor[j].p_archsub, &rmbytes)) {
        { ECPGtrans(__LINE__, NULL, "rollback work");
#line 79 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDo.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 79 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDo.pgc"

        printk("Error in DS_RmDo() call to DS_RmNow() ds_index=%llu\n",
		pcursor[j].p_ds_index);
        return DS_DATA_QRY;
      }
      bytesdeleted += rmbytes;     /* add what we deleted so far */
end_fetch2:
      continue;
    }
    *bytesdel = bytesdeleted;
    { ECPGtrans(__LINE__, NULL, "commit");
#line 89 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDo.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 89 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDo.pgc"

    return NO_ERROR;
	
sqlerror:
    printk("Error in DS_RmDo\n");
    printk("% .70s \n", sqlca.sqlerrm.sqlerrmc);
    { ECPGtrans(__LINE__, NULL, "rollback work");
#line 95 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDo.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 95 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDo.pgc"

    return DS_DATA_QRY;
}

int DS_RmNow(char *wd, uint64_t sumid, double bytes, char *effdate,
		uint64_t ds_index, int archsub, double *rmbytes) {
		
/* exec sql begin declare section */
   
   
     
   

#line 103 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDo.pgc"
  struct varchar_3  { int len; char arr[ 80 ]; }  target ;
 
#line 104 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDo.pgc"
 int l_count ;
 
#line 105 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDo.pgc"
 long long l_ds_index ;
 
#line 106 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDo.pgc"
 char stmt [ 65536 ] ;
/* exec sql end declare section */
#line 107 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDo.pgc"

  FILE *rxfp;
  uint64_t eff_date, today_date;
  char rwd[80], rectxt[128], line[128], seriesname[128];
  char *rootdir, *cptr, *cptr1, *token, *effd;

  /* exec sql whenever sqlerror  goto  sqlrmerror ; */
#line 113 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDo.pgc"

  /* exec sql whenever not found  goto  sqlrmerror ; */
#line 114 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDo.pgc"
 

    effd = get_effdate(0);
    today_date = (uint64_t)atol(effd);
    free(effd);
    /* l_ds_index is not defined - this looks broken - ART 2016.07.19. */
    { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "select count ( * ) from SUM_PARTN_ALLOC where DS_INDEX = $1  and status = 8", 
	ECPGt_long_long,&(l_ds_index),(long)1,(long)1,sizeof(long long), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EOIT, 
	ECPGt_int,&(l_count),(long)1,(long)1,sizeof(int), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EORT);
#line 121 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDo.pgc"

if (sqlca.sqlcode == ECPG_NOT_FOUND) goto sqlrmerror;
#line 121 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDo.pgc"

if (sqlca.sqlcode < 0) goto sqlrmerror;}
#line 121 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDo.pgc"

    /* !!!TBD lock the table before determine if it's DARO */
    if(l_count) {
       printk("%s is open DARO\n", wd);
       *rmbytes = 0.0;
       { ECPGtrans(__LINE__, NULL, "commit");
#line 126 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDo.pgc"

if (sqlca.sqlcode < 0) goto sqlrmerror;}
#line 126 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDo.pgc"

       return(0);		/* skip this dir opend for read */
    }
    eff_date = (uint64_t)atol(effdate);

//printk("DS_RmNow() wd=%s eff_date=%lu today_date=%lu\n", wd, eff_date, today_date); //!!!TEMP

    rootdir = strcpy(rwd, wd);
    //contin with del if garbage date > year 30000
    if(eff_date < 3000000000000) { //yyyymmddhhss
      if(eff_date > today_date) {
        *rmbytes = 0.0;
        { ECPGtrans(__LINE__, NULL, "commit");
#line 138 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDo.pgc"

if (sqlca.sqlcode < 0) goto sqlrmerror;}
#line 138 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDo.pgc"

        return(0);
      }
    }
    /* remove del pend entry from sum_partn_alloc tbl */
    if(NC_PaUpdate
        (wd, sumid, bytes,DADP,0,0,0,0,0,0,1))
    {
      printk("Err: NC_PaUpdate(%s,%ld,%e,DADP,0,0,0, ...)to rm from dp list\n", 
		wd,sumid,bytes);
      printk("  ??This is how we got the info in the first place!\n");
    }
    if(!(cptr = strstr(rootdir+1, "/D"))) {
      printk("The wd=%s doesn't have a /.../Dxxx term!\n",rootdir);
      *rmbytes = 0.0;
      { ECPGtrans(__LINE__, NULL, "commit");
#line 153 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDo.pgc"

if (sqlca.sqlcode < 0) goto sqlrmerror;}
#line 153 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDo.pgc"

      return(0);
    }
    if((cptr1 = strstr(cptr+1, "/"))) {
      *cptr1 = (char)NULL;		/* make like /SUM1/D1234 */
    }
    printk("Removing %s\n", wd);
    if(ds_index != 0) {         /* Don't take offline if 0*/
    if(SUM_StatOffline(ds_index)) {
      printk("Err: SUM_StatOffline(%llu, ...)\n", ds_index);
    }
    }
    if(archsub == DAAEDDP || archsub == DADPDELSU) { /* a temporary dataset */
      printk("Removing sum_main for ds_index = %llu\n", ds_index);
      if(DS_SumMainDelete(ds_index)) {
        printk("**Err: DS_SumMainDelete(%llu)\n", ds_index);
      }
    } 
#ifndef SUMDC
    //Note: Records.txt file is not on the datacapture nodes
    if(archsub != DADPDELSU) {
      sprintf(rectxt, "%s/Records.txt", wd);
      if(!(rxfp=fopen(rectxt, "r"))) {
        printk("**Err: DS_RmNow() can't open %s\n", rectxt);
        //NOTE: get this error for DRMS log directories w/no Records.txt
      }
      else {
        while(fgets(line, 256, rxfp)) {   /* get Records.txt file lines */
          if(!strstr(line, "DELETE_SLOTS_RECORDS")) { //must be 1st line
            break;
          }
          else {		//delete all record #s
            while(fgets(line, 256, rxfp)) {
              if(strstr(line, "series=")) {
                token=(char *)strtok(line, "=\n");
                if(token=(char *)strtok(NULL, "\n")) {
                  strcpy(seriesname, token);
                  sprintf(stmt, "delete from %s where recnum in (", 
					seriesname);
                }
                else {
                  printk("DS_RmNow() bad Records.txt file\n");
                  fclose(rxfp);
                  goto sqlrmerror;
                }
              }
              else if(strstr(line, "slot")) {
                continue;
              }
              else {		//this is line like: 0       425490
                token=(char *)strtok(line, "\t");
                if(token=(char *)strtok(NULL, "\n")) {
                  sprintf(stmt, "%s%s,", stmt, token);
                }
                else {
                  printk("DS_RmNow() bad Records.txt wd=%s\n", wd);
                  fclose(rxfp);
                  goto sqlrmerror;
                }
              }
            }
            cptr = rindex(stmt, ',');
            if(cptr) *cptr = '\0';
            sprintf(stmt, "%s);", stmt);
            printk("Records.txt statement is:\n%s\n", stmt);
            //sprintf(target.arr,  "%s@%s:%s", DBNAME, SERVER, DRMSPGPORT);
            sprintf(target.arr,  "%s@%s:%s", DBNAME, SERVER, DRMSPGPORT);
            target.len = strlen(target.arr);
            /* exec sql whenever sqlerror  goto  sqljsocerror ; */
#line 221 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDo.pgc"

            //Typically: EXEC SQL CONNECT TO jsoc@hmidb:5432;
            { ECPGconnect(__LINE__, 0, target.arr , NULL, NULL , NULL, 0); 
#line 223 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDo.pgc"

if (sqlca.sqlcode < 0) goto sqljsocerror;}
#line 223 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDo.pgc"

            { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_exec_immediate, stmt, ECPGt_EOIT, ECPGt_EORT);
#line 224 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDo.pgc"

if (sqlca.sqlcode < 0) goto sqljsocerror;}
#line 224 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDo.pgc"

            { ECPGtrans(__LINE__, NULL, "commit");
#line 225 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDo.pgc"

if (sqlca.sqlcode < 0) goto sqljsocerror;}
#line 225 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDo.pgc"

            { ECPGdisconnect(__LINE__, "CURRENT");
#line 226 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDo.pgc"

if (sqlca.sqlcode < 0) goto sqljsocerror;}
#line 226 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDo.pgc"

            /* exec sql whenever sqlerror  goto  sqlrmerror ; */
#line 227 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDo.pgc"

          }
        }
        fclose(rxfp);
      }
    }
#endif
    if(rmdirs(wd, rootdir)) {
      printk("Cannot rm %s\n", wd);
    }
    *rmbytes = bytes;
    { ECPGtrans(__LINE__, NULL, "commit");
#line 238 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDo.pgc"

if (sqlca.sqlcode < 0) goto sqlrmerror;}
#line 238 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_RmDo.pgc"

    return(0);

sqlrmerror:
    printk("Error in DS_RmNow\n");
    printk("% .70s \n", sqlca.sqlerrm.sqlerrmc);
    //EXEC SQL ROLLBACK WORK;
    return DS_DATA_QRY;

sqljsocerror:
    printk("Error in DS_RmNow() for del from Records.txt\n");
    printk("% .70s \n", sqlca.sqlerrm.sqlerrmc);
    //EXEC SQL DISCONNECT;
    //EXEC SQL ROLLBACK WORK;
    fclose(rxfp);
    return DS_DATA_QRY;
}
