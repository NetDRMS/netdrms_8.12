/* Processed by ecpg (4.10.0) */
/* These include files are added by the preprocessor */
#include <ecpglib.h>
#include <ecpgerrno.h>
#include <sqlca.h>
/* End of automatic include section */

#line 1 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Main_Update.pgc"
/* SUMLIB_Main_Update.pgc 
 *
*/
#include <SUM.h>
#include <sum_rpc.h>
#include <printk.h>

/* Updates the sum_main db table when a sum_put() is called. Called from
 * putdo_1() in sum_svc_proc.c. Can be called for multiple SUs.
 * Returns 0 on success.
 * Returns 1 on fatal error.
 * Returns 2 on error with individual wd
 * A input keylist looks like:
 * wd_0:   KEYTYP_STRING   /SUM1/D1703
 * dsix_0: KEYTYP_UINT64    1703
 * wd_1:   KEYTYP_STRING   /SUM0/D1696
 * dsix_1: KEYTYP_UINT64    1696
 * REQCODE:        KEYTYP_INT      7
 * DEBUGFLG:       KEYTYP_INT      1
 * storage_set:    KEYTYP_INT      0
 * group:  KEYTYP_INT      65
 * username:       KEYTYP_STRING   production
 * history_comment: KEYTYP_STRING   this is a dummy history comment that is
 * greater than 80 chars long to check out the code
 * dsname: KEYTYP_STRING   hmi_lev1_fd_V
 * reqcnt: KEYTYP_INT      2
 * tdays:  KEYTYP_INT      5
 * mode:   KEYTYP_INT      1
 * uid:    KEYTYP_UINT64    888
 *
 *Returns the above keylist with these values added:
 * status_0: KEYTYP_INT	1
 * status_1: KEYTYP_INT	0
 * bytes_0: KEYTYP_DOUBLE      1.200000e+08
 * bytes_1: KEYTYP_DOUBLE      1.200000e+08
 */
int SUM_Main_Update(KEY *params, KEY **results) 
{
/* exec sql begin declare section */
   
   
   
   
   
   
   
   
     
     
   
   
   
     
   
   
   
    
    
   
   

#line 40 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Main_Update.pgc"
  struct varchar_1  { int len; char arr[ 5 ]; }  online_status ;
 
#line 41 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Main_Update.pgc"
  struct varchar_2  { int len; char arr[ 5 ]; }  arch_status ;
 
#line 42 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Main_Update.pgc"
  struct varchar_3  { int len; char arr[ 5 ]; }  offsite_ack ;
 
#line 43 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Main_Update.pgc"
 char history_comment [ 80 ] ;
 
#line 44 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Main_Update.pgc"
 int storage_group ;
 
#line 45 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Main_Update.pgc"
 int storage_set ;
 
#line 46 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Main_Update.pgc"
 int safe_id ;
 
#line 47 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Main_Update.pgc"
 double bytes ;
 
#line 48 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Main_Update.pgc"
 long long ds_index ;
 
#line 49 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Main_Update.pgc"
 long long create_sumid ;
 
#line 50 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Main_Update.pgc"
 char * username ;
 
#line 51 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Main_Update.pgc"
 char * ccomment ;
 
#line 52 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Main_Update.pgc"
 char wd [ 80 ] ;
 
#line 53 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Main_Update.pgc"
 char * cwd = wd ;
 
#line 54 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Main_Update.pgc"
 char * dsname ;
 
#line 55 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Main_Update.pgc"
  struct varchar_4  { int len; char arr[ 32 ]; }  l_date ;
 
#line 56 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Main_Update.pgc"
 int mode ;
 
#line 57 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Main_Update.pgc"
 int apstatus ;
 
#line 58 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Main_Update.pgc"
 int archsub ;
 
#line 59 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Main_Update.pgc"
 int touch ;
 
#line 60 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Main_Update.pgc"
 char * effective_date ;
/* exec sql end declare section */
#line 61 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Main_Update.pgc"

  char dsnamestr[128], nametmp[80];
  int reqcnt, i, errflg;
  

#line 1 "/usr/include/postgresql/sqlca.h"
#ifndef POSTGRES_SQLCA_H
#define POSTGRES_SQLCA_H

#ifndef PGDLLIMPORT
#if  defined(WIN32) || defined(__CYGWIN__)
#define PGDLLIMPORT __declspec (dllimport)
#else
#define PGDLLIMPORT
#endif   /* __CYGWIN__ */
#endif   /* PGDLLIMPORT */

#define SQLERRMC_LEN	150

#ifdef __cplusplus
extern		"C"
{
#endif

struct sqlca_t
{
	char		sqlcaid[8];
	long		sqlabc;
	long		sqlcode;
	struct
	{
		int			sqlerrml;
		char		sqlerrmc[SQLERRMC_LEN];
	}			sqlerrm;
	char		sqlerrp[8];
	long		sqlerrd[6];
	/* Element 0: empty						*/
	/* 1: OID of processed tuple if applicable			*/
	/* 2: number of rows processed				*/
	/* after an INSERT, UPDATE or				*/
	/* DELETE statement					*/
	/* 3: empty						*/
	/* 4: empty						*/
	/* 5: empty						*/
	char		sqlwarn[8];
	/* Element 0: set to 'W' if at least one other is 'W'	*/
	/* 1: if 'W' at least one character string		*/
	/* value was truncated when it was			*/
	/* stored into a host variable.             */

	/*
	 * 2: if 'W' a (hopefully) non-fatal notice occurred
	 */	/* 3: empty */
	/* 4: empty						*/
	/* 5: empty						*/
	/* 6: empty						*/
	/* 7: empty						*/

	char		sqlstate[5];
};

struct sqlca_t *ECPGget_sqlca(void);

#ifndef POSTGRES_ECPG_INTERNAL
#define sqlca (*ECPGget_sqlca())
#endif

#ifdef __cplusplus
}
#endif

#endif

#line 65 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Main_Update.pgc"


   reqcnt = getkey_int(params, "reqcnt");
   create_sumid = (long long)getkey_uint64(params, "uid");
   safe_id = 0;			/* !!TBD check use of this */
   errflg = 0;

  for(i=0; i < reqcnt; i++) {
   sprintf(nametmp, "wd_%d", i);
   strcpy(wd, GETKEY_str(params, nametmp));
   if(!strcmp(wd, "")) { 
     sprintf(nametmp, "status_%d", i);
     setkey_int(results, nametmp, 1);
     sprintf(nametmp, "bytes_%d", i);
     setkey_double(results, nametmp, 0.0);
     errflg = 1;
     continue;
   }
   bytes = du_dir(wd);
   sprintf(nametmp, "bytes_%d", i);
   setkey_double(results, nametmp, bytes);
   if(!findkey(params, "history_comment")) { strcpy(history_comment, " "); }
   else {
     ccomment = GETKEY_str(params, "history_comment");
     if(strlen(ccomment) > 79) { 
       strncpy(history_comment, ccomment, 79);
       history_comment[79] = '\0';
     }
     else { strcpy(history_comment, ccomment); }
   }
   ccomment = history_comment;
   sprintf(nametmp, "dsix_%d", i);
   ds_index = (long long)getkey_uint64(params, nametmp);

   strcpy(online_status.arr, "Y");
   online_status.len = strlen(online_status.arr);
   strcpy(arch_status.arr, "N");
   arch_status.len = strlen(arch_status.arr);
   strcpy(offsite_ack.arr, "N");
   offsite_ack.len = strlen(offsite_ack.arr);
   dsname = getkey_str(params, "dsname");
   if(!dsname) { strcpy(dsnamestr,  "<none>"); }
   else { 
     strcpy(dsnamestr, dsname);
     free(dsname);
   }
   dsname = dsnamestr;
   storage_group = getkey_int(params, "group");
   storage_set = getkey_int(params, "storage_set");
   username = getkey_str(params, "username");
   mode = getkey_int(params, "mode");
   if (SUMS_TAPE_AVAILABLE) apstatus = DAAP; /* archive if mode & ARCH or mode & PERM */
   else apstatus = DADP; /* dont' archive */
   if(mode & ARCH) {
     archsub = DAADP;
   }
   if(mode & TEMP) {
     archsub = DAAEDDP;
   }
   if(mode & PERM) {
     /* Used in tape system. */
     archsub = DAAPERM;
   }
   if(findkey(params, "tdays")) touch = abs(getkey_int(params, "tdays"));
   else touch = 2;
   effective_date = (char *)get_effdate(touch);
   sprintf(l_date.arr, "%s", get_datetime());
   l_date.len = strlen(l_date.arr);

   /* exec sql whenever sqlerror  goto  sqlerror ; */
#line 134 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Main_Update.pgc"


   { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "insert into SUM_MAIN ( ONLINE_LOC , ONLINE_STATUS , ARCHIVE_STATUS , OFFSITE_ACK , DS_INDEX , CREATE_SUMID , HISTORY_COMMENT , OWNING_SERIES , STORAGE_GROUP , STORAGE_SET , BYTES , CREAT_DATE , ACCESS_DATE , USERNAME ) values ( $1  , $2  , $3  , $4  , $5  , $6  , $7  , $8  , $9  , $10  , $11  , $12  , $13  , $14  )", 
	ECPGt_char,&(cwd),(long)0,(long)1,(1)*sizeof(char), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_varchar,&(online_status),(long)5,(long)1,sizeof(struct varchar_1), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_varchar,&(arch_status),(long)5,(long)1,sizeof(struct varchar_2), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_varchar,&(offsite_ack),(long)5,(long)1,sizeof(struct varchar_3), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_long_long,&(ds_index),(long)1,(long)1,sizeof(long long), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_long_long,&(create_sumid),(long)1,(long)1,sizeof(long long), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_char,&(ccomment),(long)0,(long)1,(1)*sizeof(char), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_char,&(dsname),(long)0,(long)1,(1)*sizeof(char), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_int,&(storage_group),(long)1,(long)1,sizeof(int), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_int,&(storage_set),(long)1,(long)1,sizeof(int), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_double,&(bytes),(long)1,(long)1,sizeof(double), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_varchar,&(l_date),(long)32,(long)1,sizeof(struct varchar_4), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_varchar,&(l_date),(long)32,(long)1,sizeof(struct varchar_4), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_char,&(username),(long)0,(long)1,(1)*sizeof(char), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EOIT, ECPGt_EORT);
#line 168 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Main_Update.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 168 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Main_Update.pgc"


   if(NC_PaUpdate(cwd, create_sumid, bytes, apstatus, archsub, effective_date, 
       storage_group, safe_id, ds_index, 1, 0)) {
     { ECPGtrans(__LINE__, NULL, "rollback work");
#line 172 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Main_Update.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 172 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Main_Update.pgc"

     free(effective_date);
     free(username);
     return(1);
   }
   free(effective_date);
   free(username);
   sprintf(nametmp, "status_%d", i);
   setkey_int(results, nametmp, 0);
   continue;

sqlerror:
    printk("Error in SUM_Main_Update() \n");
    printk("% .70s \n", sqlca.sqlerrm.sqlerrmc);
    /* exec sql whenever sqlerror  continue ; */
#line 186 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Main_Update.pgc"

    { ECPGtrans(__LINE__, NULL, "rollback work");}
#line 187 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Main_Update.pgc"

    free(effective_date);
    free(username);
    return(1);
 }
   { ECPGtrans(__LINE__, NULL, "commit work");}
#line 192 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Main_Update.pgc"

   if(errflg) return(2);
   else return(0);
}
