/* Processed by ecpg (4.10.0) */
/* These include files are added by the preprocessor */
#include <ecpglib.h>
#include <ecpgerrno.h>
#include <sqlca.h>
/* End of automatic include section */

#line 1 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Close.pgc"
/* SUMLIB_Close.pgc
 * Remove the SUMUID entry from the sum_open db table for a user 
 * that previously opened with the SUMS. Return 0 on success.
 */
#include <SUM.h>
#include <sum_rpc.h>
#include <soi_error.h>
#include <printk.h>

int SUMLIB_Close(KEY *params)
{
/* exec sql begin declare section */
     

#line 13 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Close.pgc"
 long long uid ;
/* exec sql end declare section */
#line 14 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Close.pgc"

  
    /* exec sql whenever sqlerror  goto  sqlerror ; */
#line 16 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Close.pgc"


  uid = (long long)getkey_uint64(params, "uid");

  /* now clear all of our read-only partitions */
         { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "delete from SUM_PARTN_ALLOC where SUMID = $1  and STATUS = 8", 
	ECPGt_long_long,&(uid),(long)1,(long)1,sizeof(long long), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EOIT, ECPGt_EORT);
#line 23 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Close.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 23 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Close.pgc"


  /* now clear all of our read-write partitions */
         { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "delete from SUM_PARTN_ALLOC where SUMID = $1  and STATUS = 1", 
	ECPGt_long_long,&(uid),(long)1,(long)1,sizeof(long long), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EOIT, ECPGt_EORT);
#line 28 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Close.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 28 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Close.pgc"


    { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "delete from SUM_OPEN where SUMID = $1 ", 
	ECPGt_long_long,&(uid),(long)1,(long)1,sizeof(long long), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EOIT, ECPGt_EORT);
#line 31 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Close.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 31 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Close.pgc"

    { ECPGtrans(__LINE__, NULL, "commit work");
#line 32 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Close.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 32 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Close.pgc"

    return NO_ERROR;

sqlerror:
    printk("Error in SUMLIB_Close() call \n");
    printk("% .70s \n", sqlca.sqlerrm.sqlerrmc);
    /* exec sql whenever sqlerror  continue ; */
#line 38 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Close.pgc"

    { ECPGtrans(__LINE__, NULL, "rollback work");}
#line 39 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_Close.pgc"

    return DS_PE_ERR;
}
