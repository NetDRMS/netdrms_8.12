/* Processed by ecpg (4.10.0) */
/* These include files are added by the preprocessor */
#include <ecpglib.h>
#include <ecpgerrno.h>
#include <sqlca.h>
/* End of automatic include section */

#line 1 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailGet.pgc"
/* SUMLIB_PavailGet.pgc  
 * This is a function to query the Database Partition Availability table
 * Currently the table is SUM_partn_avail. Get the first partn that is in the
 * given storage set and  has bytes >= bytes requested.
 * Searches for storage round-robin amoungst the /SUM partitions.
 * Returns 0 on success with the wd in "partn_name" in the results keylist.
 */
#include <SUM.h>
#include <sum_rpc.h>
#include <soi_error.h>
#include <printk.h>

/* initial index to ptabx[] and last one used for each /SUM set */
/* First initialized by DS_PavailRequest2() called by SUM_Init() */
/* Set for a possible 1 /SUM partition per set */
int part_index_init[MAX_PART];
int part_index[MAX_PART];

extern PADATA *pahdr_rw;

int SUMLIB_PavailGet(double bytes, int pds_set, uint64_t uid, uint64_t sunum, KEY **results);

int SUMLIB_SumsetGet(int group, int *sumset);

int SUMLIB_PavailGet(double bytes, int pds_set, uint64_t uid, uint64_t sunum, KEY **results) 
{
/* exec sql begin declare section */
   
    
    
  	  
      /* Previously of type myuint64_t but this caused issues - Art and Niles July 2016 */

#line 28 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailGet.pgc"
  struct varchar_1  { int len; char arr[ 80 ]; }  l_name ;
 
#line 29 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailGet.pgc"
 double l_bytes ;
 
#line 30 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailGet.pgc"
 double total_bytes ;
 
#line 31 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailGet.pgc"
 int l_pnum ;
 
#line 32 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailGet.pgc"
 long long ds_index ;
/* exec sql end declare section */
#line 33 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailGet.pgc"


extern PART ptabx[]; 		/* defined in sum_svc.c */
int fnum, found, i, j;
char *pnames[MAX_PART];
double btotals[MAX_PART];
char mkpart[MAX_STR];
char dirstr[MAX_STR];


#line 1 "/usr/include/postgresql/sqlca.h"
#ifndef POSTGRES_SQLCA_H
#define POSTGRES_SQLCA_H

#ifndef PGDLLIMPORT
#if  defined(WIN32) || defined(__CYGWIN__)
#define PGDLLIMPORT __declspec (dllimport)
#else
#define PGDLLIMPORT
#endif   /* __CYGWIN__ */
#endif   /* PGDLLIMPORT */

#define SQLERRMC_LEN	150

#ifdef __cplusplus
extern		"C"
{
#endif

struct sqlca_t
{
	char		sqlcaid[8];
	long		sqlabc;
	long		sqlcode;
	struct
	{
		int			sqlerrml;
		char		sqlerrmc[SQLERRMC_LEN];
	}			sqlerrm;
	char		sqlerrp[8];
	long		sqlerrd[6];
	/* Element 0: empty						*/
	/* 1: OID of processed tuple if applicable			*/
	/* 2: number of rows processed				*/
	/* after an INSERT, UPDATE or				*/
	/* DELETE statement					*/
	/* 3: empty						*/
	/* 4: empty						*/
	/* 5: empty						*/
	char		sqlwarn[8];
	/* Element 0: set to 'W' if at least one other is 'W'	*/
	/* 1: if 'W' at least one character string		*/
	/* value was truncated when it was			*/
	/* stored into a host variable.             */

	/*
	 * 2: if 'W' a (hopefully) non-fatal notice occurred
	 */	/* 3: empty */
	/* 4: empty						*/
	/* 5: empty						*/
	/* 6: empty						*/
	/* 7: empty						*/

	char		sqlstate[5];
};

struct sqlca_t *ECPGget_sqlca(void);

#ifndef POSTGRES_ECPG_INTERNAL
#define sqlca (*ECPGget_sqlca())
#endif

#ifdef __cplusplus
}
#endif

#endif

#line 42 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailGet.pgc"


    l_bytes = bytes;
    l_pnum = pds_set;

    /* exec sql whenever sqlerror  goto  sqlerror ; */
#line 47 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailGet.pgc"

    /* exec sql whenever not found  goto  end_fetch ; */
#line 48 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailGet.pgc"


	ECPGset_var( 0, &( l_pnum ), __LINE__);\
 ECPGset_var( 1, &( l_bytes ), __LINE__);\
 /* declare availget cursor for select PARTN_NAME , AVAIL_BYTES from SUM_PARTN_AVAIL where AVAIL_BYTES >= $1  and PDS_SET_NUM = $2  */
#line 54 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailGet.pgc"
 

	{ ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "declare availget cursor for select PARTN_NAME , AVAIL_BYTES from SUM_PARTN_AVAIL where AVAIL_BYTES >= $1  and PDS_SET_NUM = $2 ", 
	ECPGt_double,&(l_bytes),(long)1,(long)1,sizeof(double), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_int,&(l_pnum),(long)1,(long)1,sizeof(int), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EOIT, ECPGt_EORT);
#line 56 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailGet.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 56 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailGet.pgc"

  if(pds_set < 0) return DATA_NOT_FOUND;
  for(fnum=0; ;fnum++ ) {
	{ ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "fetch availget", ECPGt_EOIT, 
	ECPGt_varchar,&(l_name),(long)80,(long)1,sizeof(struct varchar_1), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_double,&(total_bytes),(long)1,(long)1,sizeof(double), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EORT);
#line 59 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailGet.pgc"

if (sqlca.sqlcode == ECPG_NOT_FOUND) goto end_fetch;
#line 59 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailGet.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 59 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailGet.pgc"

        pnames[fnum] = (char *)malloc(32);
        strcpy(pnames[fnum], l_name.arr);
        btotals[fnum] = total_bytes;
  }

end_fetch:
        /* This printk() may be useful for debugging. It winds up in the SUMS log. Art and Niles July 2016. 
        printk("NILES : At end_fetch I have fnum=%d and sqlca.sqlcode=%d with sqlca.sqlerrm.sqlerrmc='%s'\n",
                fnum, sqlca.sqlcode, sqlca.sqlerrm.sqlerrmc); */
        if(fnum == 0) {
          printk("Err SUMLIB_PavailGet-can't get requested storage on SUM set %d %g bytes\n", pds_set, l_bytes);
	  { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "close availget", ECPGt_EOIT, ECPGt_EORT);
#line 71 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailGet.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 71 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailGet.pgc"

          { ECPGtrans(__LINE__, NULL, "commit");
#line 72 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailGet.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 72 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailGet.pgc"

          return DATA_NOT_FOUND;
        }
        found = 0;
        /* now find a hit that is on or past the last part_index */
        /* a hit has to be here */
        for(i=part_index[pds_set]; ; i++) {
          if(ptabx[i].pds_set_num != pds_set) {
             i = part_index_init[pds_set]; 
             part_index[pds_set] = part_index_init[pds_set];
          }
          for(j=0; j<fnum; j++) {
              if(!strcmp(ptabx[i].name, pnames[j])) {
                found = 1; 
		part_index[pds_set] = i+1; /* start after where left off */
                break;
              }
           }
           if(found) break;
        }
        strcpy(l_name.arr, pnames[j]);
        l_name.len = strlen(l_name.arr);

        for(i=0; i<fnum; i++) free(pnames[i]);

	{ ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "close availget", ECPGt_EOIT, ECPGt_EORT);
#line 97 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailGet.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 97 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailGet.pgc"

        { ECPGtrans(__LINE__, NULL, "commit");
#line 98 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailGet.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 98 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailGet.pgc"


    if(sunum == 0) {	//we assign ds_index
      { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "select NEXTVAL ( 'SUM_DS_INDEX_SEQ' )", ECPGt_EOIT, 
	ECPGt_long_long,&(ds_index),(long)1,(long)1,sizeof(long long), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EORT);
#line 101 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailGet.pgc"

if (sqlca.sqlcode == ECPG_NOT_FOUND) goto end_fetch;
#line 101 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailGet.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 101 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailGet.pgc"

    }
    else {		//use the given ds_index
      ds_index = (long long)sunum;
    }
        setkey_uint64(results, "ds_index", (unsigned long long)ds_index);
        sprintf(mkpart, "%s/D%lld", l_name.arr, ds_index);
	setkey_str(results, "partn_name", mkpart);
        sprintf(dirstr, "mkdir %s; chmod 02775 %s", mkpart, mkpart);
        if(system(dirstr)) {
          printk("Error in SUMLIB_PavailGet() for SUNUM %lld, cmd: %s\n", ds_index, dirstr);
          { ECPGtrans(__LINE__, NULL, "commit");
#line 112 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailGet.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 112 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailGet.pgc"

          return DS_ALLOC_ERR;
        }

        //printk("Allocate in SUMLIB_PavailGet %g bytes in %s\n", l_bytes, l_name.arr);
        { ECPGtrans(__LINE__, NULL, "commit");
#line 117 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailGet.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 117 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailGet.pgc"

	return NO_ERROR;
    
sqlerror:
       /* The next printk() is just too verbose for words, you have to kill SUMS or
          the logs bloat VERY rapidly if there is an error. Art and Niles July 2016.
       printk("NILES : At sqlerror I have sqlca.sqlcode=%d with sqlca.sqlerrm.sqlerrmc='%s'\n",
                sqlca.sqlcode, sqlca.sqlerrm.sqlerrmc); */

	{ ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "close availget", ECPGt_EOIT, ECPGt_EORT);
#line 126 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailGet.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 126 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailGet.pgc"

        { ECPGtrans(__LINE__, NULL, "commit");
#line 127 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailGet.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 127 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailGet.pgc"

	printk("Error in SUMLIB_PavailGet\n");  
	printk("% .70s \n", sqlca.sqlerrm.sqlerrmc);  
    return DS_PAVAIL_ERR;
}








/* Get the sum_set number for the given group number from the db
 * table sum_arch_group. If none found, sets sumset to 0.
 * Return 1 on success.
*/
int SUMLIB_SumsetGet(int group, int *sumset)
{
/* exec sql begin declare section */
       
       

#line 147 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailGet.pgc"
 int l_sumset ;
 
#line 148 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailGet.pgc"
 int l_group ;
/* exec sql end declare section */
#line 149 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailGet.pgc"


  *sumset = 0;		//default is 0
  l_group = group;

    /* exec sql whenever sqlerror  goto  sqlerror ; */
#line 154 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailGet.pgc"

    /* exec sql whenever not found  goto  sqlerror ; */
#line 155 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailGet.pgc"


  { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "select sum_set from SUM_ARCH_GROUP where group_id = $1 ", 
	ECPGt_int,&(l_group),(long)1,(long)1,sizeof(int), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EOIT, 
	ECPGt_int,&(l_sumset),(long)1,(long)1,sizeof(int), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EORT);
#line 157 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailGet.pgc"

if (sqlca.sqlcode == ECPG_NOT_FOUND) goto sqlerror;
#line 157 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailGet.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 157 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailGet.pgc"


  *sumset = l_sumset;
  return(1);

sqlerror:
        printk("Error in SUMLIB_SumsetGet for group %d\n", group);
        printk("% .70s \n", sqlca.sqlerrm.sqlerrmc);
    *sumset = 0;
    return 0;
}
