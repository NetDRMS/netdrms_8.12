/* Processed by ecpg (4.10.0) */
/* These include files are added by the preprocessor */
#include <ecpglib.h>
#include <ecpgerrno.h>
#include <sqlca.h>
/* End of automatic include section */

#line 1 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailUpdate.pgc"
/* SUMLIB_PavailUpdate.pgc
*/
#include <SUM.h>
#include <sum_rpc.h>
#include <soi_error.h>
#include <printk.h>

int SUMLIB_PavailUpdate(char *name, double bytes);
int SUMLIB_PavailOff(char *name);
int SUMLIB_PavailOn(char *name, int setnum);

int SUMLIB_PavailUpdate(char *name, double bytes)
{
/* exec sql begin declare section */
   
    

#line 15 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailUpdate.pgc"
  struct varchar_1  { int len; char arr[ 80 ]; }  l_name ;
 
#line 16 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailUpdate.pgc"
 long long l_bytes ;
/* exec sql end declare section */
#line 17 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailUpdate.pgc"



#line 1 "/usr/include/postgresql/sqlca.h"
#ifndef POSTGRES_SQLCA_H
#define POSTGRES_SQLCA_H

#ifndef PGDLLIMPORT
#if  defined(WIN32) || defined(__CYGWIN__)
#define PGDLLIMPORT __declspec (dllimport)
#else
#define PGDLLIMPORT
#endif   /* __CYGWIN__ */
#endif   /* PGDLLIMPORT */

#define SQLERRMC_LEN	150

#ifdef __cplusplus
extern		"C"
{
#endif

struct sqlca_t
{
	char		sqlcaid[8];
	long		sqlabc;
	long		sqlcode;
	struct
	{
		int			sqlerrml;
		char		sqlerrmc[SQLERRMC_LEN];
	}			sqlerrm;
	char		sqlerrp[8];
	long		sqlerrd[6];
	/* Element 0: empty						*/
	/* 1: OID of processed tuple if applicable			*/
	/* 2: number of rows processed				*/
	/* after an INSERT, UPDATE or				*/
	/* DELETE statement					*/
	/* 3: empty						*/
	/* 4: empty						*/
	/* 5: empty						*/
	char		sqlwarn[8];
	/* Element 0: set to 'W' if at least one other is 'W'	*/
	/* 1: if 'W' at least one character string		*/
	/* value was truncated when it was			*/
	/* stored into a host variable.             */

	/*
	 * 2: if 'W' a (hopefully) non-fatal notice occurred
	 */	/* 3: empty */
	/* 4: empty						*/
	/* 5: empty						*/
	/* 6: empty						*/
	/* 7: empty						*/

	char		sqlstate[5];
};

struct sqlca_t *ECPGget_sqlca(void);

#ifndef POSTGRES_ECPG_INTERNAL
#define sqlca (*ECPGget_sqlca())
#endif

#ifdef __cplusplus
}
#endif

#endif

#line 19 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailUpdate.pgc"


    sprintf(l_name.arr,"%s", name);
    l_name.len = strlen(l_name.arr);
    l_bytes = (long long)bytes;
    if(strcmp(l_name.arr, "")){
    /*printf("not null wd:: %s\n", l_wd.arr); */
    } else {
    /*printf("null l_wd.arr:: %s\n", l_wd.arr); */
            return DS_PAVAIL_ERR;
    }
    /* exec sql whenever sqlerror  goto  sqlerror ; */
#line 30 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailUpdate.pgc"


    /* exec sql whenever not found  goto  sqlerror ; */
#line 32 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailUpdate.pgc"


    { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "update SUM_PARTN_AVAIL set AVAIL_BYTES = $1  where PARTN_NAME = $2 ", 
	ECPGt_long_long,&(l_bytes),(long)1,(long)1,sizeof(long long), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_varchar,&(l_name),(long)80,(long)1,sizeof(struct varchar_1), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EOIT, ECPGt_EORT);
#line 36 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailUpdate.pgc"

if (sqlca.sqlcode == ECPG_NOT_FOUND) goto sqlerror;
#line 36 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailUpdate.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 36 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailUpdate.pgc"

    { ECPGtrans(__LINE__, NULL, "commit work");
#line 37 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailUpdate.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 37 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailUpdate.pgc"

	return NO_ERROR;
    
sqlerror:
	printk("Error in SUMLIB_PavailUpdate\n");  
	printk("% .70s \n", sqlca.sqlerrm.sqlerrmc);  
    //EXEC SQL WHENEVER SQLERROR CONTINUE;
    //EXEC SQL ROLLBACK WORK;
    return DS_PAVAIL_ERR;
}


int SUMLIB_PavailOff(char *name)
{
/* exec sql begin declare section */
   

#line 52 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailUpdate.pgc"
  struct varchar_2  { int len; char arr[ 80 ]; }  l_name ;
/* exec sql end declare section */
#line 53 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailUpdate.pgc"



#line 1 "/usr/include/postgresql/sqlca.h"
#ifndef POSTGRES_SQLCA_H
#define POSTGRES_SQLCA_H

#ifndef PGDLLIMPORT
#if  defined(WIN32) || defined(__CYGWIN__)
#define PGDLLIMPORT __declspec (dllimport)
#else
#define PGDLLIMPORT
#endif   /* __CYGWIN__ */
#endif   /* PGDLLIMPORT */

#define SQLERRMC_LEN	150

#ifdef __cplusplus
extern		"C"
{
#endif

struct sqlca_t
{
	char		sqlcaid[8];
	long		sqlabc;
	long		sqlcode;
	struct
	{
		int			sqlerrml;
		char		sqlerrmc[SQLERRMC_LEN];
	}			sqlerrm;
	char		sqlerrp[8];
	long		sqlerrd[6];
	/* Element 0: empty						*/
	/* 1: OID of processed tuple if applicable			*/
	/* 2: number of rows processed				*/
	/* after an INSERT, UPDATE or				*/
	/* DELETE statement					*/
	/* 3: empty						*/
	/* 4: empty						*/
	/* 5: empty						*/
	char		sqlwarn[8];
	/* Element 0: set to 'W' if at least one other is 'W'	*/
	/* 1: if 'W' at least one character string		*/
	/* value was truncated when it was			*/
	/* stored into a host variable.             */

	/*
	 * 2: if 'W' a (hopefully) non-fatal notice occurred
	 */	/* 3: empty */
	/* 4: empty						*/
	/* 5: empty						*/
	/* 6: empty						*/
	/* 7: empty						*/

	char		sqlstate[5];
};

struct sqlca_t *ECPGget_sqlca(void);

#ifndef POSTGRES_ECPG_INTERNAL
#define sqlca (*ECPGget_sqlca())
#endif

#ifdef __cplusplus
}
#endif

#endif

#line 55 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailUpdate.pgc"


    sprintf(l_name.arr,"%s", name);
    l_name.len = strlen(l_name.arr);
    if(strcmp(l_name.arr, "")){
    /*printf("not null wd:: %s\n", l_wd.arr); */
    } else {
    /*printf("null l_wd.arr:: %s\n", l_wd.arr); */
            return DS_PAVAIL_ERR;
    }
    /* exec sql whenever sqlerror  goto  sqlerror ; */
#line 65 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailUpdate.pgc"


    /* exec sql whenever not found  goto  sqlerror ; */
#line 67 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailUpdate.pgc"


    { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "update SUM_PARTN_AVAIL set pds_set_num = - 1 where PARTN_NAME = $1 ", 
	ECPGt_varchar,&(l_name),(long)80,(long)1,sizeof(struct varchar_2), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EOIT, ECPGt_EORT);
#line 71 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailUpdate.pgc"

if (sqlca.sqlcode == ECPG_NOT_FOUND) goto sqlerror;
#line 71 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailUpdate.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 71 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailUpdate.pgc"

    { ECPGtrans(__LINE__, NULL, "commit work");
#line 72 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailUpdate.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 72 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailUpdate.pgc"

	return NO_ERROR;
    
sqlerror:
	printk("Error in SUMLIB_PavailOff\n");  
	printk("% .70s \n", sqlca.sqlerrm.sqlerrmc);  
    //EXEC SQL WHENEVER SQLERROR CONTINUE;
    //EXEC SQL ROLLBACK WORK;
    return DS_PAVAIL_ERR;
}

int SUMLIB_PavailOn(char *name, int setnum)
{
/* exec sql begin declare section */
   
   

#line 86 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailUpdate.pgc"
  struct varchar_3  { int len; char arr[ 80 ]; }  l_name ;
 
#line 87 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailUpdate.pgc"
 int l_setnum ;
/* exec sql end declare section */
#line 88 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailUpdate.pgc"



#line 1 "/usr/include/postgresql/sqlca.h"
#ifndef POSTGRES_SQLCA_H
#define POSTGRES_SQLCA_H

#ifndef PGDLLIMPORT
#if  defined(WIN32) || defined(__CYGWIN__)
#define PGDLLIMPORT __declspec (dllimport)
#else
#define PGDLLIMPORT
#endif   /* __CYGWIN__ */
#endif   /* PGDLLIMPORT */

#define SQLERRMC_LEN	150

#ifdef __cplusplus
extern		"C"
{
#endif

struct sqlca_t
{
	char		sqlcaid[8];
	long		sqlabc;
	long		sqlcode;
	struct
	{
		int			sqlerrml;
		char		sqlerrmc[SQLERRMC_LEN];
	}			sqlerrm;
	char		sqlerrp[8];
	long		sqlerrd[6];
	/* Element 0: empty						*/
	/* 1: OID of processed tuple if applicable			*/
	/* 2: number of rows processed				*/
	/* after an INSERT, UPDATE or				*/
	/* DELETE statement					*/
	/* 3: empty						*/
	/* 4: empty						*/
	/* 5: empty						*/
	char		sqlwarn[8];
	/* Element 0: set to 'W' if at least one other is 'W'	*/
	/* 1: if 'W' at least one character string		*/
	/* value was truncated when it was			*/
	/* stored into a host variable.             */

	/*
	 * 2: if 'W' a (hopefully) non-fatal notice occurred
	 */	/* 3: empty */
	/* 4: empty						*/
	/* 5: empty						*/
	/* 6: empty						*/
	/* 7: empty						*/

	char		sqlstate[5];
};

struct sqlca_t *ECPGget_sqlca(void);

#ifndef POSTGRES_ECPG_INTERNAL
#define sqlca (*ECPGget_sqlca())
#endif

#ifdef __cplusplus
}
#endif

#endif

#line 90 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailUpdate.pgc"


    sprintf(l_name.arr,"%s", name);
    l_name.len = strlen(l_name.arr);
    if(strcmp(l_name.arr, "")){
    /*printf("not null wd:: %s\n", l_wd.arr); */
    } else {
    /*printf("null l_wd.arr:: %s\n", l_wd.arr); */
            return DS_PAVAIL_ERR;
    }
    l_setnum = setnum;
    /* exec sql whenever sqlerror  goto  sqlerror ; */
#line 101 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailUpdate.pgc"


    /* exec sql whenever not found  goto  sqlerror ; */
#line 103 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailUpdate.pgc"


    { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "update SUM_PARTN_AVAIL set pds_set_num = $1  where PARTN_NAME = $2 ", 
	ECPGt_int,&(l_setnum),(long)1,(long)1,sizeof(int), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_varchar,&(l_name),(long)80,(long)1,sizeof(struct varchar_3), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, ECPGt_EOIT, ECPGt_EORT);
#line 107 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailUpdate.pgc"

if (sqlca.sqlcode == ECPG_NOT_FOUND) goto sqlerror;
#line 107 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailUpdate.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 107 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailUpdate.pgc"

    { ECPGtrans(__LINE__, NULL, "commit work");
#line 108 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailUpdate.pgc"

if (sqlca.sqlcode < 0) goto sqlerror;}
#line 108 "/usr/local/netdrms_8.12/base/sums/libs/pg/SUMLIB_PavailUpdate.pgc"

	return NO_ERROR;
    
sqlerror:
	printk("Error in SUMLIB_PavailOn\n");  
	printk("% .70s \n", sqlca.sqlerrm.sqlerrmc);  
    //EXEC SQL WHENEVER SQLERROR CONTINUE;
    //EXEC SQL ROLLBACK WORK;
    return DS_PAVAIL_ERR;
}
