/* fitsexport.h */

#ifndef _FITSEXPORT_H
#define _FITSEXPORT_H

#include "drms.h"
#include "cfitsio.h"

enum FE_Keyword_ExtType_enum
{
   kFE_Keyword_ExtType_None = 0,
   kFE_Keyword_ExtType_Integer,
   kFE_Keyword_ExtType_Float,
   kFE_Keyword_ExtType_String,
   kFE_Keyword_ExtType_Logical,
   kFE_Keyword_ExtType_End
};

typedef enum FE_Keyword_ExtType_enum FE_Keyword_ExtType_t;

/******** Functions to handle exporting FITS segments  **********/
/* Maps to external keywords in this order.  If an item does not result in a valid
 * FITS keyword, then the next item is consulted.
 *   1. Name in keyword description.
 *   2. DRMS name.
 *   3. Name generated by default rule to convert from DRMS name to FITS name. */
int fitsexport_export_tofile(DRMS_Segment_t *seg, const char *cparms, const char *fileout, char **actualfname, unsigned long long *expsize);

/* Maps to external keywords in this order.  If an item does not result in a valid
 * FITS keyword, then the next item is consulted.
 *   1. if (map != NULL), map DRMS name to external name using map.
 *   2. if (class != NULL), use default rule associated with class to map to external name.
 *   3. Name in keyword description.
 *   4. DRMS name.
 *   5. Name generated by default rule to convert from DRMS name to FITS name. */
int fitsexport_mapexport_tofile(DRMS_Segment_t *seg, 
                                const char *cparms, 
                                const char *clname, 
                                const char *mapfile,
                                const char *fileout,
                                char **actualfname,
                                unsigned long long *expsize);

#ifndef __export_callback_func_t__
#define __export_callback_func_t__
typedef int (*export_callback_func_t)(char *, ...); //ISS fly-tar
#endif
int fitsexport_mapexport_tofile2(DRMS_Segment_t *seg,
                                 const char *cparms,
                                 const char *clname,
                                 const char *mapfile,
                                 const char *fileout,
                                 char **actualfname,
                                 unsigned long long *expsize,
                                 export_callback_func_t callback); //ISS fly-tar

CFITSIO_KEYWORD *fitsexport_mapkeys(DRMS_Segment_t *seg, 
                                    const char *clname, 
                                    const char *mapfile, 
                                    int *status);

/* Exporting DRMS keywords to FITS keywords */
int fitsexport_exportkey(DRMS_Keyword_t *key, CFITSIO_KEYWORD **fitskeys);
int fitsexport_mapexportkey(DRMS_Keyword_t *key,
                            const char *clname, 
                            Exputl_KeyMap_t *map,
                            CFITSIO_KEYWORD **fitskeys);

/* Maps to external keywords in this order.  If an item does not result in a valid
 * FITS keyword, then the next item is consulted.
 *   1. Name in keyword description.
 *   2. DRMS name.
 *   3. Name generated by default rule to convert from DRMS name to FITS name. */
int fitsexport_getextkeyname(DRMS_Keyword_t *key, char *nameOut, int size);

/* Maps to external keywords in this order.  If an item does not result in a valid
 * FITS keyword, then the next item is consulted.
 *   1. if (map != NULL), map DRMS name to external name using map.
 *   2. if (class != NULL), use default rule associated with class to map to external name.
 *   3. Name in keyword description.
 *   4. DRMS name.
 *   5. Name generated by default rule to convert from DRMS name to FITS name. */
int fitsexport_getmappedextkeyname(DRMS_Keyword_t *key, 
                                   const char *class, 
                                   Exputl_KeyMap_t *map,
                                   char *nameOut,
                                   int size);
int fitsexport_fitskeycheck(const char *fitsName);

/* Maps to internal keywords in this order.  If an item does not result in a valid
 * DRMS keyword, then the next item is consulted.
 *   1. FITS name.
 *   2. Name generated by default rule to convert from FITS name to DRMS name. */
int fitsexport_getintkeyname(const char *keyname, char *nameOut, int size);

/* Maps to internal keywords in this order.  If an item does not result in a valid
 * DRMS keyword, then the next item is consulted.
 *   1. if (map != NULL), map FITS name to DRMS name using map.
 *   2. if (class != NULL), use default rule associated with class to map to DRMS name.
 *   3. FITS name.
 *   4. Name generated by default rule to convert from FITS name to DRMS name. */
int fitsexport_getmappedintkeyname(const char *keyname, 
                                   const char *class, 
                                   Exputl_KeyMap_t *map,
                                   char *nameOut, 
                                   int size);

int fitsexport_importkey(CFITSIO_KEYWORD *fitskey, HContainer_t *keys, int verbose);
int fitsexport_mapimportkey(CFITSIO_KEYWORD *fitskey,
                            const char *clname, 
                            const char *mapfile,
                            HContainer_t *keys,
                            int verbose);

FE_Keyword_ExtType_t fitsexport_keyword_getcast(DRMS_Keyword_t *key);

int fitsexport_fitskeycheck(const char *fitsName);

int fitsexport_getextname(const char *strin, char **extname, char **cast);

/**
   @fn int fitsexport_exportkey(DRMS_Keyword_t *key, CFITSIO_KEYWORD **fitskeys)
   blah blah
*/

/**
   @fn int fitsexport_mapexportkey(DRMS_Keyword_t *key, const char *clname, Exputl_KeyMap_t *map, CFITSIO_KEYWORD **fitskeys)
   blah blah
*/

/**
   @fn int fitsexport_getextkeyname(DRMS_Keyword_t *key, char *nameOut,	int size)
   blah blah
*/

/** @fn int fitsexport_getmappedextkeyname(DRMS_Keyword_t *key, const char *class, Exputl_KeyMap_t *map, char *nameOut, int size)
    blah blah
*/

#endif // _FITSEXPORT_H
