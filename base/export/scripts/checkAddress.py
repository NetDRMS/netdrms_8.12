#!/usr/bin/env python

# The arguments to this script are parsed by cgi.FieldStorage(), which knows how to parse
# both HTTP GET and POST requests. A nice feature is that we can test the script as it runs in a CGI context
# by simply running on the command line with a single argument that is equivalent to an HTTP GET parameter string
# (e.g., address=gimli@mithril.com&addresstab=jsoc.export_addresses&domaintab=jsoc.export_addressdomains).

# Parameters:
#   address (required) - The email address to check or register.
#   addresstab (required) - The database table containing all registered (or registration-pending) email addresses.
#   domaintab (required) - The database table containing all email domains.
#   dbuser (optional) - The database account to be used when connecting to the database. The default is the value of the WEB_DBUSER parameter in DRMSParams.
#   checkonly (optional) - If set to 1, then no attept is made to register an unregistered email. In this case, if no error occurs then the possible return status codes are RV_REGISTEREDADDRESS, RV_REGISTRATIONPENDING, or RV_UNREGISTEREDADDRESS. The default is False (unknown addresses are registered).

from __future__ import print_function
import sys
import os
import pwd
import re
import uuid
from datetime import datetime
from urllib import unquote
import smtplib
import cgi
import json
import psycopg2
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../../include'))
from drmsparams import DRMSParams

# Return values
RV_ERROR = -1
RV_ERROR_ARGS = -2
RV_ERROR_MAIL = -3
RV_ERROR_PARAMS = -4
RV_ERROR_DBCMD = -5
RV_ERROR_DBCONNECT = -6
RV_REGISTRATIONINITIATED = 1
RV_REGISTEREDADDRESS = 2
RV_REGISTRATIONPENDING = 3
RV_UNREGISTEREDADDRESS = 4

def SendMail(localName, domainName, confirmation):
    subject = 'CONFIRM EXPORT ADDRESS'
    fromAddr = 'jsoc@solarpost.stanford.edu'
    toAddrs = [ localName + '@' + domainName ]
    msg = 'From: jsoc@solarpost.stanford.edu\nTo: ' + ','.join(toAddrs) + '\nSubject: ' + subject + '\nThis message was automatically generated by the JSOC export system at Stanford.\n\nYou have requested that data be exported and have provided an email address that has not yet been registered. To complete the registration process, please reply to this message within two minutes. Please do not modify the body of this message when replying. You will receive another email message notifying you of the disposition of your registration.'
    msg += '\n[' + str(confirmation) + ']'

    try:
        server = smtplib.SMTP('solarpost.stanford.edu')
        server.sendmail(fromAddr, toAddrs, msg)
        server.quit()
    except Exception as exc:
        # If any exception happened, then the email message was not received.
        raise Exception('emailBadrecipient', 'Unable to send email message to address to confirm address.', RV_ERROR_MAIL)


if __name__ == "__main__":
    msg = None
    rv = RV_ERROR
    rootObj = {}

    try:
        optD = {}
        
        # Should be invoked as an HTTP POST. If this script is invoked via HTTP POST, then FieldStorage() will consume the arguments passed
        # via STDIN, and they will no longer be available to any program called by this script.
    
        # I think I finally figured it out. cgi.FieldStorage() uses the REQUEST_METHOD environment variable to determine if the 
        # arguments are from STDIN or in QUERY_STRING (if it exists) / sys.argv[1]. If REQUEST_METHOD is not set, then it gets
        # the args from QUERY_STRING/sys.argv[1]. If REQUEST_METHOD is set, then it uses stdin.
        arguments = cgi.FieldStorage()
        
        optD['checkonly'] = False
        
        if arguments:
            for key in arguments.keys():
                val = arguments.getvalue(key)

                if key in ('address'):
                    optD['address'] = unquote(val)
                elif key in ('addresstab'):
                    optD['addresstab'] = val
                elif key in ('domaintab'):
                    optD['domaintab'] = val
                elif key in ('dbuser'):
                    optD['dbuser'] = val
                elif key in ('checkonly'):
                    if int(val) == 1:
                        optD['checkonly'] = True
                # Do not worry about extraneous arguments. I had to add a 'sleep' argument to the shell wrapper, but I do not
                # know how to strip arguments in shell.                

        # Ensure required arguments are present.
        if 'address' not in optD:
            raise Exception('caArgs', "Missing required argument 'address'.", RV_ERROR_ARGS)
            
        if 'addresstab' not in optD:
            raise Exception('caArgs', "Missing required argument 'addresstab'.", RV_ERROR_ARGS)
        
        if 'domaintab' not in optD:
            raise Exception('caArgs', "Missing required argument 'domaintab'.", RV_ERROR_ARGS)
            
        # Do a quick validation on the email address.
        regExp = re.compile(r'\s*[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,6}')
        matchObj = regExp.match(optD['address'])
        if matchObj is None:
            raise Exception('caArgs', 'Not a valid email address.', RV_ERROR_ARGS)

        drmsParams = DRMSParams()
        if drmsParams is None:
            raise Exception('drmsParams', 'Unable to locate DRMS parameters file (drmsparams.py).', RV_ERROR_PARAMS)

        if 'dbuser' not in optD:
            optD['dbuser'] = drmsParams.get('WEB_DBUSER')

        localName, domainName = optD['address'].split('@')

        try:
            with psycopg2.connect(database=drmsParams.get('DBNAME'), user=optD['dbuser'], host=drmsParams.get('SERVER'), port=drmsParams.get('DRMSPGPORT')) as conn:
                with conn.cursor() as cursor:
                    cmd = 'SELECT A.confirmation FROM ' + optD['addresstab'] + ' AS A, ' + optD['domaintab'] + " AS D WHERE A.domainid = D.domainid AND A.localname = '" + localName + "' AND D.domainname = '" + domainName + "'"
                    
                    try:
                        cursor.execute(cmd)
                        rows = cursor.fetchall()
                        if len(rows) > 1:
                            raise Exception('dbCorruption', 'Unexpected number of rows returned: ' + cmd + '.', RV_ERROR_DBCMD)
                    except psycopg2.Error as exc:
                        # Handle database-command errors.
                        raise Exception('dbCmd', exc.diag.message_primary, RV_ERROR_DBCMD)
                    
                    if len(rows) == 0:
                        # Email address is not in our database. 
                        if optD['checkonly']:
                            # Do not initiate registration.
                            rv = RV_UNREGISTEREDADDRESS
                            msg = 'Email address has neither been validated nor registered.'
                        else:
                            # Register it.
                            confirmation = uuid.uuid5(uuid.NAMESPACE_DNS, domainName)
                            # Send an email message out with a new confirmation code.
                            SendMail(localName, domainName, confirmation)
                    
                            # Insert a row in the domain table, if the domain does not exist.
                            cmd = 'SELECT domainid FROM ' + optD['domaintab'] + " WHERE lower(domainname) = '" + domainName.lower() + "'"
                            
                            try:
                                cursor.execute(cmd)
                                rows = cursor.fetchall()
                                if len(rows) > 1:
                                    raise Exception('dbCorruption', 'Unexpected number of rows returned: ' + cmd + '.', RV_ERROR_DBCMD)
                        
                                if len(rows) == 0:
                                    # The domain does not exist. Add the domain.
                                    # Get next item in sequence.
                                    cmd = "SELECT nextval('" + optD['domaintab'] + "_seq')"
                                    cursor.execute(cmd)
                                    rows = cursor.fetchall()
                                    if len(rows) > 1:
                                        raise Exception('dbCorruption', 'Unexpected number of rows returned: ' + cmd + '.', RV_ERROR_DBCMD)
                                    
                                    domainid = rows[0][0]

                                    cmd = 'INSERT INTO ' + optD['domaintab'] + '(domainid, domainname) VALUES(' + str(domainid) + ", '" + domainName.lower() + "')"
                                    cursor.execute(cmd)
                                else:
                                    # The domain does exist.
                                    domainid = rows[0][0]
                        
                                # Insert a row into the addresses table.
                                starttime = datetime.now().strftime('%Y-%m-%d %T')
            
                                cmd = 'INSERT INTO ' + optD['addresstab'] + "(localname, domainid, confirmation, starttime) VALUES('" + localName + "', " + str(domainid) + ", '" + str(confirmation) + "', '" + starttime + "')"
                                cursor.execute(cmd)
                            except psycopg2.Error as exc:
                                # Handle database-command errors.
                                raise Exception('dbCmd', exc.diag.message_primary, RV_ERROR_DBCMD)
                    
                            rv = RV_REGISTRATIONINITIATED
                            msg = 'Email address is not registered. Starting registration process. You will receive an email address from user jsoc. Please reply to this email message within two minutes without modifying the subject. The body will be ignored.'
                    else:
                        # Email address is in our database. Check to see if registration is pending.
                        confirmation = rows[0][0]
                        
                        if confirmation is None or len(confirmation) == 0:
                            # Email address in our database is registered.
                            rv = RV_REGISTEREDADDRESS
                            msg = 'Email address is valid and registered.'
                        else:
                            # We are in the process of registering this address already.
                            rv = RV_REGISTRATIONPENDING
                            msg = 'Email-address registration is pending. Please wait and try again later.'
        except psycopg2.DatabaseError as exc:
            # Closes the cursor and connection
            
            # Man, there is no way to get an error message from any exception object that will provide any information why
            # the connection failed.
            raise Exception('dbConnect', 'Unable to connect to the database.', RV_ERROR_DBCONNECT)
    except Exception as exc:
        if len(exc.args) != 3:
            raise # Re-raise

        etype = exc.args[0]

        if etype == 'emailBadrecipient' or etype == 'caArgs' or etype == 'drmsParams' or etype == 'dbCorruption' or etype == 'dbCmd' or etype == 'dbConnect':
            msg = exc.args[1]
            rv = exc.args[2]
        else:
            raise # Re-raise

    if rv != RV_REGISTRATIONINITIATED and rv != RV_REGISTEREDADDRESS and rv != RV_REGISTRATIONPENDING:
        if msg is None:
            msg = 'Unknown error'

    rootObj['status'] = rv
    rootObj['msg'] = msg

    # Do not print application/json here. This script may be called outside of a CGI context.
    print(json.dumps(rootObj))

    # Always return 0. If there was an error, an error code (the 'status' property) and message (the 'statusMsg' property) goes in the returned HTML.
    sys.exit(0)
