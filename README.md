IAS install of NetDrms 
NetDrms is a shared data management system developed by the Joint Science 
Operations Center (JSOC)

DRMS 
In order to process, archive, and distribute the substantial quantity of data 
flowing from the Atmospheric Imaging Assembly (AIA) and Helioseismic and 
Magnetic Imager (HMI) instruments on the Solar Dynamics Observatory (SDO),
the JSOC has developed its own data management system.

For more info go to [NetDrms](jsoc.stanford.edu/netDrms)


2 branches 
'master' is the original one from jsoc
'prod-IAS' is the local one with all IDOC & MEDOC modification to make it work 
